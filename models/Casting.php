<?php

class Casting extends Model
{
    public static $_table = 'casting';
    public static $_id_column = 'id';
    public function peliculas()
    {
    	$casting = Model::factory('Peliculas')
    		->select('peliculas.*')
    		->select('casting_peliculas.id_casting')
    		->join('casting_peliculas',array('peliculas.id','=','casting_peliculas.id_pelicula'))
    		->where('casting_peliculas.id_casting',$this->id)
    		->find_many();
    	return $casting;
    }
    public function nombres_peliculas()
    {
		$ret = array();
    	foreach ($this->peliculas() as $pelicula) {
    		$ret[] = $pelicula->nombre;
    	}
    	return join($ret,'<br>');
    }
}

