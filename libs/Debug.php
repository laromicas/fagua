<?php

/**
* 
*/
class Debug {
	public static function grab_dump($var) {
	    ob_start();
	    var_dump($var);
	    return ob_get_clean();
	}
}
