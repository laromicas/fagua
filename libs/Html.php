<?php

class Html {

    public static function img($img, $attrib = array()) {
    	$att = array();
    	foreach ($attrib as $key => $value) {
    		$att[] = $key.'="'.$value.'"';
    	}
    	$attstr = join($att,' ');
        return '<img '.$attstr.' src="'.URL.'public/img/'.$img.'">';
    }

    public static function javascript($js, $defer=false) {
      $def = ($defer) ? 'defer="defer" ' : '';
	    echo '<script language="javascript" '.$def.' src="'.URL.'public/js/'.$js.'.js?'.VCSSJS.'"></script>
        ';
    }

    public static function style($css, $link = false, $attrib = array()) {
      if ($link) {
        $att = array();
        foreach ($attrib as $key => $value) {
            $att[] = $key.'="'.$value.'"';
        }
        $attstr = join($att,' ');
        echo '<link rel="stylesheet" type="text/css" href="'.URL.'public/css/'.$css.'.css?'.VCSSJS.'" '.$attstr.' />';
      } else {
        echo '@import url("'.URL.'public/css/'.$css.'.css?'.VCSSJS.'");
        ';
      }
    }
    
    public static function link($url, $name='', $attrib = array()) {
    	require_once('config/routes.php');
    	$att = array();
    	foreach ($attrib as $key => $value) {
    		$att[] = $key.'="'.$value.'"';
    	}
    	$attstr = join($att,' ');
		list ($link,$nam) = ($name) ? array($url,$name) : (@$routes[$name]) ? array($url,$routes[$name]) : array($url,$url);
	    return '<a href="'.URL.$link.'" '.$attstr.'>'.$nam.'</a>';
	}

    public static function url_to($url, $name='', $attrib = array()) {
    	require_once('config/routes.php');
    	$att = array();
    	foreach ($attrib as $key => $value) {
    		$att[] = $key.'="'.$value.'"';
    	}
    	$attstr = join($att,' ');
		//list ($link,$nam) = ($name) ? array($url,$name) : (@$routes[$name]) ? array($url,$routes[$name]) : array($url,$url);
	    return URL.$url;
	}

    public static function redirect_to($url) {
        header('location: '.$url);
    }

    public static function check_image_type($src) {
        if( substr($src, 0, 5) == 'data:') {
            return 'base64';
        } else if( strtolower(substr($src, -4)) == '.jpg') {
            return 'jpeg';
        } else if( strtolower(substr($src, -4)) == '.png') {
            return 'png';
        } else if( strtolower(substr($src, -4)) == '.gif') {
            return 'gif';
        }
    }

    public static function check_image_source($src) {
        if( substr($src, 0, 1) == '/') {
            return 'local';
        } else if( substr($src, 0, 4) == 'http') {
            return 'external';
        } else {
            return 'other';
        }
    }

    public static function import_base64_image($src) {
        $data = $src;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        list(,$ext)       = explode('/', $type);
        return array("type" => $type, "ext" => $ext, "data" => base64_decode($data));
    }

    public static function make_li($menu) {
        switch ($menu["idTipoLink"]) {
            case '0':
                return '<a href="'.URL.'wiki/menu/'.$menu["key"].'">'.$menu["title"].'</a>';
                break;
            case '1':
                return '<a href="#">'.$menu["title"].'</a>';
                break;
            case '2':
                return '<a href="'.URL.'buscar/'.$menu["link"].'">'.$menu["title"].'</a>';
                break;
            case '3':
                return '<a href="'.$menu["link"].'">'.$menu["title"].'</a>';
                break;
            default:
                return '<a href="#">'.$menu["title"].'</a>';
                break;
        }
    }

    public static function limit_string($cadena, $tam){
        $PUNTOS ="";
        if(strlen($cadena)>$tam){

            $PUNTOS ="...";

        }
        $contador = 0;
        $texto = $cadena;

        // Cortamos la cadena por los espacios
        $arrayTexto = preg_split('/ /',$texto);
        $texto = '';

        // Reconstruimos la cadena
        while($tam >= strlen($texto) + strlen($arrayTexto[$contador])){
            $texto .= ' '.$arrayTexto[$contador];
            $contador++;
            if(!array_key_exists($contador, $arrayTexto)) { break; }
        }
        return $texto . $PUNTOS;
    }


    public static function get_http($url,$params) {
        // create curl resource

        $ch = curl_init();
        if ($ch == FALSE) {
            return FALSE;
        }
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // Set maximum redirects
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        // Allow a max of 5 seconds.
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        // set url
        if( count($params) > 0 ) {
            $query = http_build_query($params);
            // echo $url.'?'.$query;
            curl_setopt($ch, CURLOPT_URL, "$url?$query");
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }

        // $output contains the output string
        $output = curl_exec($ch);

        // Check for errors and such.
        $info = curl_getinfo($ch);
        $errno = curl_errno($ch);
        if( $output === false || $errno != 0 ) {
            // Do error checking
        } else if($info['http_code'] != 200) {
            // Got a non-200 error code.
            // Do more error checking
        }

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    public static function get_http_anyway($url,$params) {
        $result = Html::get_http($url,$params);
        if(!$result) {
            return Html::get_http_anyway($url,$params);
            // return get_http_anyway($url,$params);
        }
        return $result;
    }

    public static function pagination($start, $length, $total) {
        $active = floor($start/$length) + 1;
        $last = ceil($total/$length);
        // echo $start.'-';
        // echo $length.'-';
        // echo $total.'-';
        // echo $active.'-';
        // echo $last.'-';

        if($active + 3 > $last) {
            $start = $last - 2;
        } elseif($active < 4) {
            $start = 0;
        } else {
            $start = $active;
        }

        // echo $start.'-';
        $start = ($start < 2) ? 1 : $start - 2;
        $start = ($start < 2) ? 1 : $start;      // De nuevo para evitar un bug
        // echo $start.'-';

        $html = "<div id='container'>".
                "<div class='pagination'>".
                "<ul>".
                "<li p='0' class='".(($active != 1) ? 'active' : 'inactive')."'>".Translate::tr(array('pagination','first'))."</li>".
                "<li p='".(($active == 1) ? '0' : ($active-2)*$length)."' class='".(($active != 1) ? 'active' : 'inactive')."'>".Translate::tr(array('pagination','previous'))."</li>";

        for ($i=$start; $i < $start+5; $i++) {
            $html .= "<li p='".(($i-1)*$length)."' ".(($active==$i)? "style='color:#fff;background-color:#621414;' class='inactive'" : "class='active'").">".$i."</li>";
            if ($i==$last) { break; }
        }
        $html .= "<li p='".(($active == $last) ? $last : $active*$length)."' class='".(($active != $last) ? 'active' : 'inactive')."'>".Translate::tr(array('pagination','next'))."</li>".
            "<li p='".(($last-1)*$length)."' class='".(($active != $last) ? 'active' : 'inactive')."'>".Translate::tr(array('pagination','last'))."</li>".
            "</ul>".
            // "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/>".
            // "<input type='button' id='go_btn' class='go_button' value='Go'/>".
            // "<span class='total' a='738'>Pagina <b>".$active."</b> of <b>".$last."</b></span>".
            "</div>".
            "</div><br>";
        return $html;
    }
}