<?php
/**
* 
*/

class ImportExcelFile_Nguillers extends ImportExcelFile
{
    public function importExcel($inputFileName, $extension = '', $onlyStock = false) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 600);
        if($extension == 'csv') {
            return $this->importCsv($inputFileName,$onlyStock);
            $delimiter = $this->detect_delimiter($inputFileName);
            if(!$delimiter) { return array('Error en Archivo'); }
            $objReader = PHPExcel_IOFactory::createReader('CSV');
            $objReader->setDelimiter($delimiter); 
            // $objReader->setEnclosure('');
            // $objReader->setLineEnding("\r\n");
            $objReader->setSheetIndex(0);
            $objPHPExcel = $objReader->load($inputFileName);
        } else {
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        }
        
        $Pesos = $this->associate_by(Model::factory('Pesos')->find_many(),"nombrePeso");
        $Secciones = $this->associate_by(Model::factory('Secciones')->find_many(),"idSeccion");
        $Presentaciones = $this->associate_by(Model::factory('Presentaciones')->find_many(),"nombrePresentacion");
        $Paquetes = $this->associate_by(Model::factory('Paquetes')->find_many(),"nombrePaquete");
        $Idiomas = $this->associate_by(Model::factory('Idiomas')->find_many(),"nombreIdioma");

        // $objReader->setReadDataOnly(true);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,false,true);
        //$highestColumn = $sheetData->getHighestColumn();
        //$sheetData[1]['A']='nombreProducto';

        /*
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //  Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++){ 
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
            //  Insert row data array into your database of choice here
        }
        /**/

        foreach ($sheetData[1] as $key => $value) {
            if(!$value) {
                unset($sheetData[1][$key]);
            }
        }
        $cols = array_flip($sheetData[1]);
        $campoIdioma = false;
        $idIdioma = false;
        foreach ($cols as $col => $id) {
            foreach ($Idiomas as $idioma => $value) {
                if(strpos($col, '-'.$idioma)) {
                    $campoIdioma = $idioma;
                    $idIdioma = $value->idIdioma;
                    break 2;
                }
            };
        }

        $required = array('refPaqueteProducto','nombreProducto','nombrePeso','idSeccion','nombrePresentacion');
        $checknull = array('refPaqueteProducto','nombreProducto','nombrePeso','idSeccion','nombrePresentacion');
        $titulos = $sheetData[1];
        $keys = array_keys($titulos);
		$last_key = array_pop($keys); // key( array_slice( $errores, -1, 1, TRUE ) ); //
        $key_errores = ++$last_key;
        $errores = array();
		//echo json_encode(array("error"=>0,"message"=>$key_errores)); die();
        $sheetData[1][$key_errores] = 'Errores';
        $errores[] = $sheetData[1];
        $this->validate_rows($cols,$errores,$required);


        $isFirst = true;
        $cont = 0;
        foreach ($sheetData as $row) {
            if ($isFirst) {
                $isFirst = false;
                continue;
            }
            $cont++;
            $percent = $cont/(count($sheetData)-1);
            $params = array('row' => $cont, 'total' => count($sheetData)-1);
            $this->execXhr($percent,$params);

            if($row[$cols['nombrePresentacion']] == 'paq') { $row[$cols['nombrePresentacion']] = 'Paquete'; }
            if($row[$cols['nombrePresentacion']] == 'Paq') { $row[$cols['nombrePresentacion']] = 'Paquete'; }
            if($row[$cols['nombrePresentacion']] == 'PAQ') { $row[$cols['nombrePresentacion']] = 'Paquete'; }
            if($row[$cols['nombrePresentacion']] == 'Rll') { $row[$cols['nombrePresentacion']] = 'Rollo'; }
            if($row[$cols['nombrePresentacion']] == 'Unid') { $row[$cols['nombrePresentacion']] = 'Unidades'; }
            if($row[$cols['nombrePaquete']]) {
                $paq = explode(' ', $row[$cols['nombrePaquete']]);
                $row[$cols['nombrePaquete']] = $paq[0];
            }

            // // if($cont == 4173 || $cont == 4174) {
            //     echo $cont.'<br>';
            //     echo '-';
            //     echo $cols['nombrePaquete'];
            //     echo $row[$cols['nombrePaquete']];
            //     var_dump($row);
            // // }
            // echo '-<br>';

            // if($onlyStock) {
            //     $PaqueteProducto = Model::factory("PaquetesProducto")->where('refPaqueteProducto',$row[$cols['refPaqueteProducto']])->find_one();
            //     if(!$PaqueteProducto) {
            //         continue;
            //     }
            //     if(@$cols['stock'] || @$cols['stockPaqueteProducto']) {
            //         if(@$cols['stockPaqueteProducto']) {
            //             if($row[$cols['stockPaqueteProducto']]) {
            //                 $PaqueteProducto->stockPaqueteProducto = $row[$cols['stockPaqueteProducto']];
            //             }
            //         }
            //         if(@$cols['stock']) {
            //             if($row[$cols['stock']]) {
            //                 $PaqueteProducto->stockPaqueteProducto = $row[$cols['stock']];
            //             }
            //         }
            //     }
            //     $PaqueteProducto->save();
            //     continue;
            // }

            if(!array_key_exists($row[$cols['nombrePeso']], $Pesos) && $row[$cols['nombrePeso']]) { $row[$key_errores] = "Peso ".$row[$cols['nombrePeso']]." no existe"; $errores[] = $row; continue; }
            if(!array_key_exists(intval($row[$cols['idSeccion']]), $Secciones)) { $row[$key_errores] = "Seccion ".$row[$cols['idSeccion']]." no existe"; $errores[] = $row; continue; }
            if(count($Secciones[$row[$cols['idSeccion']]]->childs())) { $row[$key_errores] = "seccion ".$row[$cols['idSeccion']]." tiene subcategorias"; $errores[] = $row; continue; }
            if(!array_key_exists($row[$cols['nombrePresentacion']], $Presentaciones)) { $row[$key_errores] = "Presentacion ".$row[$cols['nombrePresentacion']]." no existe"; $errores[] = $row; continue; }
            //Si la intervalo1 2 o 3 tienen caracteres no validos
            // if(!$row[$cols['nombrePresentacion']], $Presentaciones)) { $row[$key_errores] = "Presentacion ".$row[$cols['nombrePresentacion']]." no existe"; $errores[] = $row; continue; }
            if(!array_key_exists($row[$cols['nombrePaquete']], $Paquetes)) { $row[$key_errores] = "Paquete ".$row[$cols['nombrePaquete']]." invalido"; $errores[] = $row; continue; }

            if(!$row[$cols['intervalo1']] || !$row[$cols['precio1']]) { $row[$key_errores] = "Intervalo1 esta vacio"; $errores[] = $row; continue; }
            if(!$row[$cols['intervalo2']] || !$row[$cols['precio2']]) { $row[$key_errores] = "Intervalo2 esta vacio"; $errores[] = $row; continue; }
            if(!$row[$cols['intervalo3']] || !$row[$cols['precio3']]) { $row[$key_errores] = "Intervalo2 esta vacio"; $errores[] = $row; continue; }

            $extension = $this->check_extension(TMP_IMG_PATH.$row[$cols['refPaqueteProducto']]);
            $image = "";
            if($extension) {
                $image = $this->load_image(TMP_IMG_PATH.$row[$cols['refPaqueteProducto']].".".$extension);
            }

            // if($onlyStock) {
            //     $PaqueteProducto = Model::factory("PaquetesProducto")->where('refPaqueteProducto',$row[$cols['refPaqueteProducto']])->find_one();
            //     if(!$PaqueteProducto) {
            //         continue;
            //     }
            //     continue;
            // }
            $producto = null;
            $PaqueteProducto = Model::factory("PaquetesProducto")->where('refPaqueteProducto',$row[$cols['refPaqueteProducto']])->find_one();
            if($PaqueteProducto) {
                $producto = $PaqueteProducto->producto(); // Model::factory('Productos')->where('nombreProducto',$row[$cols['nombreProducto']])->find_one();
            } else {
                if($onlyStock) {
                    $row[$key_errores] = "Referencia ".$row[$cols['refPaqueteProducto']]." no Existe"; $errores[] = $row; continue;
                }
                $PaqueteProducto = Model::factory("PaquetesProducto")->create();
                $PaqueteProducto->refPaqueteProducto = $row[$cols['refPaqueteProducto']];
                $producto = Model::factory('Productos')->where('nombreProducto',$row[$cols['nombreProducto']])->find_one();
            }
            if (!$producto && !$onlyStock) { $producto = Model::factory('Productos')->create(); }
            if (!$producto && $onlyStock) {
                $row[$key_errores] = "Producto ".$row[$cols['refPaqueteProducto']]." no Existe"; $errores[] = $row; continue;
            }
            if(@$cols['ACCION']) {
                if(in_array($row[$cols['ACCION']], array('X','x'))) {
                    $producto->activeProducto = false;
                    $producto->save();
                    continue;
                }
            }

            $producto->nombreProducto = $row[$cols['nombreProducto']];
            $producto->descripcionProducto = $row[$cols['descripcionProducto']];
            $producto->medidaProducto = $row[$cols['medidaProducto']];
            $producto->materialProducto = $row[$cols['materialProducto']];
            $producto->nombreFormaProducto = ($cols['nombreFormaProducto']) ? $row[$cols['nombreFormaProducto']] : $row[$cols['nombreForma']];
            $producto->colorProducto = $row[$cols['colorProducto']];
            // echo $row[$cols['novedadProducto']];
            if(isset($row[$cols['novedadProducto']])) {
                if(in_array((string) $row[$cols['novedadProducto']], array("VERDADERO","TRUE","1"))) {
                    // echo "1";
                    $producto->novedadProducto = 1;
                }
                if(in_array((string) $row[$cols['novedadProducto']], array("FALSO","FALSE","0"))) {
                    // echo "0";
                    $producto->novedadProducto = 0;
                }
            }
            $producto->descuentoProducto = $row[$cols['descuentoProducto']];
            $producto->idSeccion = $row[$cols['idSeccion']];

            if($row[$cols['nombrePeso']]) { $producto->idPeso = $Pesos[$row[$cols['nombrePeso']]]->idPeso; }
            $producto->set_expr('fechaProducto','NOW()');
            
            if(!$producto->save()) {
                $row[$key_errores] = "Error al salvar"; $errores[] = $row; continue;
            }

            if($extension) {
                $imagen = $this->image($producto->idProducto,$PaqueteProducto->refPaqueteProducto,$producto->imagenProducto);
                if(!isset($imagen['error'])) {
                    $producto->imagenProducto = $imagen['imagen'];
                    $producto->save();
                }
                if(isset($imagen['warning'])) {
                    $row[$key_errores] = $imagen['warning']; $errores[] = $row; continue;
                }
            }
            $this->extra_images($producto->idProducto, $PaqueteProducto->refPaqueteProducto);
            $this->extra_swf($producto->idProducto, $PaqueteProducto->refPaqueteProducto);
            // echo 'salio';die();

            if($idIdioma) {
                $idiomaProducto = Model::factory('ProductosTraduccion')->where('idProducto',$producto->idProducto)->where('idIdioma',$idIdioma)->find_one();
                if(!$idiomaProducto) {
                    $idiomaProducto = Model::factory("ProductosTraduccion")->create();
                }
                $idiomaProducto->idProducto = $producto->idProducto;
                $idiomaProducto->idIdioma = $idIdioma;

                $idiomaProducto->nombreProducto = ($row[$cols['nombreProducto-'.$campoIdioma]]) ? $row[$cols['nombreProducto-'.$campoIdioma]] : $row[$cols['nombreProducto']];
                $idiomaProducto->descripcionProducto = ($row[$cols['descripcionProducto-'.$campoIdioma]]) ? $row[$cols['descripcionProducto-'.$campoIdioma]] : $row[$cols['descripcionProducto']];
                $idiomaProducto->materialProducto = ($row[$cols['materialProducto-'.$campoIdioma]]) ? $row[$cols['materialProducto-'.$campoIdioma]] : $row[$cols['materialProducto']];
                $idiomaProducto->nombreFormaProducto = ($row[$cols['nombreFormaProducto-'.$campoIdioma]]) ? $row[$cols['nombreFormaProducto-'.$campoIdioma]] : $row[$cols['nombreFormaProducto']];
                $idiomaProducto->colorProducto = ($row[$cols['colorProducto-'.$campoIdioma]]) ? $row[$cols['colorProducto-'.$campoIdioma]] : $row[$cols['colorProducto']];
                $idiomaProducto->save();
            }

            $PaqueteProducto->idProducto = $producto->idProducto;
            $PaqueteProducto->idPaquete = $Paquetes[$row[$cols['nombrePaquete']]]->idPaquete;
            $PaqueteProducto->idPresentacion = $Presentaciones[$row[$cols['nombrePresentacion']]]->idPresentacion;
            $PaqueteProducto->refPaqueteProducto = $row[$cols['refPaqueteProducto']];
            $PaqueteProducto->intervalo1 = $row[$cols['intervalo1']];
            $PaqueteProducto->precio1 = $row[$cols['precio1']];
            $PaqueteProducto->intervalo2 = $row[$cols['intervalo2']];
            $PaqueteProducto->precio2 = $row[$cols['precio2']];
            $PaqueteProducto->intervalo3 = $row[$cols['intervalo3']];
            $PaqueteProducto->precio3 = $row[$cols['precio3']];
            if(@$cols['stock'] || @$cols['stockPaqueteProducto']) {
                if(@$cols['stockPaqueteProducto']) {
                    if(is_numeric($row[$cols['stockPaqueteProducto']])) {
                        if($row[$cols['stockPaqueteProducto']] < 0) { $row[$cols['stockPaqueteProducto']]; }
                        $PaqueteProducto->stockPaqueteProducto = $row[$cols['stockPaqueteProducto']];
                    }
                }
                if(@$cols['stock']) {
                    if(is_numeric($row[$cols['stock']])) {
                        if($row[$cols['stock']] < 0) { $row[$cols['stock']]; }
                        $PaqueteProducto->stockPaqueteProducto = $row[$cols['stock']];
                    }
                }
            }
            if(@$cols['minStock']) { $PaqueteProducto->minStock = $row[$cols['minStock']]; }
            if(@$cols['barcode']) { $PaqueteProducto->barcode = $row[$cols['barcode']]; }
            if(@$cols['costo']) { $PaqueteProducto->costo = $row[$cols['costo']]; }
            if(@$cols['tarifaIVA']) { $PaqueteProducto->tarifaIVA = $row[$cols['tarifaIVA']]; }
            if(@$cols['GrupoDIAN'] || @$cols['grupoDIAN']) { $PaqueteProducto->grupoDIAN = (@$cols['grupoDIAN']) ? $row[$cols['grupoDIAN']] : $row[$cols['GrupoDIAN']]; }
            if(@$cols['tipo']) { $PaqueteProducto->tipo = $row[$cols['tipo']]; }
            if(@$cols['nombreCategoria']) { $PaqueteProducto->nombreCategoria = $row[$cols['nombreCategoria']]; }
            if(@$cols['ubicacion']) { $PaqueteProducto->ubicacion = $row[$cols['ubicacion']]; }
            if(@$cols['EsMov']) {
                $PaqueteProducto->EsMov = ($row[$cols['EsMov']] == "VERDADERO") ? 1 : 0;
            }
            if(@$cols['EsIVA']) {
                $PaqueteProducto->EsIVA = ($row[$cols['EsIVA']] == "VERDADERO") ? 1 : 0;
            }
            if(@$cols['ASOCIADO']) { if(@$row[$cols['ASOCIADO']]) { $PaqueteProducto->grupo_precios = trim($row[$cols['ASOCIADO']]); }}
            if(@$cols['ANCHO']) { if(@$row[$cols['ANCHO']]) { $PaqueteProducto->ancho = @$row[$cols['ANCHO']]; }}
            if(@$cols['ALTO']) { if(@$row[$cols['ALTO']]) { $PaqueteProducto->alto = @$row[$cols['ALTO']]; }}
            if(@$cols['LARGO']) { if(@$row[$cols['LARGO']]) { $PaqueteProducto->largo = @$row[$cols['LARGO']]; }}
            if(@$cols['PESO']) { if(@$row[$cols['PESO']]) { $PaqueteProducto->peso = @$row[$cols['PESO']]; }}
            $PaqueteProducto->save();
        }
        // die();
        return $errores;
    }


    public function importCsv($inputFileName,$onlyStock = false) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 600);
        ini_set("auto_detect_line_endings", true);

        $Pesos = $this->associate_by(Model::factory('Pesos')->find_many(),"nombrePeso");
        $Secciones = $this->associate_by(Model::factory('Secciones')->find_many(),"idSeccion");
        $Presentaciones = $this->associate_by(Model::factory('Presentaciones')->find_many(),"nombrePresentacion");
        $Paquetes = $this->associate_by(Model::factory('Paquetes')->find_many(),"nombrePaquete");
        $Idiomas = $this->associate_by(Model::factory('Idiomas')->find_many(),"nombreIdioma");


        $csv = array();
        $row = 0;
        if (($handle = fopen($inputFileName, "r")) !== FALSE) {
            $data = fgets($handle, 4096);
            $csv = explode(';', $data);
            if(count($csv) > 1) {
                $delimiter = ';';
            } else {
                $csv = explode(',', $data);
                if(count($csv) > 1) {
                    $delimiter = ',';
                } else {
                    $csv = explode("\t", $data);
                    if(count($csv) > 1) {
                        $delimiter = "\t";
                    } else {
                        return array(array('Error en archivo'));
                    }
                }
            }
            // while (($data = fgetcsv($handle, 4096, $delimiter)) !== FALSE) {
            //     $num = count($data);
            //     echo "<p> $num fields in line $row: <br /></p>\n";
            //     $row++;
            //     for ($c=0; $c < $num; $c++) {
            //         echo $data[$c] . "<br />\n";
            //     }
            // }
            // fclose($handle);
        } else {
            return array(array('Error en archivo'));
        }

        foreach ($csv as $key => $value) {
            if(!$value) {
                unset($csv[$key]);
            }
        }
        $cols = array_flip($csv);
        $campoIdioma = false;
        $idIdioma = false;
        foreach ($cols as $col => $id) {
            foreach ($Idiomas as $idioma => $value) {
                if(strpos($col, '-'.$idioma)) {
                    $campoIdioma = $idioma;
                    $idIdioma = $value->idIdioma;
                    break 2;
                }
            };
        }

        $required = array('refPaqueteProducto','nombreProducto','nombrePeso','idSeccion','nombrePresentacion');
        $checknull = array('refPaqueteProducto','nombreProducto','nombrePeso','idSeccion','nombrePresentacion');
        $titulos = $csv;
        $keys = array_keys($titulos);
        $last_key = array_pop($keys); // key( array_slice( $errores, -1, 1, TRUE ) ); //
        $key_errores = ++$last_key;
        $errores = array();
        //echo json_encode(array("error"=>0,"message"=>$key_errores)); die();
        $csv[$key_errores] = 'Errores';
        $errores[] = $csv;
        $this->validate_rows($cols,$errores,$required);


        $isFirst = true;
        $cont = 0;
        // foreach ($sheetData as $row) {
        while (($row = fgetcsv($handle, 4096, $delimiter)) !== FALSE) {
            if ($isFirst) {
                $isFirst = false;
                continue;
            }
            $cont++;
            // echo 'por aca';
            $percent = $cont/(count($sheetData)-1);
            $params = array('row' => $cont, 'total' => count($sheetData)-1);
            $this->execXhr($percent,$params);

            if($row[$cols['nombrePresentacion']] == 'paq') { $row[$cols['nombrePresentacion']] = 'Paquete'; }
            if($row[$cols['nombrePresentacion']] == 'Paq') { $row[$cols['nombrePresentacion']] = 'Paquete'; }
            if($row[$cols['nombrePresentacion']] == 'PAQ') { $row[$cols['nombrePresentacion']] = 'Paquete'; }
            if($row[$cols['nombrePresentacion']] == 'Rll') { $row[$cols['nombrePresentacion']] = 'Rollo'; }
            if($row[$cols['nombrePresentacion']] == 'Unid') { $row[$cols['nombrePresentacion']] = 'Unidades'; }
            if($row[$cols['nombrePaquete']]) {
                $paq = explode(' ', $row[$cols['nombrePaquete']]);
                $row[$cols['nombrePaquete']] = $paq[0];
            }

            // // if($cont == 4173 || $cont == 4174) {
            //     echo $cont.'<br>';
            //     echo '-';
            //     echo $cols['nombrePaquete'];
            //     echo $row[$cols['nombrePaquete']];
            //     var_dump($row);
            // // }
            // echo '-<br>';

            // if($onlyStock) {
            //     $PaqueteProducto = Model::factory("PaquetesProducto")->where('refPaqueteProducto',$row[$cols['refPaqueteProducto']])->find_one();
            //     if(!$PaqueteProducto) {
            //         continue;
            //     }
            //     if(@$cols['stock'] || @$cols['stockPaqueteProducto']) {
            //         if(@$cols['stockPaqueteProducto']) {
            //             if($row[$cols['stockPaqueteProducto']]) {
            //                 $PaqueteProducto->stockPaqueteProducto = $row[$cols['stockPaqueteProducto']];
            //             }
            //         }
            //         if(@$cols['stock']) {
            //             if($row[$cols['stock']]) {
            //                 $PaqueteProducto->stockPaqueteProducto = $row[$cols['stock']];
            //             }
            //         }
            //     }
            //     $PaqueteProducto->save();
            //     continue;
            // }

// echo $row[$cols['idSeccion']].'<br>';
            if(!array_key_exists($row[$cols['nombrePeso']], $Pesos) && $row[$cols['nombrePeso']]) { $row[$key_errores] = "Nombre Peso ".$row[$cols['nombrePeso']]." no existe"; $errores[] = $row; continue; }
            if(!array_key_exists(intval($row[$cols['idSeccion']]), $Secciones) || !intval($row[$cols['idSeccion']])) { $row[$key_errores] = "Seccion ".$row[$cols['idSeccion']]." no existe"; $errores[] = $row; continue; }
            if(count($Secciones[$row[$cols['idSeccion']]]->childs())) { $row[$key_errores] = "seccion ".$row[$cols['idSeccion']]." tiene subcategorias"; $errores[] = $row; continue; }
            if(!array_key_exists($row[$cols['nombrePresentacion']], $Presentaciones)) { $row[$key_errores] = "Presentacion ".$row[$cols['nombrePresentacion']]." no existe"; $errores[] = $row; continue; }
            //Si la intervalo1 2 o 3 tienen caracteres no validos
            // if(!$row[$cols['nombrePresentacion']], $Presentaciones)) { $row[$key_errores] = "Presentacion ".$row[$cols['nombrePresentacion']]." no existe"; $errores[] = $row; continue; }
            if(!array_key_exists($row[$cols['nombrePaquete']], $Paquetes)) { $row[$key_errores] = "Paquete ".$row[$cols['nombrePaquete']]." invalido"; $errores[] = $row; continue; }

            if(!$row[$cols['intervalo1']] || !$row[$cols['precio1']]) { $row[$key_errores] = "Intervalo1 esta vacio"; $errores[] = $row; continue; }
            if(!$row[$cols['intervalo2']] || !$row[$cols['precio2']]) { $row[$key_errores] = "Intervalo2 esta vacio"; $errores[] = $row; continue; }
            if(!$row[$cols['intervalo3']] || !$row[$cols['precio3']]) { $row[$key_errores] = "Intervalo2 esta vacio"; $errores[] = $row; continue; }



            $extension = $this->check_extension(TMP_IMG_PATH.$row[$cols['refPaqueteProducto']]);
            $image = "";
            if($extension) {
                $image = $this->load_image(TMP_IMG_PATH.$row[$cols['refPaqueteProducto']].".".$extension);
            }

            $producto = null;
            $PaqueteProducto = Model::factory("PaquetesProducto")->where('refPaqueteProducto',$row[$cols['refPaqueteProducto']])->find_one();
            if($PaqueteProducto) {
                $producto = $PaqueteProducto->producto(); // Model::factory('Productos')->where('nombreProducto',$row[$cols['nombreProducto']])->find_one();
            } else {
                if($onlyStock) {
                    $row[$key_errores] = "Referencia ".$row[$cols['refPaqueteProducto']]." no Existe"; $errores[] = $row; continue;
                }
                $PaqueteProducto = Model::factory("PaquetesProducto")->create();
                $producto = Model::factory('Productos')->where('nombreProducto',$row[$cols['nombreProducto']])->find_one();
            }
            if (!$producto && !$onlyStock) { $producto = Model::factory('Productos')->create(); }
            if (!$producto && $onlyStock) {
                $row[$key_errores] = "Producto ".$row[$cols['refPaqueteProducto']]." no Existe"; $errores[] = $row; continue;
            }
            if(@$cols['ACCION']) {
                if(in_array($row[$cols['ACCION']], array('X','x'))) {
                    $producto->activeProducto = false;
                    $producto->save();
                    continue;
                }
            }

            // $producto = Model::factory('Productos')->where('nombreProducto',$row[$cols['nombreProducto']])->find_one();
            if (!$producto) { $producto = Model::factory('Productos')->create(); }
            $producto->nombreProducto = $row[$cols['nombreProducto']];
            $producto->descripcionProducto = $row[$cols['descripcionProducto']];
            $producto->medidaProducto = $row[$cols['medidaProducto']];
            $producto->materialProducto = $row[$cols['materialProducto']];
            $producto->nombreFormaProducto = ($cols['nombreFormaProducto']) ? $row[$cols['nombreFormaProducto']] : $row[$cols['nombreForma']];
            $producto->colorProducto = $row[$cols['colorProducto']];
            if(isset($row[$cols['novedadProducto']])) {
                if(in_array((string) $row[$cols['novedadProducto']], array("VERDADERO","TRUE","1"))) {
                    // echo "1";
                    $producto->novedadProducto = 1;
                }
                if(in_array((string) $row[$cols['novedadProducto']], array("FALSO","FALSE","0"))) {
                    // echo "0";
                    $producto->novedadProducto = 0;
                }
            }
            $producto->descuentoProducto = $row[$cols['descuentoProducto']];
            $producto->idSeccion = $row[$cols['idSeccion']];

            if($row[$cols['nombrePeso']]) { $producto->idPeso = $Pesos[$row[$cols['nombrePeso']]]->idPeso; }
            $producto->set_expr('fechaProducto','NOW()');
            
            if(!$producto->save()) {
                $row[$key_errores] = "Error al salvar"; $errores[] = $row; continue;
            }

            //Para guardar las imagenes (redimensionando y con extras)
            if($extension) {
                $imagen = $this->image($producto->idProducto,$PaqueteProducto->refPaqueteProducto,$producto->imagenProducto);
                if(!isset($imagen['error'])) {
                    $producto->imagenProducto = $imagen['imagen'];
                }
                if(isset($imagen['warning'])) {
                    $row[$key_errores] = $imagen['warning']; $errores[] = $row; // continue;
                }
                $producto->save();
            }
            $this->extra_images($producto->idProducto, $PaqueteProducto->refPaqueteProducto);
            $this->extra_swf($producto->idProducto, $PaqueteProducto->refPaqueteProducto);

            if($idIdioma) {
                $idiomaProducto = Model::factory('ProductosTraduccion')->where('idProducto',$producto->idProducto)->where('idIdioma',$idIdioma)->find_one();
                if(!$idiomaProducto) {
                    $idiomaProducto = Model::factory("ProductosTraduccion")->create();
                }
                $idiomaProducto->idProducto = $producto->idProducto;
                $idiomaProducto->idIdioma = $idIdioma;

                $idiomaProducto->nombreProducto = ($row[$cols['nombreProducto-'.$campoIdioma]]) ? $row[$cols['nombreProducto-'.$campoIdioma]] : $row[$cols['nombreProducto']];
                $idiomaProducto->descripcionProducto = ($row[$cols['descripcionProducto-'.$campoIdioma]]) ? $row[$cols['descripcionProducto-'.$campoIdioma]] : $row[$cols['descripcionProducto']];
                $idiomaProducto->materialProducto = ($row[$cols['materialProducto-'.$campoIdioma]]) ? $row[$cols['materialProducto-'.$campoIdioma]] : $row[$cols['materialProducto']];
                $idiomaProducto->nombreFormaProducto = ($row[$cols['nombreFormaProducto-'.$campoIdioma]]) ? $row[$cols['nombreFormaProducto-'.$campoIdioma]] : $row[$cols['nombreFormaProducto']];
                $idiomaProducto->colorProducto = ($row[$cols['colorProducto-'.$campoIdioma]]) ? $row[$cols['colorProducto-'.$campoIdioma]] : $row[$cols['colorProducto']];
                $idiomaProducto->save();
            }

            // $PaqueteProducto = Model::factory("PaquetesProducto")->where('refPaqueteProducto',$row[$cols['refPaqueteProducto']])->find_one();
            // if(!$PaqueteProducto) {
            //     $PaqueteProducto = Model::factory("PaquetesProducto")->create();
            // }
            $PaqueteProducto->idProducto = $producto->idProducto;
            $PaqueteProducto->idPaquete = $Paquetes[$row[$cols['nombrePaquete']]]->idPaquete;
            $PaqueteProducto->idPresentacion = $Presentaciones[$row[$cols['nombrePresentacion']]]->idPresentacion;
            $PaqueteProducto->refPaqueteProducto = $row[$cols['refPaqueteProducto']];
            $PaqueteProducto->intervalo1 = $row[$cols['intervalo1']];
            $PaqueteProducto->precio1 = $row[$cols['precio1']];
            $PaqueteProducto->intervalo2 = $row[$cols['intervalo2']];
            $PaqueteProducto->precio2 = $row[$cols['precio2']];
            $PaqueteProducto->intervalo3 = $row[$cols['intervalo3']];
            $PaqueteProducto->precio3 = $row[$cols['precio3']];

            if(@$cols['stock'] || @$cols['stockPaqueteProducto']) {
                if(@$cols['stockPaqueteProducto']) {
                    if(is_numeric($row[$cols['stockPaqueteProducto']])) {
                        if($row[$cols['stockPaqueteProducto']] < 0) { $row[$cols['stockPaqueteProducto']]; }
                        $PaqueteProducto->stockPaqueteProducto = $row[$cols['stockPaqueteProducto']];
                    }
                }
                if(@$cols['stock']) {
                    if(is_numeric($row[$cols['stock']])) {
                        if($row[$cols['stock']] < 0) { $row[$cols['stock']]; }
                        $PaqueteProducto->stockPaqueteProducto = $row[$cols['stock']];
                    }
                }
            }

            if(@$cols['minStock']) { $PaqueteProducto->minStock = $row[$cols['minStock']]; }
            if(@$cols['barcode']) { $PaqueteProducto->barcode = $row[$cols['barcode']]; }
            if(@$cols['costo']) { $PaqueteProducto->costo = $row[$cols['costo']]; }
            if(@$cols['tarifaIVA']) { $PaqueteProducto->tarifaIVA = $row[$cols['tarifaIVA']]; }
            if(@$cols['GrupoDIAN'] || @$cols['grupoDIAN']) { $PaqueteProducto->grupoDIAN = (@$cols['grupoDIAN']) ? $row[$cols['grupoDIAN']] : $row[$cols['GrupoDIAN']]; }
            if(@$cols['tipo']) { $PaqueteProducto->tipo = $row[$cols['tipo']]; }
            if(@$cols['nombreCategoria']) { $PaqueteProducto->nombreCategoria = $row[$cols['nombreCategoria']]; }
            if(@$cols['ubicacion']) { $PaqueteProducto->ubicacion = $row[$cols['ubicacion']]; }
            if(@$cols['EsMov']) {
                $PaqueteProducto->EsMov = ($row[$cols['EsMov']] == "VERDADERO") ? 1 : 0;
            }
            if(@$cols['EsIVA']) {
                $PaqueteProducto->EsIVA = ($row[$cols['EsIVA']] == "VERDADERO") ? 1 : 0;
            }
            if(@$cols['ASOCIADO']) { $PaqueteProducto->grupo_precios = trim($cols['ASOCIADO']); }
            if(@$cols['ANCHO']) { $PaqueteProducto->ancho = @$cols['ANCHO']; }
            if(@$cols['ALTO']) { $PaqueteProducto->alto = @$cols['ALTO']; }
            if(@$cols['LARGO']) { $PaqueteProducto->largo = @$cols['LARGO']; }
            if(@$cols['PESO']) { $PaqueteProducto->peso = @$cols['PESO']; }
            $PaqueteProducto->save();
        }
        fclose($handle);
        return $errores;
    }



    public function exportExcel($productos,$campoIdioma = false) {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 600);


        $objPHPExcel = new PHPExcel();

        if($campoIdioma) {
            $idioma = Model::factory('Idiomas')->where('nombreIdioma',$campoIdioma)->find_one();
            $idIdioma = $idioma->idIdioma;
        }

        //echo date('H:i:s') . " Set properties\n";
        $objPHPExcel->getProperties()->setCreator("Lak Mir");
        $objPHPExcel->getProperties()->setLastModifiedBy("Nguillers");
        $objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Products Export");
        $objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Products Export");
        $objPHPExcel->getProperties()->setDescription("Export for nguillers products.");

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->getSheetView()->setZoomScale(80);

        $sheet->setTitle('Hoja 1');

        $col = 0; // $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "idProducto"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "refPaqueteProducto"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "nombreProducto"); $col++;
        if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, 1, "nombreProducto-".$campoIdioma); $col++;}
        $sheet->setCellValueByColumnAndRow($col, 1, "medidaProducto"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "nombreCategoria"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "tipo"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "nombreFormaProducto"); $col++;
        if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, 1, "nombreFormaProducto-".$campoIdioma); $col++;}
        $sheet->setCellValueByColumnAndRow($col, 1, "idSeccion"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "colorProducto"); $col++;
        if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, 1, "colorProducto-".$campoIdioma); $col++;}
        $sheet->setCellValueByColumnAndRow($col, 1, "materialProducto"); $col++;
        if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, 1, "materialProducto-".$campoIdioma); $col++;}
        $sheet->setCellValueByColumnAndRow($col, 1, "nombrePresentacion"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "nombrePaquete"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "nombrePeso"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "minStock"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "ubicacion"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "costo"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "precio1"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "intervalo1"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "precio2"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "intervalo2"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "precio3"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "intervalo3"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "tarifaIVA"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "barcode"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "descripcionProducto"); $col++;
        if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, 1, "descripcionProducto-".$campoIdioma); $col++;}
        $sheet->setCellValueByColumnAndRow($col, 1, "novedadProducto"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "descuentoProducto"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "stockPaqueteProducto"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "EsMov"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "EsIVA"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "grupoDIAN"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "ASOCIADO"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "ANCHO"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "ALTO"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "LARGO"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "PESO"); $col++;
        $sheet->setCellValueByColumnAndRow($col, 1, "ACCION"); $col++;
        //$sheet->setCellValueByColumnAndRow($col, 1, "ASOCIADO"); $col++;

        $row = 1;
        foreach ($productos as $producto) {
            $paquetes = $producto->paquetes();
            if(count($paquetes)) {
                foreach ($paquetes as $paquete) {
                    if($campoIdioma) {
                    $idioma = Model::factory('ProductosTraduccion')->where('idProducto',$producto->idProducto)->where('idIdioma',$idIdioma)->find_one();
                        if(!$idioma) {
                            $idioma = Model::factory('ProductosTraduccion')->create();
                            $idioma->idProducto = $producto->idProducto;
                            $idioma->idIdioma = $idIdioma;
                        }
                    }

                    $col = 0; // $col++;
                    $row++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->idProducto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->refPaqueteProducto);  $col++;

                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->nombreProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->nombreProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->medidaProducto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++; //nombreCategoria
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++; //tipo
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->nombreFormaProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->nombreFormaProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->idSeccion);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->colorProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->colorProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->materialProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->materialProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->presentacion()->nombrePresentacion);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->paquete()->nombrePaquete);  $col++;

                    $nombrePeso = ($producto->peso()) ? $producto->peso()->nombrePeso : $producto->idPeso;
                    $sheet->setCellValueByColumnAndRow($col, $row, $nombrePeso);  $col++;
                    if(is_numeric($nombrePeso)) {
                        $sheet->getStyle('M'.$row)
                            ->getFill()
                            ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => 'FF0000')
                                ));
                            // ->getStartColor()
                            // ->setRGB('FF0000');
                        //$sheet->setCellValueByColumnAndRow($col, $row, $nombrePeso);  $col++;
                    }

                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->minStock);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->ubicacion);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->costo);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->precio1);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->intervalo1);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->precio2);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->intervalo2);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->precio3);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->intervalo3);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->tarifaIVA);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->barcode);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->descripcionProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->descripcionProducto); $col++;}
                    $novedadProducto = ($producto->novedadProducto) ? "VERDADERO" : "FALSO";
                    $sheet->setCellValueByColumnAndRow($col, $row, $novedadProducto);  $col++;
                    $descuentoProducto = ($producto->descuentoProducto) ? $producto->descuentoProducto : 0;
                    $sheet->setCellValueByColumnAndRow($col, $row, $descuentoProducto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->stockPaqueteProducto);  $col++;
                    $EsMov = ($paquete->EsMov) ? "VERDADERO" : "FALSO";
                    $sheet->setCellValueByColumnAndRow($col, $row, $EsMov);  $col++;
                    $EsIVA = ($paquete->EsIVA) ? "VERDADERO" : "FALSO";
                    $sheet->setCellValueByColumnAndRow($col, $row, $EsIVA);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->grupoDIAN);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->grupo_precios);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->ancho);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->alto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->largo);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $paquete->peso);  $col++;
                }
            } else {
                    if($campoIdioma) {
                    $idioma = Model::factory('ProductosTraduccion')->where('idProducto',$producto->idProducto)->where('idIdioma',$idIdioma)->find_one();
                        if(!$idioma) {
                            $idioma = Model::factory('ProductosTraduccion')->create();
                            $idioma->idProducto = $producto->idProducto;
                            $idioma->idIdioma = $idIdioma;
                        }
                    }

                    $col = 0;
                    $row++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->idProducto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;

                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->nombreProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->nombreProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->medidaProducto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++; //nombreCategoria
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++; //tipo
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->nombreFormaProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->nombreFormaProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->idSeccion);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->colorProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->colorProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->materialProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->materialProducto); $col++;}
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->peso()->nombrePeso);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, $producto->descripcionProducto);  $col++;
                    if($campoIdioma) {$sheet->setCellValueByColumnAndRow($col, $row, $idioma->descripcionProducto); $col++;}
                    $novedadProducto = ($producto->novedadProducto) ? "VERDADERO" : "FALSO";
                    $sheet->setCellValueByColumnAndRow($col, $row, $novedadProducto);  $col++;
                    $descuentoProducto = ($producto->descuentoProducto) ? $producto->descuentoProducto : 0;
                    $sheet->setCellValueByColumnAndRow($col, $row, $descuentoProducto);  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
                    $sheet->setCellValueByColumnAndRow($col, $row, "");  $col++;
            }
        }
        if($campoIdioma) {
            $sheet->getColumnDimension('C')->setWidth(50);
            $sheet->getColumnDimension('D')->setWidth(50);
            $sheet->getColumnDimension('AC')->setWidth(50);
            $sheet->getColumnDimension('AD')->setWidth(50);
        } else {
            $sheet->getColumnDimension('C')->setWidth(50);
            $sheet->getColumnDimension('Y')->setWidth(50);
        }
        //$objPHPExcel->getActiveSheet()->getStyle('L3:N2048')                              ->getNumberFormat()->setFormatCode('0000');
        return $objPHPExcel;

    }
}
