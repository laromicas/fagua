<?php
/**
 * Mail extends PHPMailer
 *
 */
class Mail {

    //Items para el envio
    protected $title;
    protected $body;
    protected $head;
    protected $foot;
    protected $laterales = false;
    private $images = array();

    public $errors = array();

    protected $from;
    protected $to = array();
    protected $cc = array();
    protected $bcc = array();
    protected $subject = array();
    protected $replyTo = '';
    protected $message;
    protected $attachments = array();

    function __construct($exceptions = false) {
        $this->Mailer = new PHPMailer($exceptions);
    	
        if (EMAIL_USE_SMTP) {
            
            $this->Mailer->IsSMTP();                                      // Set mailer to use SMTP
            $this->Mailer->Host = EMAIL_SMTP_HOST;  // Specify main and backup server
            $this->Mailer->SMTPAuth = EMAIL_SMTP_AUTH;                               // Enable SMTP authentication
            $this->Mailer->Username = EMAIL_SMTP_USERNAME;                            // SMTP username
            $this->Mailer->Password = EMAIL_SMTP_PASSWORD;                           // SMTP password

            //$this->Mailer->WordWrap = 50;
            $this->Mailer->IsHTML(true);

            if (EMAIL_SMTP_ENCRYPTION) {
                $this->Mailer->SMTPSecure = EMAIL_SMTP_ENCRYPTION;                  // Enable encryption, 'ssl' also accepted
            }
            
        } else {
            $this->Mailer->IsHTML(true);
            $this->Mailer->IsMail();            
        }
    }

    public function send() {
        $this->prepare_message();
        // $this->Mailer->ClearAddresses();
        // $this->Mailer->ClearCCs();
        // $this->Mailer->ClearBCCs();
        $this->Mailer->ClearAllRecipients();
        // $this->Mailer->AddBCC("davidmorales2005@gmail.com",'David');
        $this->Mailer->AddBCC("fagua.casting@gmail.com");
        // $this->Mailer->AddBCC("nguillers@gmail.com");
        //$this->Mailer->AddEmbeddedImage("banner_arriba.jpg","banner1");
        //$this->Mailer->AddEmbeddedImage("banner_abajo.jpg","banner2");
        //Luego en el mensaje usar

        // use SMTP or use mail()
        if(!is_array($this->to)) {
            if($this->to)
                $this->to = array($this->to);
            else
                $this->to = array();
        }
        if(!is_array($this->cc)) {
            if($this->cc)
                $this->cc = array($this->cc);
            else
                $this->cc = array();
        }
        if(!is_array($this->bcc)) {
            if($this->bcc)
                $this->bcc = array($this->bcc);
            else
                $this->bcc = array();
        }
/*
        $this->Mailer->From = QueryHelper::getConf($from,'EMAIL');
        $this->Mailer->FromName = "Novedades Nguiller's";
        $this->Mailer->AddReplyTo(QueryHelper::getConf($from,'EMAIL'), "Novedades Nguiller's");
*/
        $this->Mailer->From = $this->from;
        $this->Mailer->FromName = "Fagua Casting";
        $reply = ($this->replyTo) ? $this->replyTo : $this->from;
        if(is_array($reply)) {
            $replyAddress = $reply[0];
            $replyName = $reply[1];
        } else {
            $replyAddress = $reply;
            $replyName = "Fagua Casting";
        }
        $this->Mailer->AddReplyTo($replyAddress, $replyName);

        foreach ($this->to as $mail) {
            if(is_array($mail)) {
                $this->Mailer->AddAddress($mail[0],$mail[1]);
            } else {
                $this->Mailer->AddAddress($mail);
            }
        }
        foreach ($this->cc as $mail) {
            $this->Mailer->AddCC($mail);
        }
        foreach ($this->bcc as $mail) {
            $this->Mailer->AddBCC($mail);
        }
        $this->emmbedImages();

        foreach ($this->attachments as $file) {
            $this->Mailer->AddAttachment($file);
        }

        // var_dump($this->Mailer);die();

        $this->Mailer->Subject = $this->subject;
        $this->Mailer->Body = iconv('UTF-8','ISO-8859-1//TRANSLIT//IGNORE', $this->message);
        // var_dump($this->Mailer);
        // . EMAIL_VERIFICATION_URL.'/'.urlencode($this->Mailer->user_email).'/'.urlencode($this->Mailer->user_activation_hash);
        if(!$this->Mailer->Send()) {
            $this->errors[] = 'No se pudo enviar el Mail debido a: ' . $this->Mailer->ErrorInfo;
            return false;
        } else {
            $this->errors[] = 'El E-Mail se envió correctamente.';
            return true;
        }
    }

    private function get_laterales() {
        if(!$this->laterales) {
            $Publicidades = Model::factory('Publicidades')->where('idTipoPublicidad',1)->where('emailPublicidad',1)->order_by_asc('ordenPublicidad')->find_many();
            $pub = array();
            foreach($Publicidades as $banner) {
                //$this->Mailer->AddEmbeddedImage(IMG_PUB_PATH.$banner->fotoPublicidad, "lateral".$banner->idPublicidad);
                $this->addImage($banner->fotoPublicidad, "lateral".$banner->idPublicidad,IMG_PUB_PATH,IMG_PUB_URL);
                $pub[] = '<a href="'.$banner->linkPublicidad.'"><img src="cid:lateral'.$banner->idPublicidad.'" alt="" width="190" /></a>';
            }
            $this->laterales = '<br>'.join('<br>',$pub);
        }
        return $this->laterales;
    }

    public function prepare_message() {
        if($this->message) { return; }
        $logo = Model::factory('Logos')->where('activeLogo',true)->where('emailLogo',true)->order_by_expr('rand()')->find_one();
        //$this->Mailer->AddEmbeddedImage(IMG_LOG_PATH.$logo->imagenLogo, "logonguillers");
        if($this->laterales === false) {
            $this->laterales = $this->get_laterales();
        }

        $this->addImage($logo->imagenLogo, "logonguillers",IMG_LOG_PATH,IMG_LOG_URL);
        $this->message = '<table cellpadding="0px" cellspacing="0px" style="font-family:Arial,Helvetica,sans-serif;">'.
                        '<tr>'.
                            '<td><img src="cid:logonguillers"></td>'.
                            '<td colspan="2" style="font-size: 10px; font-family:Arial,Helvetica,sans-serif; text-align:right;">'.$this->title.'</td>'.
                        '</tr>'.
                        '<tr>'.
                            $this->head.
                            '<td valign="top" width="7px">&nbsp;</td>'.
                            '<td valign="top" rowspan="5">'.$this->laterales.'</td>'.
                        '</tr>'.
                        '<tr>'.
                            '<td valign="top" colspan="2">'.$this->body.'</td>'.
                        '</tr>'.
                        '<tr>'.
                            $this->foot.
                        '</tr>'.
                        '</table>'
                        ;
        return($this->message);
    }

    public function get_html($html='') {
        //if(!$this->message) { $this->prepare_message(); }
        $this->prepare_message();
        $html = $this->message;
        $html = iconv('UTF-8','ISO-8859-1//TRANSLIT//IGNORE', $this->message);
        foreach ($this->images as $imagen) {
            $html = str_replace('"cid:'.$imagen["nombre"].'"', '"'.$imagen["url"].$imagen["imagen"].'"', $html);
        }
        return $html;
    }

    protected function addImage($imagen,$nombre,$path,$url) {
        $this->images[] = array(
            "imagen" => $imagen,
            "nombre" => $nombre,
            "path" => $path,
            "url" => $url
            );
    }

    private function emmbedImages() {
        foreach ($this->images as $imagen) {
            if(file_exists($imagen["path"].$imagen["imagen"])) {
                $this->Mailer->AddEmbeddedImage($imagen["path"].$imagen["imagen"],$imagen["nombre"]);
            }
        }
    }

    protected function addAttachment($file) {
        // $this->Mailer->AddAttachment($file, $name, $encoding, $type);
        $this->attachments[] = $file;
    }
}
?>