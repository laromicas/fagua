<?php

class Currency {

	public function getMultiplier($to,$from = 'COP') {
		//return Currency::google($from,$to);
		// print_r(array($to,$from));
		$result = Currency::yahoo($to,$from);
		return ($result) ? 1/$result : 1;
	}

	public static function yahoo($monedaIni, $monedaFin){
		$path = "http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json";
		$data = file_get_contents($path);
		$posicion=  strripos($data,$monedaIni."/".$monedaFin);
		$cambio = substr($data, $posicion+21, 11); 
		return $cambio;
	}

	public static function google($from, $to){
// require_once("json/JSON.php");
		$google_url = "http://www.google.com/ig/calculator?hl=en&q=1". $from . "=?" .$to;
		$result = file_get_contents($google_url);
		$result = explode('"', $result);

		$fromParts = explode(' ',  $result[1]);
		$toParts = explode(' ',  $result[3]);

		$return = array(
			'from' => array(
				'code' => $from,
				'amount' => Currency::cleanAmount($fromParts[0])
				),
			'to' => array(
				'code' => $to,
				'amount' => Currency::cleanAmount($toParts[0])
				)
			);
		return Currency::cleanAmount($toParts[0]);
	}

	public static function cleanAmount($str) {
		$result = '';
		for ($i = 0; $i < strlen($str); $i++) {

			$char = $str[$i];
			if (is_numeric($char)) {
				$result .= $char;
			}
			if ($char == '.' || $char == ',') {
				$result .= '.';
			}
		}
		return (float) $result;
	}

	public static function get_currency() {
		if(!isset($_COOKIE['currency'])) {
	        Cookie::set('currency','COP');
	        Cookie::set('currencyMultiplier','1');
		}
	}
}
