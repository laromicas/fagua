<?php

class Auth {
    public static function handleLogin() {
        Session::init();
        if (Session::get('user_logged_in') == false) {
            Session::set('URI',$_SERVER['REQUEST_URI']);
//            Session::destroy();
            Html::redirect_to(URL.'login/index');
            exit();
        }
    }

    public static function handleAccess($controller) {
        if (Session::get('access')) {
        	if (Session::get('access') == 'ALL' || $controller == 'inicio') {
        		return;
        	}
            $accessList = json_decode(Session::get('access'));
            $access = false;
            foreach ($accessList as $value) {
            	if($controller==$value) {
            		$access = true;
            	}
            }
            if ($access == false) {
            	Session::set('flashError','1');
            	Session::set('flash','El usuario no tiene los permisos requeridos para el acceso a este control');
            	Html::redirect_to(URL.'index');
            	exit();
            }
        }
    }

    public static function verify_access($controller) {
        if (Session::get('access')) {
        	if (Session::get('access') == 'ALL') {
        		return true;
        	}
            $accessList = json_decode(Session::get('access'));
            foreach ($accessList as $value) {
            	if($controller==$value) {
            		return true;
            	}
            }
            return false;
        }
    }

    public static function handleClientLogin() {
        Session::init();
        if (Session::get('client_logged_in') == false) {
            Session::set('URI',$_SERVER['REQUEST_URI']);
//            Session::destroy();
            Html::redirect_to(URL.'login');
            exit();
        }
    }

    public static function client_data() {
        if (Session::get('client_logged_in') == false) {
            Session::set('URI',$_SERVER['REQUEST_URI']);
        }
    }

    public static function current_client($client_data = '') {
        if($client_data) {
            Session::set('client_data',$client_data);
            $perfil = $client_data->perfil();
            if($perfil) {
                Session::set('perfil_data',$perfil);
                $idioma = $perfil->idioma();
                $moneda = $perfil->moneda();
                Session::set('valid_prices',$perfil->idPrecio - 1);
                
                if($idioma || $moneda) {
                    require_once 'controllers/language_controller.php';
                    $language = new Language_Controller();
                    if($idioma) {
                        $idio = $idioma->shortName;
                        $language->change($idio,false);
                        Cookie::set('lang',$idio);
                    }
                    if($moneda) {
                        $curr = $moneda->codigoMoneda;
                        $language->currency($curr,false);
                        Cookie::set('currency',$curr);
                    }
                }
            } else {
                Session::set('valid_prices',0);
            }
            
        } else {
            Session::get('client_data');
        }
    }

    public static function current_user($user_data = '') {
        if($user_data) {
            Session::set('user_data',$user_data);
        } else {
            return Session::get('user_data');
        }
    }

    public function verify_thing($value='')
    {
        # code...
    }
}