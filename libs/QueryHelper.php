<?php

class QueryHelper {
    public static function getDataFrom($sTable,$sIndexColumn,$aColumns,$get,$extras) {

        $tabla =  Model::factory($sTable);

        /*
         * Ordering
         */
        if ( isset( $get['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $get['iSortingCols'] ) ; $i++ )
            {
                if ( $get[ 'bSortable_'.intval($get['iSortCol_'.$i]) ] == "true" )
                {
                    $tabla = $tabla->order_by($aColumns[ intval( $get['iSortCol_'.$i] ) ],$get['sSortDir_'.$i] );
                }
            }
        }
        if (array_key_exists("unique", $extras) && @$extras["unique"]) {
            foreach ($extras["search"] as $value) {
                $tabla = $tabla->where($value, $extras["unique"]);
            }
        } else if (array_key_exists("search", $extras) && $get['sSearch'] != '' ) {
            $orData = array();
            $searchData = array();
            foreach ($extras["search"] as $value) {
                $orData[] = $value." like ? ";
                $searchData[] = '%'.$get['sSearch'].'%';
            }
            $search = join(' OR ',$orData);
            $tabla = $tabla->where_raw('('.$search.")",$searchData);
            //$tabla = $tabla->where_like($extras["search"][0],'%'.$get['sSearch'].'%');
        }
        
        if (array_key_exists("direct_search", $extras)) {
            foreach ($extras["direct_search"] as $key => $value) {
                $tabla->where_like($key,'%'.$value.'%');
            }
        }

        if (array_key_exists("filters", $extras)) {
            $filters = $extras["filters"];
            foreach ($filters as $value) {
                if(is_array($value)) {
                    switch (count($value)) {
                        case 5:
                            $tabla = $tabla->filter($value[0],$value[1],$value[2],$value[3],$value[4]);
                            break;
                        case 4:
                            $tabla = $tabla->filter($value[0],$value[1],$value[2],$value[3]);
                            break;
                        case 3:
                            $tabla = $tabla->filter($value[0],$value[1],$value[2]);
                            break;
                        case 2:
                            $tabla = $tabla->filter($value[0],$value[1]);
                            break;
                        case 1:
                            $tabla = $tabla->filter($value[0]);
                            break;
                        default:
                            $tabla = $tabla->filter($value[0]);
                            break;
                    }
                } else {
                    $tabla = $tabla->filter($value);
                }
            }
        }

        $tabTemp = $tabla;

        if ( isset( $get['iDisplayStart'] ) && $get['iDisplayLength'] != '-1' )
        {
            $tabla = $tabla->limit($get['iDisplayStart'].','.$get['iDisplayLength']);
        }


        /*
         * SQL queries
         * Get data to display
         */
        // print_r($tabla->get_query());
        $rResult = $tabla->find_many();
        $iFilteredTotal = $tabTemp->count();
        $iTotal = Model::factory($sTable)->count();
        $rCounts = array();

        if (array_key_exists("counts", $extras)) {
            $tabla_copy = $tabla;
            if(!is_array($extras["counts"])) {
                $extras["counts"] = array($extras["counts"]);
            }
            $tabla_copy->clear_selects();
            $tabla_copy->clear_limit();
            foreach ($extras["counts"] as $column) {
                $tabla_copy = $tabla_copy->select($column);
            }
            $tabla_copy = $tabla_copy->select_expr("count(".$extras["counts"][0].")","cantCol");
            $tabla_copy = $tabla_copy->group_by($extras["counts"][0]);
            //print_r($tabla_copy->get_query());
            $rCounts = $tabla_copy->find_array();
        }

        $rDistincts = array();
        if (array_key_exists("distincts", $extras)) {
            if(!is_array($extras["distincts"])) {
                $extras["distincts"] = array($extras["distincts"]);
            }
            foreach ($extras["distincts"] as $tbl => $col) {
                $tabla_copy = $tabla;
                $tabla_copy->clear_selects();
                $tabla_copy->clear_limit();

                if (is_numeric($tbl)) {
                    $tabla_copy = $tabla_copy->select($col);
                } else {
                    $tabla_copy = $tabla_copy->select_expr($col,$tbl);
                }
                $col = (is_numeric($tbl)) ? $col : $tbl;
                //print_r($tabla_copy->get_query());
                $rDistincts[$col] = $tabla_copy->distinct()->find_array();
            }
        }

        $rQuery = json_encode($tabla->get_query());

        /*
         * Output
         */
        // echo $rQuery;
        $output = array(
            "sEcho" => intval($get['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "wQuery" => $rQuery,
            "aCounts" => $rCounts,
            "aDistincts" => $rDistincts,
            "aaData" => array()
        );
        foreach ($rResult as $aRow) {
            $row = array();
            foreach ($aColumns as $i => $aColumn) {
                if (array_key_exists("concat", $extras) && array_key_exists($i, $extras["concat"])) {
                    $r = array();
                    foreach ($extras["concat"][$i] as $key => $value) {
                        $r[] = $aRow->$value;
                    }
                    $row[] = join(" ",$r);
                } else {
                    //var_dump($aRow);
                    if(is_numeric($i)) {
                        $row[] = $aRow->$aColumns[$i];
                    } else {
                        $row[] = $aRow->$i;
                    }
                }
            }
            $output['aaData'][] = $row;
        }

        // foreach ($rCounts as $aRow) {
        //     # code...
        // }

        return $output;
    }


    public static function getDataForProducts($sTable,$sIndexColumn,$aColumns,$get,$extras) {

        $tabla =  Model::factory($sTable);
        /*
         * Ordering
         */
        if ( isset( $get['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $get['iSortingCols'] ) ; $i++ )
            {
                if ( $get[ 'bSortable_'.intval($get['iSortCol_'.$i]) ] == "true" )
                {
                    $tabla = $tabla->order_by($aColumns[ intval( $get['iSortCol_'.$i] ) ],$get['sSortDir_'.$i] );
                }
            }
        }
        if (array_key_exists("unique", $extras) && @$extras["unique"]) {
            foreach ($extras["search"] as $value) {
                $tabla = $tabla->where($value, $extras["unique"]);
            }
        } else if (array_key_exists("search", $extras) && $get['sSearch'] != '' ) {
            $orData = array();
            $searchData = array();
            foreach ($extras["search"] as $value) {
                $orData[] = $value." like ? ";
                $searchData[] = '%'.$get['sSearch'].'%';
            }
            if (array_key_exists("searchProducts", $extras)) {
                foreach ($extras["searchProducts"] as $key => $value) {
                    $orData[] = $value." = ? ";
                    $searchData[] = $key;
                }
            }

            $search = join(' OR ',$orData);
            $tabla = $tabla->where_raw('('.$search.")",$searchData);
            //$tabla = $tabla->where_like($extras["search"][0],'%'.$get['sSearch'].'%');
        }
        if (array_key_exists("direct_search", $extras)) {
            foreach ($extras["direct_search"] as $key => $value) {
                $tabla->where($key,$value);
                // $tabla->where_like($key,'%'.$value.'%');
            }
        }
        if (array_key_exists("filters", $extras)) {
            $filters = $extras["filters"];
            foreach ($filters as $value) {
                if(is_array($value)) {
                    switch (count($value)) {
                        case 5:
                            $tabla = $tabla->filter($value[0],$value[1],$value[2],$value[3],$value[4]);
                            break;
                        case 4:
                            $tabla = $tabla->filter($value[0],$value[1],$value[2],$value[3]);
                            break;
                        case 3:
                            $tabla = $tabla->filter($value[0],$value[1],$value[2]);
                            break;
                        case 2:
                            $tabla = $tabla->filter($value[0],$value[1]);
                            break;
                        case 1:
                            $tabla = $tabla->filter($value[0]);
                            break;
                        default:
                            $tabla = $tabla->filter($value[0]);
                            break;
                    }
                } else {
                    $tabla = $tabla->filter($value);
                }
            }
        }
        $tabTemp = $tabla;
        if ( isset( $get['iDisplayStart'] ) && $get['iDisplayLength'] != '-1' )
        {
            $tabla = $tabla->limit($get['iDisplayStart'].','.$get['iDisplayLength']);
        }

        /*
         * SQL queries
         * Get data to display
         */
        $tbl = clone $tabla;
        // print_r($tbl->get_query());die();
        $rQuery = json_encode($tbl->get_query());
        $rResult = $tabla->find_many();
        $iFilteredTotal = $tabTemp->count();
        $iTotal = Model::factory($sTable)->count();
        $rCounts = array();

        if (array_key_exists("counts", $extras)) {
            $tabla_copy = $tabla;
            if(!is_array($extras["counts"])) {
                $extras["counts"] = array($extras["counts"]);
            }
            $tabla_copy->clear_selects();
            $tabla_copy->clear_limit();
            foreach ($extras["counts"] as $column) {
                $tabla_copy = $tabla_copy->select($column);
            }
            $tabla_copy = $tabla_copy->select_expr("count(".$extras["counts"][0].")","cantCol");
            $tabla_copy = $tabla_copy->group_by($extras["counts"][0]);
            //print_r($tabla_copy->get_query());
            $rCounts = $tabla_copy->find_array();
        }

        $rDistincts = array();
        if (array_key_exists("distincts", $extras)) {
            if(!is_array($extras["distincts"])) {
                $extras["distincts"] = array($extras["distincts"]);
            }
            foreach ($extras["distincts"] as $tbl => $col) {
                $tabla_copy = $tabla;
                $tabla_copy->clear_selects();
                $tabla_copy->clear_limit();
                $tabla_copy->clear_group_by();

                if (is_numeric($tbl)) {
                    $tabla_copy = $tabla_copy->select($col);
                } else {
                    $tabla_copy = $tabla_copy->select_expr($col,$tbl);
                }
                $col = (is_numeric($tbl)) ? $col : $tbl;
                // print_r($tabla_copy->get_query());
                $rDistincts[$col] = $tabla_copy->distinct()->find_array();
            }
        }

        

        /*
         * Output
         */
        // echo $rQuery;
        $output = array(
            "sEcho" => intval($get['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "wQuery" => $rQuery,
            "aCounts" => $rCounts,
            "aDistincts" => $rDistincts,
            "aaData" => array()
        );
        foreach ($rResult as $aRow) {
            $row = array();
            foreach ($aColumns as $i => $aColumn) {
                if (array_key_exists("concat", $extras) && array_key_exists($i, $extras["concat"])) {
                    $r = array();
                    foreach ($extras["concat"][$i] as $key => $value) {
                        $r[] = $aRow->$value;
                    }
                    $row[] = join(" ",$r);
                } else {
                    //var_dump($aRow);
                    if(is_numeric($i)) {
                        $row[] = $aRow->$aColumns[$i];
                    } else {
                        $row[] = $aRow->$i;
                    }
                }
            }
            $output['aaData'][] = $row;
        }

        return $output;
    }




    public static function make_tree($idParent = '',$out = 4,$arraySelected = array(), $exclude = array())
    {
        $tree = array();
        if($idParent) {
            $Secciones = Model::factory('Secciones')->where('padreSeccion',$idParent)->order_by_asc('nombreSeccion')->find_many();
        } else {
            $Secciones = Model::factory('Secciones')->where('padreSeccion',0)->order_by_asc('nombreSeccion')->find_many();
        }
        foreach ($Secciones as $seccion) {
            if(!in_array($seccion->idSeccion, $exclude)) {
                if(count($seccion->childs()) && $out > 1){
                    //if(count($seccion->childs()) && $seccion->idSeccion && $out > 1){
                    $tr = array();
                    if(in_array($seccion->idSeccion, $arraySelected)) {
                        $tr = array("select" => true);
                    }
                    $tree[] = array_merge(array(
                        "key" => $seccion->idSeccion,
                        "title" => $seccion->nombreSeccion,
                        "active" => $seccion->active,
                        "isFolder" => true,
                        "children" => QueryHelper::make_tree($seccion->idSeccion,$out-1,$arraySelected,$exclude)
                        ),$tr);
                } else {
                    $tr = array();
                    if(in_array($seccion->idSeccion, $arraySelected)) {
                        $tr = array("select" => true);
                    }
                    $tree[] = array_merge(array(
                        "key" => $seccion->idSeccion,
                        "title" => $seccion->nombreSeccion,
                        "active" => $seccion->active
                        ),$tr);
                }
            }
        }
        return $tree;
    }

    public static function make_ticket_tree($idParent = '',$out = 5,$arraySelected = array(), $exclude = array())
    {
        $tree = array();
        if($idParent) {
            $TiposTicket = Model::factory('TiposTicket')->where('idPadre',$idParent)->order_by_asc('prioridad')->find_many();
        } else {
            $TiposTicket = Model::factory('TiposTicket')->where('idPadre',0)->order_by_asc('prioridad')->find_many();
        }
        foreach ($TiposTicket as $tipoticket) {
            if(!in_array($tipoticket->idTipo, $exclude)) {
                if(count($tipoticket->childs()) && $tipoticket->idTipo && $out > 1){
                    $tr = array();
                    if(in_array($tipoticket->idTipo, $arraySelected)) {
                        $tr = array("select" => true);
                    }
                    $tree[] = array_merge(array(
                        "key" => $tipoticket->idTipo,
                        "title" => $tipoticket->nombreTipo,
                        "isFolder" => true,
                        "children" => QueryHelper::make_ticket_tree($tipoticket->idTipo,$out-1,$arraySelected,$exclude)
                        ),$tr);
                } else {
                    $tr = array();
                    if(in_array($tipoticket->idTipo, $arraySelected)) {
                        $tr = array("select" => true);
                    }
                    $tree[] = array_merge(array(
                        "key" => $tipoticket->idTipo,
                        "title" => $tipoticket->nombreTipo
                        ),$tr);
                }
            }
        }
        return $tree;
    }

    public static function make_menu($idParent = '',$Top = true)
    {
        $tree = array();
        if($Top) {
            $Menus = Model::factory('Menus')->where('idTipoMenu',$idParent)->where('idParentMenu',0)->order_by_asc('ordenMenu')->find_many();
        } else {
            $Menus = Model::factory('Menus')->where('idParentMenu',$idParent)->order_by_asc('ordenMenu')->find_many();
        }
        foreach ($Menus as $menu) {
            if(count($menu->childs()) && $Top){
                $tree[] = array(
                    "key" => $menu->idMenu,
                    "title" => $menu->nombreMenu(),
                    "link" => $menu->link,
                    "idTipoLink" => $menu->idTipoLink,
                    "isFolder" => true,
                    "children" => QueryHelper::make_menu($menu->idMenu,false)
                    );
            } else {
                $tree[] = array(
                    "key" => $menu->idMenu,
                    "title" => $menu->nombreMenu(),
                    "link" => $menu->link,
                    "idTipoLink" => $menu->idTipoLink
                    );
            }
        }
        return $tree;
    }

    public static function Abuelos() {
        return Model::factory('Secciones')->where('padreSeccion',0)->where_not_equal('idSeccion',0)->where('active',true)->order_by_asc('ordenAbuelo')->find_many();
    }

    public static function getPrice($ref,$cant,$type = 'id',$precioperfil = 0) {
        if($type == 'id'){
            $paquete = Model::factory('Productos')->filter('joinPaquete')->where('paquetesproducto.idPaqueteProducto',$ref)->find_one();
        } else {
            $paquete = Model::factory('Productos')->filter('joinPaquete')->where('paquetesproducto.refPaqueteProducto',$ref)->find_one();
        }
        if($precioperfil > 0) {
            return $paquete->{'precio'.$precioperfil};
        }
        if(!$paquete) { return false; }
        $descuento = (1-($paquete->descuentoProducto/100));
        if($cant <= $paquete->intervalo1) {
            return $paquete->precio1*$descuento;
        }
        if($cant >$paquete->intervalo1 && $cant <= $paquete->intervalo2) {
            return $paquete->precio2*$descuento;
        }
        if($cant > $paquete->intervalo2) {
            return $paquete->precio3*$descuento;
        }
    }

    public static function getConf($key,$type='',$array = false) {
        $constant = Model::factory('Configuracion');
        $constant = ($array) ? $constant->where_like('key',$key.'%') : $constant->where('key',$key);
        $constant = ($type) ? $constant->where('type',$type) : $constant;
        if(!$array) {
            $constant = $constant->find_one();
            if(!$constant) { return ''; }
            return $constant->value;
        } else {
            $resultConstants = array();
            $Constants = $constant->find_many();
            foreach ($Constants as $const) {
                $keytemp = explode('-',$const->key);
                $id = $keytemp[1];
                $resultConstants[$id] = $const->value;
            }
            return $resultConstants;
        }
    }

    public static function setConf($key,$type,$value) {
        $constant = Model::factory('Configuracion')->where('key',$key)->where('type',$type)->find_one();
        if(!$constant) {
            $constant = Model::factory('Configuracion')->create(array('key'=>$key,'type'=>$type,'value'=>$value));
            $constant->save();
            return;
        }
        $constant->value = $value;
        $constant->save();
    }

    public static function getAbuelos($secciones)
    {
        $abuelos = array();
        foreach ($secciones as $seccion) {
            if($seccion->bisabuelo > 0) {
                $abuelos[$seccion->bisabuelo] = $seccion->bisabuelo;
            } else {
                if($seccion->abuelo > 0) {
                    $abuelos[$seccion->abuelo] = $seccion->abuelo;
                } else {
                    if($seccion->padre > 0) {
                        $abuelos[$seccion->padre] = $seccion->padre;
                    } else {
                        if($seccion->hijo > 0) {
                            $abuelos[$seccion->hijo] = $seccion->hijo;
                        }
                    }
                }
            }
        }
        return $abuelos;
    }

    public static function getPathAbuelos($aData) {
        $seccionesArray = array();
        foreach ($aData["aaData"] as $key => $value) {
            $seccionesArray[] = $value[12];
        };
        if(count($seccionesArray)) {
            if(!Session::get('id_lang') || Session::get('id_lang') == 0 || Cookie::get('lang') == 'esp' || !Cookie::get('lang')) {
                $secciones = Model::factory('Secciones')
                    ->table_alias('s')
                    ->select('s.idSeccion','idHijo')
                    ->select('s.nombreSeccion','nombreHijo')
                    ->select('t.idSeccion','idPadre')
                    ->select('t.nombreSeccion','nombrePadre')
                    ->select('u.idSeccion','idAbuelo')
                    ->select('u.nombreSeccion','nombreAbuelo')
        //            ->left_join('secciones', array('p.idSeccion', '=', 's.idSeccion'),'s')
                    ->left_join('secciones', array('s.padreSeccion', '=', 't.idSeccion'),'t')
                    ->left_join('secciones', array('t.padreSeccion', '=', 'u.idSeccion'),'u')
                    ->where_in('s.idSeccion',$seccionesArray);
                    // var_dump($secciones->get_query());
            } else {
                // TODO
                $secciones = Model::factory('Secciones')
                    ->table_alias('s')
                    ->select('s.idSeccion','idHijo')
                    // ->select('s.nombreSeccion','nombreHijo')
                    ->select_expr('IF (st.nombreSeccion is null,s.nombreSeccion,st.nombreSeccion)','nombreHijo')
                    ->select('t.idSeccion','idPadre')
                    // ->select('t.nombreSeccion','nombrePadre')
                    ->select_expr('IF (tt.nombreSeccion is null,t.nombreSeccion,tt.nombreSeccion)','nombrePadre')
                    ->select('u.idSeccion','idAbuelo')
                    // ->select('u.nombreSeccion','nombreAbuelo')
                    ->select_expr('IF (ut.nombreSeccion is null,u.nombreSeccion,ut.nombreSeccion)','nombreAbuelo')
        //            ->left_join('secciones', array('p.idSeccion', '=', 's.idSeccion'),'s')
                    ->left_join('secciones', array('s.padreSeccion', '=', 't.idSeccion'),'t')
                    ->left_join('secciones', array('t.padreSeccion', '=', 'u.idSeccion'),'u')

                    ->left_join('seccionestraduccion', array('st.idSeccion', '=', 's.idSeccion'),'st')
                    ->left_join('seccionestraduccion', array('tt.idSeccion', '=', 't.idSeccion'),'tt')
                    ->left_join('seccionestraduccion', array('ut.idSeccion', '=', 'u.idSeccion'),'ut')


                    ->where_in('s.idSeccion',$seccionesArray)
                    ->where_raw('(st.idIdioma = ? OR st.idIdioma is null)',array(Session::get('id_lang')))
                    ->where_raw('(tt.idIdioma = ? OR tt.idIdioma is null)',array(Session::get('id_lang')))
                    ->where_raw('(ut.idIdioma = ? OR ut.idIdioma is null)',array(Session::get('id_lang')));
                    // print_r($secciones->get_query());
                // ->select_expr('IF (productostraduccion.nombreProducto is null,productos.nombreProducto,productostraduccion.nombreProducto)','nombreHijo')
                // ->left_join('seccionestraduccion', array('st.padreSeccion', '=', 'u.idSeccion'),'u')
            }



            $secc = array();
            foreach ($secciones->find_array() as $value) {
                $secc[$value['idHijo']] = $value;
            }
            $aData['parents'] = $secc;
        } else {
            $aData['parents'] = array();
        }
        return $aData;
    }

    public static function getPackages($aData) {
        if(!count($aData["aaData"])) { return $aData; }
        $productos = array();
        foreach ($aData["aaData"] as $key => $value) {
            $paqueteproducto = Model::factory('PaquetesProducto')->where('active',true)->where('idProducto',$value[1])->find_many();
            $productos[$value[1]] = array();
            foreach ($paqueteproducto as $paquete) {
                $presentacion = $paquete->presentacion();
                $productos[$value[1]][$paquete->refPaqueteProducto] = array(
                    'referencia' => $paquete->refPaqueteProducto,
                    'presentacion' => $presentacion->nombrePresentacion(),
                    'medida' => $presentacion->medidaPresentacion(),
                    'paquete' => $paquete->nombrePaquete(),
                    'stock' => ($paquete->stockPaqueteProducto < $paquete->minStock) ? 0 : $paquete->stockPaqueteProducto - $paquete->minStock,
                    'precio1' => $paquete->precio1,
                    'precio2' => $paquete->precio2,
                    'precio3' => $paquete->precio3,
                    'intervalo1' => $paquete->intervalo1,
                    'intervalo2' => $paquete->intervalo2,
                    'intervalo3' => $paquete->intervalo3
                    );
            }
        };

        $aData['paquetes'] = $productos;
        return $aData;
    }

    public static function getAditionalPackages($aData) {
        if(!count($aData["aaData"])) { return $aData; }
        $productos = array();
        foreach ($aData["aaData"] as $key => $value) {
            $paqueteproducto = Model::factory('PaquetesProducto')->where('idProducto',$value[1])->find_many();
            $productos[$value[1]] = array();
            if(count($paqueteproducto)>1) {
                foreach ($paqueteproducto as $paquete) {
                    if($paquete->refPaqueteProducto != $value[4]) {
                        $productos[$value[1]][$paquete->refPaqueteProducto] = array(
                            'referencia' => $paquete->refPaqueteProducto,
                            'presentacion' => $paquete->nombrePresentacion(),
                            'paquete' => $paquete->nombrePaquete(),
                            'stock' => $paquete->stockPaqueteProducto,
                            'precio1' => $paquete->precio1,
                            'precio2' => $paquete->precio2,
                            'precio3' => $paquete->precio3,
                            'intervalo1' => $paquete->intervalo1,
                            'intervalo2' => $paquete->intervalo2,
                            'intervalo3' => $paquete->intervalo3
                            );
                    }
                }
            }
        };

        $aData['aditional_data'] = $productos;
        return $aData;
    }

    public static function logo($email = false) {
        $logo = Model::factory('Logos')->where('activeLogo',true)->where('emailLogo',$email)->order_by_expr('rand()')->find_one();
        return $logo->imagenLogo;
    }

    public static function getAbuelosOnly() {
        $Secciones = Model::factory('Secciones')->where_equal('padreSeccion',0)->order_by_asc('nombreSeccion')->find_many();
        $result = array();
        foreach ($Secciones as $seccion) {
            if(!$seccion->agruparSubsecciones) {
                $result[] = $seccion;
            } else {
                foreach ($seccion->childs() as $child) {
                    $child->nombreSeccion = $child->nombreSeccion.' ('.$seccion->nombreSeccion.')';
                    $result[] = $child;
                 }
            }
        }
        return $result;
    }

    public static function getAbuelosForSearch($aData,$idBusqueda) {
        if(!count($aData["aCounts"])) { return $aData; }
        $seccs = array();
        $oldcants = array();
        $newcants = array();
        $padrescants = array();
        $cants = array();
        $names = array();
        foreach ($aData["aCounts"] as $secc) {
            $seccs[] = $secc["idSeccion"];
            $oldcants[$secc["idSeccion"]] = $secc["cantCol"];
        }
        if(!Session::get('id_lang') || Session::get('id_lang') == 0 || Cookie::get('lang') == 'esp' || !Cookie::get('lang')) {
            $seccionesList = Model::factory("Secciones")
                ->table_alias("s")
                ->select("s.idSeccion","idHijo")
                ->select("s.nombreSeccion","nombreHijo")
                ->select("t.idSeccion","idPadre")
                ->select("t.nombreSeccion","nombrePadre")
                ->select("t.agruparSubsecciones","agruparPadre")
                ->select("u.idSeccion","idAbuelo")
                ->select("u.nombreSeccion","nombreAbuelo")
                ->select("u.agruparSubsecciones","agruparAbuelo")
                ->select("v.idSeccion","idBis")
                ->select("v.nombreSeccion","nombreBis")
                ->left_join("secciones",array("s.padreSeccion","=","t.idSeccion"),"t")
                ->left_join("secciones",array("t.padreSeccion","=","u.idSeccion"),"u")
                ->left_join("secciones",array("u.padreSeccion","=","v.idSeccion"),"v")
                ->where_in("s.idSeccion",$seccs);
        } else {
            // $seccionesList = Model::factory("Secciones")
            //     ->table_alias("s")
            //     ->select("s.idSeccion","idHijo")
            //     ->select("s.nombreSeccion","nombreHijo")
            //     ->select("t.idSeccion","idPadre")
            //     ->select("t.nombreSeccion","nombrePadre")
            //     ->select("t.agruparSubsecciones","agruparPadre")
            //     ->select("u.idSeccion","idAbuelo")
            //     ->select("u.nombreSeccion","nombreAbuelo")
            //     ->select("u.agruparSubsecciones","agruparAbuelo")
            //     ->select("v.idSeccion","idBis")
            //     ->select("v.nombreSeccion","nombreBis")
            //     ->left_join("secciones",array("s.padreSeccion","=","t.idSeccion"),"t")
            //     ->left_join("secciones",array("t.padreSeccion","=","u.idSeccion"),"u")
            //     ->left_join("secciones",array("u.padreSeccion","=","v.idSeccion"),"v")
            //     ->where_in("s.idSeccion",$seccs)->find_many();

            $seccionesList = Model::factory("Secciones")
                ->table_alias("s")
                ->select("s.idSeccion","idHijo")
                // ->select("s.nombreSeccion","nombreHijo")
                ->select_expr('IF (st.nombreSeccion is null,s.nombreSeccion,st.nombreSeccion)','nombreHijo')
                ->select("t.idSeccion","idPadre")
                // ->select("t.nombreSeccion","nombrePadre")
                ->select_expr('IF (tt.nombreSeccion is null,t.nombreSeccion,tt.nombreSeccion)','nombrePadre')
                ->select("t.agruparSubsecciones","agruparPadre")
                ->select("u.idSeccion","idAbuelo")
                // ->select("u.nombreSeccion","nombreAbuelo")
                ->select_expr('IF (ut.nombreSeccion is null,u.nombreSeccion,ut.nombreSeccion)','nombreAbuelo')
                ->select("u.agruparSubsecciones","agruparAbuelo")
                ->select("v.idSeccion","idBis")
                // ->select("v.nombreSeccion","nombreBis")
                ->select_expr('IF (vt.nombreSeccion is null,v.nombreSeccion,vt.nombreSeccion)','nombreBis')
                ->left_join("secciones",array("s.padreSeccion","=","t.idSeccion"),"t")
                ->left_join("secciones",array("t.padreSeccion","=","u.idSeccion"),"u")
                ->left_join("secciones",array("u.padreSeccion","=","v.idSeccion"),"v")

                ->left_join('seccionestraduccion', array('st.idSeccion', '=', 's.idSeccion'),'st')
                ->left_join('seccionestraduccion', array('tt.idSeccion', '=', 't.idSeccion'),'tt')
                ->left_join('seccionestraduccion', array('ut.idSeccion', '=', 'u.idSeccion'),'ut')
                ->left_join('seccionestraduccion', array('vt.idSeccion', '=', 'v.idSeccion'),'vt')

                ->where_raw('(st.idIdioma = ? OR st.idIdioma is null)',array(Session::get('id_lang')))
                ->where_raw('(tt.idIdioma = ? OR tt.idIdioma is null)',array(Session::get('id_lang')))
                ->where_raw('(ut.idIdioma = ? OR ut.idIdioma is null)',array(Session::get('id_lang')))
                ->where_raw('(vt.idIdioma = ? OR vt.idIdioma is null)',array(Session::get('id_lang')))

                ->where_in("s.idSeccion",$seccs);
        }
        // print_r( $seccionesList->get_query() );

        // var_dump($seccionesList);die();
        // var_dump($oldcants);
        $list = array("Hijo","Padre","Abuelo","Bis");
        $seccionesList = $seccionesList->find_many();
        foreach ($seccionesList as $secc) {
            // var_dump($secc->as_array());
            $idHijo = $secc->idHijo;
            $id = 0;
            $nombre = '';
            if($secc->idBis && !$id) {
                $id = $secc->idAbuelo;
                $nombre = $secc->nombreAbuelo.' ('.$secc->nombreBis.')';
            }
            if($secc->idAbuelo && !$id && !$secc->agruparAbuelo) {
                $id = $secc->idAbuelo;
                $nombre = $secc->nombreAbuelo;
            }
            if($secc->idPadre && !$id && $secc->agruparAbuelo) {
                $id = $secc->idPadre;
                $nombre = $secc->nombrePadre.' ('.$secc->nombreAbuelo.')';
            }
            if($secc->idPadre && !$id && !$secc->agruparPadre) {
                $id = $secc->idPadre;
                $nombre = $secc->nombrePadre;
            }
            if($secc->idHijo && !$id && $secc->agruparPadre) {
                $id = $secc->idHijo;
                $nombre = $secc->nombreHijo;
            }
            if($secc->idHijo && !$id) {
                $id = $secc->idHijo;
                $nombre = $secc->nombreHijo.' ('.$secc->nombrePadre.')';
            }
            $cants[$id] = (array_key_exists($id, $cants)) ? $cants[$id] + $oldcants[$idHijo] : $oldcants[$idHijo];
            $names[$id] = $nombre;

            // foreach ($list as $nm) {
            //     $id = $secc->{"id".$nm};
            //     $nombre = $secc->{"nombre".$nm};
            //     if(array_key_exists($id, $oldcants)) {
            //         $padrescants[] = array("idSeccion"=>$id,"cantCol"=>$oldcants[$id],"nombreSeccion" => $nombre);
            //     }
            // }
        }
        foreach ($cants as $id => $cantidad) {
            $newcants[] = array("idSeccion"=>$id,"cantCol"=>$cantidad,"nombreSeccion" => $names[$id]);
        }
        $aData["aCounts"] = $newcants;
        $cants = array();
        $names = array();
        if(count($newcants) == 1) {
            // $idPadre = $newcants[0]['idSeccion'];
            $padrescants = $newcants;
            $idPadre = $idBusqueda;
            foreach ($seccionesList as $secc) {
                $idHijo = $secc->idHijo;
                $id = 0;
                $nombre = '';
                if($secc->idBis == $idPadre) {
                    $id = $secc->idAbuelo;
                    $nombre = $secc->nombreAbuelo;
                }
                if($secc->idAbuelo == $idPadre) {
                    $id = $secc->idPadre;
                    $nombre = $secc->nombrePadre;
                }
                if($secc->idPadre == $idPadre || $secc->idHijo == $idPadre) {
                    $id = $secc->idHijo;
                    $nombre = $secc->nombreHijo;
                }
                $cants[$id] = (array_key_exists($id, $cants)) ? $cants[$id] + $oldcants[$idHijo] : $oldcants[$idHijo];
                $names[$id] = $nombre;
            }
            foreach ($cants as $id => $cantidad) {
                // $padrescants[] = array("idSeccion"=>$id,"cantCol"=>$cantidad,"nombreSeccion" => $names[$id]);
                $padrescants[] = array("idSeccion"=>$id,"cantCol"=>$cantidad,"nombreSeccion" => $names[$id],"ident" => 1);
            }

            $aData["aCounts"] = $padrescants;
        }
        return $aData;
    }

    public static function getLastUpdate($tipo) {
        $update = Model::factory('Actualizaciones')->where('tipo',$tipo)->find_one();
        if($update) {
            $usuario = $update->usuario();
            $user = ($usuario) ? $usuario->nombreUsuario : 'cron';

            return array('fecha'=>$update->fecha, 'user'=>$user);
        }
        return array('fecha'=>'','user'=>'');
    }

    public static function getSwfs($aData) {
        // if(!count($aData["aaData"])) { return $aData; }
        $swfs = array();
        foreach ($aData["aaData"] as $prd) {
            $swfs[$prd["1"]] = 0;
            $path = IMG_PRD_PATH_BIG.$prd["1"]."/*.swf";
            foreach (glob($path) as $swf) {
                $swfs[$prd["1"]] = 1;
            }
        }
        $aData['aSwfs'] = $swfs;
        return $aData;
    }

    public static function get_coupon_mail($id, $return_values = false, $especial = true, $alwaysok = false) {
        if($especial) {
            $cupon = Model::factory('CuponesOtros')->filter('get_coupon',$id)->find_one();

            if($cupon->is_active() && $cupon->vigenciaCuponOtros > 0 && $cupon->codigoCuponOtros || $alwaysok) {
                if($return_values) {
                    return $cupon;
                }
                $textoCupon = $cupon->textoCupon.'<font color="#5F0606" size="3"><br>CUPÓN: '.$cupon->codigoCuponOtros.'<br><br>'.
                    'Descuendo del '.$cupon->porcentajeCuponOtros.'% '.
                    'Valido por '.$cupon->vigenciaCuponOtros.' días<br>'.
                    'Usa el código para obtener este beneficio.</font>';
                return $textoCupon;
            } else {
                return '';
            }

        } else {
            $cupon = Model::factory('Cupones')->where('idCupon',$id)->find_one();
            $textoCupon = $cupon->descipcionCupon.'<font color="#5F0606" size="3"><br>CUPÓN: {custom:codigoCupon}<br><br>'.
                'Descuendo del '.$cupon->porcentajeCupon.'% '.
                'Fecha Vencimiento: '.$cupon->fechaVencimientoCupon.'<br>'.
                'Usa el código para obtener este beneficio.</font>';
            return $textoCupon;
        }
        // if($cupon->is_active() && $cupon->vigenciaCuponOtros > 0) {
    }

    public static function validate_sections($idParent = '') {
        if($idParent) {
            $Secciones = Model::factory('Secciones')->where('padreSeccion',$idParent)->find_many();
        } else {
            $Secciones = Model::factory('Secciones')->where('padreSeccion',0)->find_many();
        }
        $active_section = false;
        foreach ($Secciones as $seccion) {
            $active = false;
            if($seccion->has_children()) {
                $active = QueryHelper::validate_sections($seccion->idSeccion);
            } else {
                if($seccion->has_active_products()) {
                    $active = true;
                } else {
                    $active = false;
                }
            }
            $seccion->active = $active;
            $seccion->save();
            echo '<br>'.$seccion->idSeccion;
            echo '-'.$seccion->nombreSeccion.'- ';
            echo ($seccion->active) ? '<font color="#0f0">habilitada</font>' : '<font color="#f00">deshabilitada</font>';
            if($active) {
                $active_section = true;
            }
        }
        return $active_section;
    }
}