<?php

class Cart {
    public $Items = array();
    public $ItemGroups = array();
    public $DHLPrice = 0;
    public $country = '';
    public $city = '';
    public $zipcode = '';
    public $volume = 0;
    public $peso = 0;
    public $shippingPriceDirty = true;

    function __construct($load = true) {
        if($load) {
            $this->Items = $this->get_cart();
            // var_dump($this->Items);
            $this->calcGroups($this->Items);
            // var_dump($this->ItemGroups);
        }
    }

    public function count() {
        $cart = $this->Items;
        $total = 0;
        foreach ($cart as $item) {
            $total = $total + $item["cant"];
        }
        return $total;
    }

    public function total_price() {
        $cart = $this->Items;
        $total = 0;
        foreach ($cart as $item) {
            $total = $total + ($item["cant"]*$item["price"]);
        }
        return $total;
    }

    public function volume_price() {
        $volumen = 0;
        $cart = $this->Items;
        foreach ($cart as $item) {
            $volumen = $volumen + ($item["cant"]*$item["volumen"]);
        }
        $volumePrice = Model::factory('Volumenes')->where_gt('volumen',$volumen)->order_by_asc('volumen')->find_one();
        if(!$volumePrice) {
            $volumePrice = Model::factory('Volumenes')->order_by_desc('volumen')->find_one();
            // echo 'aqui';die();
            return round($volumen*$volumePrice->precio/$volumePrice->volumen, -2);
        } else {
            // echo round($volumePrice->precio, -2);die();
            return round($volumePrice->precio, -2);

        }
    }

    public function DHL_price($country, $city, $zipcode) {
        if($this->country != $country) {
            $this->shippingPriceDirty = true;
        }
        if($this->city != $city) {
            $this->shippingPriceDirty = true;
        }
        if($this->zipcode != $zipcode) {
            $this->shippingPriceDirty = true;
        }
        if($this->volume != $this->volume()) {
            $this->shippingPriceDirty = true;
        }
        if($this->peso != $this->peso()) {
            $this->shippingPriceDirty = true;
        }

        $this->country = $country;
        $this->city = $city;
        $this->zipcode = $zipcode;
        $this->volume = $this->volume();
        $this->peso = $this->peso();

        // var_dump($this->Items);
        if($this->shippingPriceDirty) {
            $w = ceil(pow($this->volume(), 1/3));
            $params = array(
                'w0' => $w,
                'h0' => $w,
                'l0' => $w,
                'wgt0' => $this->peso(),
                'dstCtry' => $country,
                'dstCity' => $city,
                'dstZip' => $zipcode
                );
            // var_dump($params);
            $this->DHLPrice = round(DHL::getQuote($params), -2);
            $this->shippingPriceDirty = false;
            return $this->DHLPrice;
        } else {
            return $this->DHLPrice;
        }
    }

    public function volume() {
        $volumen = 0;
        $cart = $this->Items;
        foreach ($cart as $item) {
            $volumen = $volumen + ($item["cant"]*$item["volumen"]);
        }
        return $volumen;
    }

    public function peso() {
        $peso = 0;
        $cart = $this->Items;
        foreach ($cart as $item) {
            $peso = $peso + ($item["cant"]*$item["peso"]);
        }
        return $peso/1000;
    }

    function total_with_discount() {
        $cart = $this->Items;
        $discount = 0;
        foreach ($cart as $item) {
            $discount = $discount + ($item["cant"]*($item["price"]*(1-($item["discount"]/100))));
        }
        return $discount;
    }

    function total_discount() {
        $cart = $this->Items;
        $discount = 0;
        foreach ($cart as $item) {
            $discount = $discount + ($item["cant"]*($item["price"]*($item["discount"]/100)));
        }
        return $discount;
    }

    public function get_cart() {
        $idCliente = Session::get("client_id");
        if(Session::get('client_logged_in')) {
            $cartModel = Model::factory('clientesCart')->where('idCliente',$idCliente)->find_one();
            if(!$cartModel) {
                $newCart = array();
            } else if (!$cartModel->cart) {
                $newCart = array();
            } else {
                $newCart = json_decode($cartModel->cart,true);
            }
            $oldCart = Session::get('cart');
            if(!is_array($oldCart)) { $oldCart = array(); }
            return array_merge($newCart,$oldCart);
        } else {
            $cart = Session::get('cart');
            if($cart) {
                return $cart;
            }
            // $cart = Cookie::get('cart');
            if(!$cart) {
                return array();
            }
            $cart = str_replace('\"', '"', $cart);
            return json_decode($cart,true);
        }
        return array();
    }

    public function save_cart($cart) {
        $cart = $this->clean($cart);
        $idCliente = Session::get("client_id");
        if(Session::get('client_logged_in')) {
            $cartModel = Model::factory('clientesCart')->where('idCliente',$idCliente)->find_one();
            if(!$cartModel) {
                $cartModel = Model::factory('clientesCart')->create();
                $cartModel->idCliente = $idCliente;
            }
            $cartModel->cart = json_encode($cart);
            $cartModel->save();
        } else {
            // Cookie::set('cart',json_encode($cart));
        }
        $this->shippingPriceDirty = true;
        Session::set('cart',$cart);
        return true;
    }

    function addTo($newItem) {
        $cart = $this->Items;
        foreach ($newItem as $newRef => $newData) {
            if(array_key_exists($newRef, $cart)) {
                $newData["cant"] = $cart[$newRef]["cant"]+$newData["cant"];
            }
            $newData = $this->getProduct($newData);
            if(!$newData) { return false; }
            // $cart[$newRef] = $newData;
            $cart[$newData["ref"]] = $newData;
            //$cart = $this->updateGroupPrices($cart,$newData);
        }
        $cart = $this->updateAllItems($cart);
        return $this->save_cart($cart);
    }

    function updateItem($newItem) {
        $cart = $this->Items;
        foreach ($newItem as $newRef => $newData) {
            $newData = $this->getProduct($newData);
            // if(!$newData) { continue; }
            if(!$newData) { return false; }
            // $cart[$newRef] = $newData;
            $cart[$newData["ref"]] = $newData;
            //$cart = $this->updateGroupPrices($cart,$newData);
        }
        $cart = $this->updateAllItems($cart);
        return $this->save_cart($cart);
    }

    function updateAllItems($cart) {
        //$cart = $this->Items;
        $newCart = array();
        $this->calcGroups($cart);
        foreach ($cart as $newRef => $newData) {
            $newData = $this->getProduct($newData);
            if(!$newData) { return false; }
            // $cart[$newRef] = $newData;
            $newCart[$newData["ref"]] = $newData;
        }
        return $newCart;
    }

    public function deleteItem($newItem) { //TODO
        $cart = $this->Items;
        foreach ($newItem as $newRef => $newData) {
            if(array_key_exists($newRef, $cart)) {
                unset($cart[$newRef]);
            }
        }
        $cart = $this->updateAllItems($cart);
        return $this->save_cart($cart);
    }

    public function delete() {
        Session::delete('cart');
        Cookie::delete('cart');
        $this->Items = array();
        $this->save_cart(array());
        return true;
    }

    private function clean($cart) {
        foreach ($cart as $ref => $data) {
            if($cart[$ref]["cant"] == 0) {
                unset($cart[$ref]);
            }
        }
        return $cart;
    }

    private function producto($item)
    {
        if($item['ref']) {
            $producto = Model::factory('Productos')->filter('joinPaquete')->where('paquetesproducto.refPaqueteProducto',$item['ref'])->find_one();
        } else {
            $producto = Model::factory('Productos')->filter('joinPaquete')->where('producto.idProducto',$item['id'])->find_one();
        }
        return $producto;
    }

    private function getProduct($item) {
        $producto = $this->producto($item);
        if(!isset($item["cant"])) { $item["cant"] = 0; }
        if($producto->stockPaqueteProducto < $producto->minStock) {
            $cartItem = array(
                "id" => $producto->idProducto,
                "ref" => $producto->refPaqueteProducto,
                "cant" => 0,
                "price" => 0,
                // "price" => $this->getPrice($producto,$item["cant"]),
                "discount" => 0,
                "full_price" => 0,
                "ancho" => 0,
                "largo" => 0,
                "alto" => 0,
                "peso" => 0,
                "volumen" => 0,
                "price_group" => ""
                );
            return $cartItem;
        }
        if($producto->stockPaqueteProducto < $item["cant"]) {
            return false;
        }
        if($producto->grupo_precios) {
            $cant = $this->getCantGroups($producto->refPaqueteProducto, $producto->grupo_precios, $item["cant"]);
        } else {
            $cant = $item["cant"];
        }
        if(is_numeric($producto->descuentoProducto)) {
            $descuento = $producto->descuentoProducto;
        } else {
            $descuento = 0;
        }
        $precioperfil = 0;
        if($perfil = Session::get('perfil_data')) {
            $precio = $perfil->idPrecio;
            $precioperfil = $precio - 1;
        }
        $cartItem = array(
            "id" => $producto->idProducto,
            "ref" => $producto->refPaqueteProducto,
            "cant" => $item["cant"],
            "price" => ($this->getPrice($producto,$cant,$precioperfil)*(1-($descuento/100))),
            // "price" => $this->getPrice($producto,$item["cant"]),
            "discount" => $producto->descuentoProducto,
            "full_price" => $this->getPrice($producto,$cant,$precioperfil),
            "ancho" => ($producto->ancho) ? $producto->ancho : '2',
            "largo" => ($producto->largo) ? $producto->largo : '8',
            "alto" => ($producto->alto) ? $producto->alto : '8',
            "peso" => (($producto->peso) ? $producto->peso : 0.07108) * $item["cant"],
            "volumen" => ($producto->ancho) ? ($producto->ancho * $producto->largo * $producto->alto) : 128,
            "price_group" => $producto->grupo_precios
            );
        return $cartItem;
    }

    private function getPrice($paquete,$cant,$precioperfil) {
        if($precioperfil) {
            return $paquete->{'precio'.$precioperfil};
        }
        if (!isset($paquete->intervalo1)) {
            return false;
        }
        if($cant <= $paquete->intervalo1) {
            return $paquete->precio1;
        }
        if($cant >$paquete->intervalo1 && $cant <= $paquete->intervalo2) {
            return $paquete->precio2;
        }
        if($cant > $paquete->intervalo2) {
            return $paquete->precio3;
        }
    }

    private function calcGroups($cart) {
        $this->ItemGroups = array();
        foreach ($cart as $ref => $item) {
            if(@$item["price_group"]) {
                $this->ItemGroups[$item["price_group"]]["items"][$item["ref"]] = $item["cant"];
                if(!isset($this->ItemGroups[$item["price_group"]]["total"])) {
                    $this->ItemGroups[$item["price_group"]]["total"] = $item["cant"];
                } else {
                    $this->ItemGroups[$item["price_group"]]["total"] += $item["cant"];
                }
            }
        }
    }

    private function getCantGroups($ref, $group, $cant) {
        $this->ItemGroups[$group]["items"][$ref] = $cant;
        $this->ItemGroups[$group]["total"] = 0;
        foreach ($this->ItemGroups[$group]["items"] as $key => $value) {
            $this->ItemGroups[$group]["total"] += $value;
        }
        return $this->ItemGroups[$group]["total"];
    }

    private function updateGroupPrices($cart, $newData) {
         foreach ($newData as $ref => $item) {
            $group = $item["price_group"];
            if($group) {
                if(isset($this->ItemGroups[$group])) {
                    foreach ($this->ItemGroups[$group]["items"] as $ref => $cant) {
                        $cart[$ref]["price"] = $item["price"];
                    }
                }
            }
        }
        return $cart;
   }

   //  private function updateGroups($cart, $newData) {
   //       foreach ($newData as $ref => $item) {
   //          $group = $item["price_group"];
   //          if(isset($this->ItemGroups[$group])) {
   //              foreach ($this->ItemGroups[$group]["items"] as $ref => $cant) {
   //                  $cart[$ref]["price"] = $item["price"];
   //              }
   //          }
   //      }
   //      return $cart;
   // }
}