<?php
/**
* 
*/
$debug = (defined('DEBUG')) ? DEBUG : false;
define('UNLINK_IMAGES', !$debug);
class ImportExcelFile
{
    private $xhr = "";
    private $xhrparams = array();
    private $xhrclass = false;
    public function validate_rows($cols,&$errores,$required = array())
    {
        foreach ($required as $key => $value) {
            if(!array_key_exists($value, $cols)) {
                $errores[] = array('A' => "No existe la columna ".$value.".");
                /*
                if(!is_numeric($key)) {
                    $errores[] = array('A' => "No existe la columna ".$value.".");
                } else {
                    $errores[] = array('A' => "No existe la columna ".$value.".");
                }
                */
            }
        }
    }

    public function validate_cell($row,$cols,&$errores,&$checknull)
    {
        foreach ($checknull as $key => $value) {
            if(isset($row[$cols[$value]])) {
                if(!is_numeric($key)) {
                    if(!array_key_exists(intval($row[$cols[$key]]), $$value)) { $errores[] = array_merge($row, array($key_errores => $key." ".$row[$cols[$key]]." no existe")); continue; }
                } else {
                    if($row[$cols[$value]] == '' || $row[$cols[$value]] == null) {
                        $errores[] = array_merge($row, array($key_errores => $value." nulo"));
                        return false;
                    }
                }
            }
        }
    }

    public function saveErrores($errores, $fileName = 'php://output', $type = 'csv') {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 600);
        if($type == 'json') {
            file_put_contents($fileName.'.'.$type, json_encode($errores));
            return end(explode('/', $fileName)).'.'.$type;
        }
        if($type == 'csv') {
            // echo 'hasta aca llego';die();
            $fp = fopen($fileName.'.'.$type, "wb");
            foreach ($errores as $line) {
                if(!is_array($line)) { $line = array($line); }
                fputcsv($fp, $line, QueryHelper::getConf('CSV_DELIMITER','CONFIG'));
            }
            fclose($fp);
            $f = explode('/', $fileName);
            $f = end($f);
            return $f.'.'.$type;
        }


        $objPHPExcel = new PHPExcel();

        //echo date('H:i:s') . " Set properties\n";
        $objPHPExcel->getProperties()->setCreator("Lak Mir");
        $objPHPExcel->getProperties()->setLastModifiedBy("Nguillers");
        $objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Products Export");
        $objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Products Export");
        $objPHPExcel->getProperties()->setDescription("Export for nguillers products.");

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->getSheetView()->setZoomScale(80);

        $sheet->setTitle('Hoja 1');

        $sheet->fromArray($errores, NULL, 'A1');

        // if($campoIdioma) {
        //     $sheet->getColumnDimension('C')->setWidth(50);
        //     $sheet->getColumnDimension('D')->setWidth(50);
        //     $sheet->getColumnDimension('AC')->setWidth(50);
        //     $sheet->getColumnDimension('AD')->setWidth(50);
        // } else {
        // }
            $sheet->getColumnDimension('C')->setWidth(50);
            $sheet->getColumnDimension('Y')->setWidth(50);

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        //header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        //header("Content-Disposition: attachment; filename=\"productos.xlsx\";" );

        //$objWriter->save('php://output');
        $objWriter->save($fileName.'.'.$type);
        return end(explode('/', $fileName)).'.'.$type;
    }

    public function associate_by($data,$column) {
        $output = array();
        foreach ($data as $value) {
            $output[$value->$column] = $value;
        }
        return $output;
    }

    // Funciones para imagenes y extras
    public function check_extension($image) {
        $extension = 0;
        if(file_exists($image.".jpg")) {
            $extension = "jpg";
        } elseif (file_exists($image.".png")) {
            $extension = "png";
        } elseif (file_exists($image.".gif")) {
            $extension = "gif";
        }
        return $extension;
    }

    public function checkExtraImages($image) {
        $extraimages = glob(TMP_IMG_PATH.$image . '_*.*', GLOB_MARK);
        return $extraimages;
    }

    public function checkSwf($refPaqueteProducto) {
        if(file_exists(TMP_IMG_PATH.$refPaqueteProducto . '.swf')) {
            return TMP_IMG_PATH.$refPaqueteProducto . '.swf';
        }
        return false;
    }

    public function extra_images($idproducto, $refPaqueteProducto) {
        $extraImages = $this->checkExtraImages($refPaqueteProducto);
        // var_dump($extraImages);
        foreach ($extraImages as $imagen) {
            $tmp = explode('.', $imagen);
            $extension = end($tmp);
            if(!in_array($extension, array('jpg','png','gif','jpeg'))) {
                // return "Imagen ".$image." no valida";
                $warning[] = "Imagen ".$imagen." no valida";
                continue;
            }
            $image = "";
            $image = $this->load_image($imagen);
            if(!$extension) {
                $warning[] = "Tamaño de imagen ".$image." no valida";
                continue;
            }
            $this->save_image($idproducto,$extension,$image,array('BIG' => false,'SMALL','CART'),true); //Al enviar BIG en false, no deberia hacer resize
            if(UNLINK_IMAGES) {
                $tmp = explode('/', $image->filename);
                $tmp[count($tmp)-1] = 'imported/'.$tmp[count($tmp)-1];
                rename($image->filename,implode('/', $tmp));
                // unlink($image->filename);
            }
        }
    }

    public function extra_swf($idproducto, $refPaqueteProducto) {
        $swf = $this->checkSwf($refPaqueteProducto);
        if(!$swf) {
            return false;
        }
        $this->save_swf($idproducto, $swf);
    }

    public function load_image($imagen) {
        $image = new SimpleImage();
        $image->load($imagen);
        if($image->getWidth() != IMG_PRODUCTO_BIG_WIDTH || $image->getHeight() != IMG_PRODUCTO_BIG_HEIGHT) {
            return false; // $row[$key_errores] = "Tamanio de imagen no valido"; $errores[] = $row; continue;
        }
        return $image;
    }

    public function save_image($idProducto,$extension,$orgimage,$paths = array('BIG'),$extra = false) {
        // var_dump($paths);die();
        $uniqid = uniqid($idProducto);
        foreach ($paths as $key => $value) {
            $image = clone $orgimage;
            if(!is_numeric($key)) {
                $resize = $value;
                $type = $key;
            } else {
                $resize = true;
                $type = $value;
            }
            if($extra) {
                $path = constant('IMG_PRD_PATH_'.$type).$idProducto.'/';
                if(!is_dir($path)) {
                    mkdir($path);
                }
                $name = $uniqid.'.'.$extension;
            } else {
                $path = constant('IMG_PRD_PATH_'.$type);
                $name = $idProducto.'.'.$extension;
            }
            $pathImage = $path.$name;
            if(file_exists($pathImage)) {
                unlink($pathImage);
            }
            if($resize) {
                $image->resize(constant('IMG_PRODUCTO_'.$type.'_WIDTH'),constant('IMG_PRODUCTO_'.$type.'_HEIGHT'));
            }
            $image->save($pathImage);
            // var_dump($image);
            // die();
        }
    }

    public function save_swf($idProducto, $newswf, $path = 'IMG_PRD_PATH_BIG', $extra = true) {
        if($extra) {
            $path = constant($path).$idProducto.'/';
            if(!is_dir($path)) {
                mkdir($path);
            }
            $oldswfs = glob($path.'*.swf',GLOB_MARK);
            // var_dump($oldswfs);
            if(count($oldswfs)) {
                foreach ($oldswfs as $swf) {
                    unlink($swf);
                }
            }
            $name = uniqid($idProducto).'.swf';
        } else {
            $path = $path;
            $name = $idProducto.'.swf';
        }
        $pathSwf = $path.$name;
        if(file_exists($pathSwf)) {
            unlink($pathSwf);
        }
        // echo $newswf.'<br>'.$pathSwf;die();
        copy($newswf,$pathSwf);
        if(UNLINK_IMAGES) {
            unlink($newswf);
        }
    }

    public function image($idProducto, $refPaqueteProducto,$imagenProducto) {
        $warning = array();
        $error = '';
        $refPaquete = TMP_IMG_PATH.$refPaqueteProducto;
        $extension = $this->check_extension($refPaquete);
        if(!$extension && !$imagenProducto) {
            $error = "Imagen no encontrada";
            return(array('error' => $error));
        }
        $image = "";
        $image = $this->load_image($refPaquete.".".$extension);
        if(!$image) {
            if(!$imagenProducto) {
                $error = "Tamaño de imagen no valido";
                return(array('error' => $error));
            } else {
                $warning[] = "Tamaño de imagen no valido";
            }
        }
// ($dProducto,$extension,$orgimage,$paths = array('BIG'),$extra = false) {
        if($imagenProducto) {
            if(file_exists(IMG_PRD_PATH_BIG.$imagenProducto)) {
                unlink(IMG_PRD_PATH_BIG.$imagenProducto);
            }
            if(file_exists(IMG_PRD_PATH_SMALL.$imagenProducto)) {
                unlink(IMG_PRD_PATH_SMALL.$imagenProducto);
            }
            if(file_exists(IMG_PRD_PATH_CART.$imagenProducto)) {
                unlink(IMG_PRD_PATH_CART.$imagenProducto);
            }
        }
        // var_dump(array($idProducto,$extension,$image,array('BIG' => false,'SMALL','CART')));die();
        $this->save_image($idProducto,$extension,$image,array('BIG' => false,'SMALL','CART'));
        if(UNLINK_IMAGES) {
            unlink($image->filename);
        }
        // $producto->save();
        $imagenProducto = $idProducto.".".$extension;
        $result = array('imagen'=>$imagenProducto);
        if(count($warning)) {
            $result['warning'] = implode(' || ', $warning);
        }

        return $result;
    }


    //Funciones para llevar Xhr
    public function setXhr($xhr,$xhrparams = array(),$xhrclass = false) {
        $this->xhr = $xhr;
        $this->xhrparams = $xhrparams;
        if($xhrclass) {
            $this->xhrclass = new $xhrclass;
        }
    }

    public function execXhr($percent,$extraparams = array()) {
        if($this->xhr) {
            // var_dump($this->xhrparams);
            // var_dump($extraparams);
            $params = array_merge($this->xhrparams,$extraparams);
            // die();
            if($this->xhrclass) {
                $this->xhrclass->{$this->xhr} ($percent, $params);
            } else {
                eval($this->xhr.'($percent,$params);');
            }
        }
    }

    public function detect_delimiter($inputFileName) {
        $delimiter = false;
        $row = 0;
        if (($handle = fopen($inputFileName, "r")) !== FALSE) {

            $data = fgets($handle, 4096);
            $csv = explode(';', $data);
            if(count($csv) > 1) {
                $delimiter = ';';
            }
            $csv = explode(',', $data);
            if(count($csv) > 1) {
                $delimiter = ',';
            }
            $csv = explode("\t", $data);
            if(count($csv) > 1) {
                $delimiter = "\t";
            }
            fclose($handle);
        }
        return $delimiter;
    }
}