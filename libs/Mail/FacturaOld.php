<?php
/**
* 
*/
class Mail_FacturaOld extends Mail
{
    private $factura;
    private $idFactura;
    private $cliente;
    private $type;
    private $header;
    private $publicidades;

    function __construct($idFactura,$type = 'NEW_ORDER',$publicidades = false) {
        parent::__construct();
        if(is_array($type)) {
            $this->header = $type[0];
            $this->subject = $type[1];
            $this->footer = QueryHelper::getConf('FOOT_UPDATED_ORDER','EMAIL');
        } else {
            $this->header = QueryHelper::getConf('HEAD_'.$type,'EMAIL');
            $this->footer = QueryHelper::getConf('FOOT_'.$type,'EMAIL');
            $this->subject($idFactura);
        }
        $this->type = $type;
        $this->publicidades = $publicidades;
        $this->idFactura = $idFactura;
        $this->factura = Model::factory('Facturas')->where("idFactura",$idFactura)->find_one();
        $this->cliente = $this->factura->cliente();
        


        $this->to[] = array($this->cliente->emailCliente, $this->cliente->nombresCliente);
        $this->cc[] = QueryHelper::getConf('ORDER','EMAIL');   // Ojo, esto activar en produccion
        $this->from = QueryHelper::getConf('ORDER','EMAIL');
        $this->title();
        $this->head();
        $this->foot();
        $this->body();
        $this->get_laterales();
        //if(!$factura) { return false; }
    }

    public function checkdata() {
        if ($this->factura) { return true; }
        return false;
    }

    public function subject($idFactura, $altSubject = '') {
        if($altSubject) {
            $this->subject = str_replace('{idFactura}', $idFactura, $altSubject);
        } else {
            $this->subject = "Su pedido #".$idFactura." en Novedades Nguiller's";
        }
    }

    private function title() {
        $idFactura = $this->factura->idFactura;
        $this->title = '<span style="font-size:20px">ORDEN DE PEDIDO</span><br><span style="font-size:15px">Pedido N&uacute;mero: </span><span style="font-size:15px">'.$idFactura.'</span>';
    }

    private function body() {
        $factura = $this->factura;
$factor = 1/$factura->multiplicadorMoneda;
$decimal = ($factor == 1) ? 0 : 2;
$moneda = ($factor == 1) ? '$' : 'USD$';

        $cont = 0;
        $Total = 0;
        $detallefactura = $factura->detalle();
        $html = '<table cellpadding="3px" cellspacing="0px">'.
        '<thead style="border-right: medium none;"><tr style="font-size: 12px;">'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="5%">#</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="5%">Imagen</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="40%">Producto</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="10%">Referencia</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="15%">Presentacion</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="10%">V. Unit.</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="5%">Cant.</th>'.
        '<th style="border: 1px solid #E6E6E6; background:#eee;" width="15%">Total</th>'.
        '</tr></thead><tbody>';
        foreach ($detallefactura as $producto) {
            $cont++;
            $Total += $producto->valorTotal;
            $this->addImage($producto->imagenProducto, "producto".$producto->idProducto,IMG_PRD_PATH_CART,IMG_PRD_URL_CART);
            $html = $html.'<tr style="font-size: 11px;"><td style="border: 1px solid #E6E6E6;">'.$cont.'</td>'.
                    '<td style="border: 1px solid #E6E6E6;"><img width="50px" height="50px" src="cid:producto'.$producto->idProducto.'"></td>'.
                    '<td style="border: 1px solid #E6E6E6;">'.$producto->nombreProducto.'</td>'.
                    '<td style="border: 1px solid #E6E6E6;">'.$producto->refPaqueteProducto.'</td>'.
                    '<td style="border: 1px solid #E6E6E6;">'.$producto->nombrePresentacion.' de '.$producto->nombrePaquete.' '.$producto->medidaPresentacion.'</td>'.
                    '<td style="border: 1px solid #E6E6E6; text-align:right;">'.$moneda.number_format($producto->valorUnitario*$factor,$decimal).'</td>'.
                    '<td style="border: 1px solid #E6E6E6; text-align:right;">'.$producto->cantidad.'</td>'.
                    '<td style="border: 1px solid #E6E6E6; text-align:right;">'.$moneda.number_format($producto->valorTotal*$factor,$decimal).'</td></tr>';
        }

        $flete = ($factura->flete) ? $factura->flete : 0;
        $adicionales = ($factura->adicionales) ? $factura->adicionales : 0;
        $html = $html.'</tbody><tfoot><tr><td colspan="4" rowspan="4" style="border: 1px solid #E6E6E6; font-size: 10px; font-family:Arial,Helvetica,sans-serif; text-align:right;"></td>';
        if($flete || $adicionales) {
            $html = $html.'<td colspan="2" style="font-weight:bold; border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right;">Subtotal</td><td style="border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right; font-weight:bold;" colspan="2">'.$moneda.' '.number_format($Total*$factor,$decimal).'</td></tr>';
            if($flete) {
                $html = $html.'<tr><td colspan="2" style="font-weight:bold; border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right;">Flete</td><td style="border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right; font-weight:bold;" colspan="2">'.$moneda.' '.number_format($flete*$factor,$decimal).'</td></tr>';
            }
            if($adicionales) {
                $html = $html.'<tr><td colspan="2" style="font-weight:bold; border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right;">Comisión Bancaria</td><td style="border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right; font-weight:bold;" colspan="2">'.$moneda.' '.number_format($adicionales*$factor,$decimal).'</td></tr>';
            }
        }
        $html = $html.'<tr><td colspan="2" style="font-weight:bold; border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right;">Total</td><td style="border: 1px solid #E6E6E6; font-size: 14px; font-family:Arial,Helvetica,sans-serif; text-align:right; font-weight:bold;" colspan="2">'.$moneda.' '.number_format(($Total+$adicionales+$flete)*$factor,$decimal).'</td></tr></tfoot></table>';
        $this->body = $html;
    }

    private function head() {
        $cliente = $this->cliente;
        $factura = $this->factura;
        $nombre = $cliente->nombresCliente;
        $billing = '<table cellpadding="3px" cellspacing="0px" width="100%" style="font-size:12px">'.
                        '<thead><tr style="text-align:left">'.
                                '<th>Informacion de Facturación</th>'.
                        '</tr></thead>'.
                        '<tbody>'.
                            '<tr>'.
                                '<td>'.$cliente->nombresCliente.' '.$cliente->apellidosCliente.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->direccionCliente.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->ciudadCliente.', '.
                                $cliente->estadoCliente.', '.
                                $cliente->paisCliente.', '.
                                $cliente->codigoPostalCliente.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->telefonoCliente.'</td>'.
                            '</tr>'.
                        '</tbody>'.
                    '</table>';

        if(!$factura->use_billing) {
            $shipping = '<table cellpadding="3px" cellspacing="0px" width="100%" style="font-size:12px">'.
                        '<thead><tr style="text-align:left">'.
                                '<th>Informacion de Envío</th>'.
                        '</tr></thead>'.
                        '<tbody>'.
                            '<tr>'.
                                '<td>'.$cliente->nombresCorrespondencia.' '.$cliente->apellidosCorrespondencia.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->direccionCorrespondencia.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->ciudadCorrespondencia.', '.
                                $cliente->estadoCorrespondencia.', '.
                                $cliente->paisCorrespondencia.', '.
                                $cliente->codigoPostalCorrespondencia.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->telefonoCorrespondencia.'</td>'.
                            '</tr>'.
                        '</tbody>'.
                    '</table>';
        } else {
            $shipping = '<table cellpadding="3px" cellspacing="0px" width="100%" style="font-size:12px">'.
                        '<thead><tr style="text-align:left">'.
                                '<th>Informacion de Envío</th>'.
                        '</tr></thead>'.
                        '<tbody>'.
                            '<tr>'.
                                '<td>'.$cliente->nombresCliente.' '.$cliente->apellidosCliente.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->direccionCliente.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->ciudadCliente.', '.
                                $cliente->estadoCliente.', '.
                                $cliente->paisCliente.', '.
                                $cliente->codigoPostalCliente.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td>'.$cliente->telefonoCliente.'</td>'.
                            '</tr>'.
                        '</tbody>'.
                    '</table>';
        }


        $numGuia = ($factura->numeroGuia) ? '<tr><td colspan="2">Número de Guía: <b>'.$factura->numeroGuia.'</b></td></tr>' : '';
        $html = 
                '<td valign="top" colspan="3" style="font-size: 12px; font-family:Arial,Helvetica,sans-serif;"><br><br>'.
                '<b>Apreciado(a) / Dear : &nbsp;&nbsp;&nbsp;'.$nombre.'</b><br><br>'.
                    $this->header.
                '</td>'.
            '</tr>'.
            '<tr>'.
                '<td valign="top" colspan="3"><br>'.
                    '<table cellpadding="3px" cellspacing="0px" width="550px" style="font-size:12px">'.
                        '<tbody>'.
                            '<tr>'.
                                '<td>'.$billing.'</td>'.
                                '<td>'.$shipping.'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="2">Opciones de envio: <b>'.$factura->nombreCarrier().'</b></td>'.
                            '</tr>'.
                            $numGuia.
                            '<tr>'.
                                '<td colspan="2">Forma de Pago: <b>'.$factura->nombrePayment().'</b></td>'.
                            '</tr>'.
                        '</tbody>'.
                    '</table>'.
                    '<br>'.
                '</td>'
        ;


        $this->head = $html;
        return $this->head;
    }

    private function foot() {
        $html = 
                '<td valign="top" colspan="3" style="font-size:12px"><br>'.
                    $this->footer.
                '<br><br></td>'.
            '</tr>'.
            '<tr>'.
                '<td valign="top" colspan="3" style="font-size:12px">'.
                    QueryHelper::getConf('FIRMA_COTIZACIONES','PEDIDO').
                '</td>'
        ;
        $this->foot = $html;
        //$this->head = QueryHelper::getConf('HEAD','PEDIDO');
    }

    private function get_laterales() {
        if($this->publicidades) {
            if(!$this->laterales) {
                $Publicidades = Model::factory('Publicidades')->where('idTipoPublicidad',1)->where('emailPublicidad',1)->order_by_asc('ordenPublicidad')->find_many();
                $pub = array();
                foreach($Publicidades as $banner) {
                    //$this->Mailer->AddEmbeddedImage(IMG_PUB_PATH.$banner->fotoPublicidad, "lateral".$banner->idPublicidad);
                    $this->addImage($banner->fotoPublicidad, "lateral".$banner->idPublicidad,IMG_PUB_PATH,IMG_PUB_URL);
                    $pub[] = '<a href="'.$banner->linkPublicidad.'"><img src="cid:lateral'.$banner->idPublicidad.'" alt="" width="190" /></a>';
                }
                $this->laterales = '<br>'.join('<br>',$pub);
            }
        } else {
            $this->laterales = '';
        }
        return $this->laterales;
    }
}