<?php
/**
* 
*/
class Mail_Cupon extends Mail
{
    private $cliente;
    private $type;

    function __construct($idEnvio, $codigoCupon = '')
    {
        $envio = Model::factory('CuponesEnvios')->where('idEnvio',$idEnvio)->find_one();
        $idCupon = $envio->idCupon;
        if($envio->especial) {
            $message = QueryHelper::get_coupon_mail($idCupon, false, true, true);

        } else {
            $message = QueryHelper::get_coupon_mail($idCupon, false, false, true);
        }
        if(!$codigoCupon) {
            if($envio->generico) {
                $codigoCupon = $envio->prefijo;
            } else {
                $codigoCupon = $envio->codigo;
            }
        }
        $cupon = $envio->cupon();
        $this->header = QueryHelper::getConf('HEAD_CUPON','EMAIL');
        $this->subject = $this->header;

        $this->firma = QueryHelper::getConf('FIRMA_COTIZACIONES','PEDIDO');
        $this->address = QueryHelper::getConf('ADDRESS','EMAIL');

        $this->idCupon = $idCupon;
        $html = file_get_contents(MAIL_PATH.'commonText.html');
        $html = str_replace('{cliente:nombresCliente}', 'Apreciado Cliente', $html);
        $message = str_replace('{custom:codigoCupon}', $codigoCupon, $message);
        
        

        // $this->productos = $this->ticket->detalle();
        // $this->comercial = ($this->cliente->asesor()) ? $this->cliente->asesor() : Session::get('user_data');

        // $this->comercial = (Session::get('user_data')) ? Session::get('user_data') : $this->cliente->asesor();
        $logo = QueryHelper::logo(true);
        $this->addImage($logo, 'logo', IMG_LOG_PATH, IMG_LOG_URL);
        $this->addImage('icon_youtube.png', 'youtube', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_twitter.png', 'twitter', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_facebook.png', 'facebook', MAIL_PATH.'images/', MAIL_URL.'images/');
        $publicidad = Model::factory('Publicidades')->where('idTipoPublicidad',3)->order_by_expr('rand()')->find_one();
        

        // $this->addImage('banner_cotizaciones_2.jpg', 'banner_cotizaciones_2',MAIL_PATH,MAIL_URL);
        // $this->addImage('banner_cotizaciones_3.jpg', 'banner_cotizaciones_3',MAIL_PATH,MAIL_URL);

// QueryHelper::getConf('HEAD_'.$this->type,'EMAIL');
        $url = 'http://'.$_SERVER['SERVER_NAME'].URL;
        $url = str_replace('admin/', '', $url);
        $urlCupon = $url.'contacto/email_cupon/'.$idCupon;
        $html = str_replace('{url}', $url, $html);
        $html = str_replace('{urlCupon}', $urlCupon.'/'.$codigoCupon, $html);
        $html = str_replace('{copyright_notice}', QueryHelper::getConf('COPYRIGHT_NOTICE','EMAIL'), $html);
        $html = str_replace('{header}', $this->header, $html);
        // $html = str_replace('{title}', '', $html);
        $html = str_replace('{title}', 'Cupon '.$codigoCupon, $html);
        $html = str_replace('{subject}', $this->subject, $html);
        $html = str_replace('{message}', $message, $html);

        // $html = str_replace('{footer}', $this->footer, $html);
        $html = str_replace('{firma}', $this->firma, $html);
        $html = str_replace('{address}', $this->address, $html);

        // $html = str_replace('{address}', $this->address, $html);

        $publicidadstart = strpos($html,'{begin:publicidad}');
        $publicidadend = strpos($html,'{end:publicidad}')+16;

        if($publicidad) {
            switch ($publicidad->idTipoLink) {
                case 3:
                    $html = str_replace('{custom:linkPublicidad}', $publicidad->linkPublicidad, $html);
                    break;
                case 2:
                    $html = str_replace('{custom:linkPublicidad}', $url.'buscar/'.$publicidad->linkPublicidad, $html);
                    break;
                default:
                    $html = str_replace('{custom:linkPublicidad}', '#', $html);
                    break;
            }
            $this->addImage($publicidad->fotoPublicidad, 'publicidad', IMG_PUB_PATH, IMG_PUB_URL);
            $html = str_replace('{begin:publicidad}', '', $html);
            $html = str_replace('{end:publicidad}', '', $html);
        } else {
            $publicidadhtml = substr($html, $publicidadstart, $publicidadend - $publicidadstart);
            $html = str_replace($publicidadhtml, '', $html);
        }

        // $cupon = Model::factory('CuponesOtros')->get(1)->find_one();
        // if($cupon->is_active()) {
        //     $html = str_replace('{custom:coupon}', 'Aqui va el cupon', $html);
        // } else {
            $html = str_replace('{custom:coupon}', '', $html);
        // }
        
        $this->message = $html;
        // echo $this->message;

        $this->to[] = $this->loadTo($envio);
        // $mail = ($this->ticket->emailTo) ? $this->ticket->emailTo : QueryHelper::getConf('CONTACT','EMAIL');

        $this->cc[] = QueryHelper::getConf('CONTACT','EMAIL');
        $this->from = QueryHelper::getConf('NO_REPLY','EMAIL');

        // $this->replyTo = @$this->user->emailUsuario;
        // if(@$asesor) {
        //     $this->cc[] = $asesor->emailUsuario;
        //     $this->replyTo = $asesor->emailUsuario;
        // }
        parent::__construct();
    }

    public function checkdata() {
        return true;
    }

    public function subject($idCupon, $altSubject = '') {
        if($altSubject) {
            $this->subject = str_replace('{idCupon}', $idCupon, $altSubject);
        } else {
            if($this->ticket->idEstado == 1) {
                $this->subject = "Nguillers - Se ha creado el PQR #".$idCupon;
            } elseif($this->ticket->idEstado == 4) {
                $this->subject = "Nguillers - Se ha cerrado el PQR #".$idCupon;
            } else {
                $this->subject = "Nguillers - Respuesta a PQR #".$idCupon;
            }
        }
    }

    public function loadTo($envio) {
        $to = array();
        if($envio->idPerfil) {
            $clientes = Model::factory('Clientes')->select('emailCliente')->where('idPerfil',$envio->idPerfil)->find_many();
            foreach ($clientes as $cliente) {
                $to[] = $cliente->emailCliente;
            }
        }
        // var_dump($envio->clientes());
        if(count($clientes = $envio->clientes())) {
            foreach ($clientes as $cliente) {
                $to[] = $cliente->cliente()->emailCliente;
            }
        }
        return $to;
    }
}