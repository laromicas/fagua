<?php
/**
* 
*/
class Mail_Registro extends Mail
{
    private $cliente;
    private $type;
    private $emailCliente;
    public $link = 'confirm';
    public $title = 'CONFIRMACION DE REGISTRO';

    function __construct($emailCliente,$type = 'NEW_USER')
    {
        parent::__construct();
        $this->type = $type;
        if($type == 'RESET_PASSWORD') {
            $this->link = 'resetpass';
            $this->title = 'RESTITUCION DE CONTRASEÑA';
        }
        $this->emailCliente = $emailCliente;
        $this->cliente = Model::factory('Clientes')->where("emailCliente",$emailCliente)->find_one();
        $this->to();
        $this->from();
        $this->subject();
        $this->body();
        $this->title();
        $this->head();
        $this->foot();
        //if(!$factura) { return false; }
    }

    public function checkdata() {
        if ($this->cliente) { return true; }
        return false;
    }

    private function to() {
        $this->to = array($this->emailCliente);
    }

    private function from() {
        $this->from = QueryHelper::getConf('CONTACT','EMAIL');
        $this->replyTo = QueryHelper::getConf('NO_REPLY','EMAIL');
    }

    private function subject() {
        $this->subject = "Novedades Nguiller's";
    }

    private function title() {
        $this->title = '<span style="font-size:20px">'.$this->title.'</span>';
    }

    private function body() {
        $cliente = $this->cliente;
        $cont = 0;
        $Total = 0;
        $hash = $cliente->hashConfirmacion;
        $html = '<a href="http://'.$_SERVER["HTTP_HOST"].URL.'login/'.$this->link.'/'.$hash.'">http://'.$_SERVER["HTTP_HOST"].URL.'login/'.$this->link.'/'.$hash.'</a>';
        $this->body = $html;
    }

    private function head() {
        $cliente = $this->cliente;
        $html = 
            '<td valign="top" colspan="3" style="font-size: 11px; font-family:Arial,Helvetica,sans-serif;">'.
            '<br><p style="font-size:14"><b>Apreciado(a) '.
            ''.$this->cliente->nombresCliente.
            '</p><br></b><p>'.
                QueryHelper::getConf('HEAD_'.$this->type,'EMAIL').
            '</p><br></td>'
        ;
        $this->head = $html;
    }

    private function foot() {
        $html = 
                '<td valign="top" colspan="3">'.
                '</td>'.
            '</tr>'.
            '<tr>'.
                '<td valign="top" colspan="3">'.
                '</td>'
        ;
        $this->foot = $html;
        //$this->head = QueryHelper::getConf('HEAD','PEDIDO');
    }
}