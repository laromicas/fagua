<?php
/**
* 
*/
class Mail_Contacto extends Mail
{
    private $nombre;
    private $email;
    private $telefono;
    private $comentario;

    function __construct($post,$type = 'NEW_CONTACT')
    {
        parent::__construct();
        $this->type = $type;
        $this->nombre = $post['name'];
        $this->email = $post['email'];
        $this->telefono = $post['telephone'];
        $this->comentario = $post['comment'];

        $this->header = QueryHelper::getConf('HEAD_'.$type,'EMAIL');

        $this->firma = QueryHelper::getConf('FIRMA_COTIZACIONES','PEDIDO');
        $this->address = QueryHelper::getConf('ADDRESS','EMAIL');

        $this->title = "CONTACTO PAGINA";
        $this->subject = "Contacto Pagina Novedades Guiller's";
        $html = file_get_contents(MAIL_PATH.'commonText.html');

        $logo = QueryHelper::logo(true);
        $this->addImage($logo, 'logo', IMG_LOG_PATH, IMG_LOG_URL);
        $this->addImage('icon_youtube.png', 'youtube', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_twitter.png', 'twitter', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_facebook.png', 'facebook', MAIL_PATH.'images/', MAIL_URL.'images/');

        $html = str_replace('{subject}', $this->subject, $html);
        $html = str_replace('{title}', $this->title, $html);
        // $html = str_replace('{message}', $this->comentario, $html);
        $html = str_replace('{header}', $this->header, $html);
        $html = str_replace('{cliente:nombresCliente}', $this->nombre, $html);
        $html = str_replace('{firma}', $this->firma, $html);
        $html = str_replace('{address}', $this->address, $html);

        $message = 
                '<div>'.
                    '<span style="font-size:12px">Nombre:</span> '.
                    '<span style="font-size:11px">'.$this->nombre.'</span><br/>'.
                    '<span style="font-size:12px">Email:</span> '.
                    '<span style="font-size:11px">'.$this->email.'</span><br/>'.
                    '<span style="font-size:12px">Telefono:</span> '.
                    '<span style="font-size:11px">'.$this->telefono.'</span><br/>'.
                    '<span style="font-size:12px">Comentario:</span> '.
                    '<span style="font-size:11px">'.$this->comentario.'</span><br/>'.
                '</div>'
        ;
        $html = str_replace('{message}', $message, $html);

        $this->from = $this->email;
        if(is_array(@$post['tipoTicket'])) {
            $idTipo = end($post['tipoTicket']);
            $ticket = Model::factory('TiposTicket')->where('idTipo',$idTipo)->find_one();
            if(!$ticket) {
                $this->to = QueryHelper::getConf('CONTACT','EMAIL');
            } else {
                $this->to = ($ticket->emailTo) ? $ticket->emailTo : QueryHelper::getConf('CONTACT','EMAIL');
            }
        } else {
            $this->to = QueryHelper::getConf('CONTACT','EMAIL');
        }

        // $this->cc = QueryHelper::getConf('ORDER','EMAIL');   // Ojo, esto activar en produccion
        // $this->from = QueryHelper::getConf('TICKET','EMAIL');
        $this->subject = "Consulta de contacto en Novedades Nguiller's";
        $this->replyTo = $this->email;

        $publicidad = Model::factory('Publicidades')->where('idTipoPublicidad',3)->order_by_expr('rand()')->find_one();
        $publicidadstart = strpos($html,'{begin:publicidad}');
        $publicidadend = strpos($html,'{end:publicidad}')+16;

        if($publicidad) {
            switch ($publicidad->idTipoLink) {
                case 3:
                    $html = str_replace('{custom:linkPublicidad}', $publicidad->linkPublicidad, $html);
                    break;
                case 2:
                    $html = str_replace('{custom:linkPublicidad}', $url.'buscar/'.$publicidad->linkPublicidad, $html);
                    break;
                default:
                    $html = str_replace('{custom:linkPublicidad}', '#', $html);
                    break;
            }
            $this->addImage($publicidad->fotoPublicidad, 'publicidad', IMG_PUB_PATH, IMG_PUB_URL);
            $html = str_replace('{begin:publicidad}', '', $html);
            $html = str_replace('{end:publicidad}', '', $html);
        } else {
            $publicidadhtml = substr($html, $publicidadstart, $publicidadend - $publicidadstart);
            $html = str_replace($publicidadhtml, '', $html);
        }


        $this->message = $html;
    }

    public function checkdata() {
        return true;
    }

}