<?php
/**
* 
*/
class Mail_Registro extends Mail
{
    public $cliente;
    private $type;
    private $emailCliente;
    public $link = 'confirm';
    public $title = 'CONFIRMACION DE REGISTRO';

    function __construct($emailCliente,$type = 'NEW_USER', $hash='newhash')
    {
        $this->subject = "Confirmacion Registro Novedades Guiller's";
        if($type == 'RESET_PASSWORD') {
            $this->link = 'resetpass';
            $this->title = 'RESTITUCION DE CONTRASEÑA';
            $this->subject = "Restitucion Password Novedades Guiller's";
        }

        $this->firma = QueryHelper::getConf('FIRMA_COTIZACIONES','PEDIDO');
        $this->address = QueryHelper::getConf('ADDRESS','EMAIL');

        $this->type = $type;
        $html = file_get_contents(MAIL_PATH.'commonLink.html');

        $this->header = QueryHelper::getConf('HEAD_'.$type,'EMAIL');

        $this->cliente = Model::factory('Clientes')->where('emailCliente',$emailCliente)->find_one();
        $logo = QueryHelper::logo(true);
        $this->addImage($logo, 'logo', IMG_LOG_PATH, IMG_LOG_URL);
        $this->addImage('icon_youtube.png', 'youtube', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_twitter.png', 'twitter', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_facebook.png', 'facebook', MAIL_PATH.'images/', MAIL_URL.'images/');
        $publicidad = Model::factory('Publicidades')->where('idTipoPublicidad',3)->order_by_expr('rand()')->find_one();

        // $this->addImage('banner_cotizaciones_2.jpg', 'banner_cotizaciones_2',MAIL_PATH,MAIL_URL);
        // $this->addImage('banner_cotizaciones_3.jpg', 'banner_cotizaciones_3',MAIL_PATH,MAIL_URL);

        $hash = ($hash != 'newhash') ? $hash : $this->cliente->hashConfirmacion;

        $html = str_replace('{link}', '{url}login/'.$this->link.'/'.$hash, $html);

        $url = 'http://'.$_SERVER['SERVER_NAME'].URL;
        $url = str_replace('admin/', '', $url);
        $urlTicket = $url.'contacto/';
        $html = str_replace('{url}', $url, $html);
        $html = str_replace('{urlTicket}', $urlTicket, $html);
        $html = str_replace('{copyright_notice}', QueryHelper::getConf('COPYRIGHT_NOTICE','EMAIL'), $html);
        $html = str_replace('{header}', $this->header, $html);
        // $html = str_replace('{title}', '', $html);
        if($type == 'RESET_PASSWORD') {
            $html = str_replace('{title}', 'RESTITUCION DE CONTRASEÑA', $html);
        }
        $html = str_replace('{title}', '', $html);
        $html = str_replace('{subject}', $this->subject, $html);

        // $html = str_replace('{footer}', $this->footer, $html);
        $html = str_replace('{firma}', $this->firma, $html);
        $html = str_replace('{address}', $this->address, $html);

        $publicidadstart = strpos($html,'{begin:publicidad}');
        $publicidadend = strpos($html,'{end:publicidad}')+16;

        if($publicidad) {
            switch ($publicidad->idTipoLink) {
                case 3:
                    $html = str_replace('{custom:linkPublicidad}', $publicidad->linkPublicidad, $html);
                    break;
                case 2:
                    $html = str_replace('{custom:linkPublicidad}', $url.'buscar/'.$publicidad->linkPublicidad, $html);
                    break;
                default:
                    $html = str_replace('{custom:linkPublicidad}', '#', $html);
                    break;
            }
            $this->addImage($publicidad->fotoPublicidad, 'publicidad', IMG_PUB_PATH, IMG_PUB_URL);
            $html = str_replace('{begin:publicidad}', '', $html);
            $html = str_replace('{end:publicidad}', '', $html);
        } else {
            $publicidadhtml = substr($html, $publicidadstart, $publicidadend - $publicidadstart);
            $html = str_replace($publicidadhtml, '', $html);
        }

        $html = str_replace('{custom:coupon}', QueryHelper::get_coupon_mail(1), $html);
        
        $tables = array('cliente'=>$this->cliente);

        foreach ($tables as $keytable => $table) {
            foreach ($table->as_array() as $key => $value) {
                $html = str_replace('{'.$keytable.':'.$key.'}', $value, $html);
            }
        }

        $this->message = $html;

        $this->from = QueryHelper::getConf('NO_REPLY','EMAIL');
        $this->replyTo = QueryHelper::getConf('NO_REPLY','EMAIL');

        $this->to = array($this->cliente->emailCliente, $this->cliente->nombresCliente.' '.$this->cliente->apellidosCliente);
        
        parent::__construct();
    }

    public function checkdata() {
        return true;
    }

    public function subject($idTicket, $altSubject = '') {
        if($altSubject) {
            $this->subject = str_replace('{idTicket}', $idTicket, $altSubject);
        } else {
            $this->subject = "Respuesta a PQR #".$idTicket;
        }
    }
}