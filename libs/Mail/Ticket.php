<?php
/**
* 
*/
class Mail_Ticket extends Mail
{
    private $cliente;
    private $type;

    function __construct($idTicket, $type = 'TICKET')
    {

        $this->ticket = Model::factory('Tickets')->where("idTicket",$idTicket)->find_one();
        if(is_array($type)) {
            $this->header = $type[0];
            $this->subject = $type[1];
            // $this->footer = QueryHelper::getConf('FOOT_UPDATED_ORDER','EMAIL');
        } else {
            $this->header = QueryHelper::getConf('HEAD_'.$type,'EMAIL');
            // $this->footer = QueryHelper::getConf('FOOT_'.$type,'EMAIL');
            $this->subject(str_pad($idTicket, 5, '0', STR_PAD_LEFT));
        }

        $this->firma = QueryHelper::getConf('FIRMA_COTIZACIONES','PEDIDO');
        $this->address = QueryHelper::getConf('ADDRESS','EMAIL');

        $this->type = $type;
        $this->idTicket = $idTicket;
        $html = file_get_contents(MAIL_PATH.'commonLink.html');

        $this->ticket->idTicket = str_pad($this->ticket->idTicket, 5, '0', STR_PAD_LEFT);
        $this->cliente = $this->ticket->cliente();
        // $this->productos = $this->ticket->detalle();
        // $this->comercial = ($this->cliente->asesor()) ? $this->cliente->asesor() : Session::get('user_data');

        // $this->comercial = (Session::get('user_data')) ? Session::get('user_data') : $this->cliente->asesor();
        $logo = QueryHelper::logo(true);
        $this->addImage($logo, 'logo', IMG_LOG_PATH, IMG_LOG_URL);
        $this->addImage('icon_youtube.png', 'youtube', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_twitter.png', 'twitter', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_facebook.png', 'facebook', MAIL_PATH.'images/', MAIL_URL.'images/');
        $publicidad = Model::factory('Publicidades')->where('idTipoPublicidad',3)->order_by_expr('rand()')->find_one();
        

        // $this->addImage('banner_cotizaciones_2.jpg', 'banner_cotizaciones_2',MAIL_PATH,MAIL_URL);
        // $this->addImage('banner_cotizaciones_3.jpg', 'banner_cotizaciones_3',MAIL_PATH,MAIL_URL);

// QueryHelper::getConf('HEAD_'.$this->type,'EMAIL');
        $html = str_replace('{link}', '{url}account/ticket/{ticket:idTicket}', $html);

        $url = 'http://'.$_SERVER['SERVER_NAME'].URL;
        $url = str_replace('admin/', '', $url);
        $urlTicket = $url.'account/ticket/'.$idTicket;
        $html = str_replace('{url}', $url, $html);
        $html = str_replace('{urlTicket}', $urlTicket, $html);
        $html = str_replace('{copyright_notice}', QueryHelper::getConf('COPYRIGHT_NOTICE','EMAIL'), $html);
        $html = str_replace('{header}', $this->header, $html);
        // $html = str_replace('{title}', '', $html);
        $html = str_replace('{title}', 'PQR No. {ticket:idTicket}', $html);
        $html = str_replace('{subject}', $this->subject, $html);

        // $html = str_replace('{footer}', $this->footer, $html);
        $html = str_replace('{firma}', $this->firma, $html);
        $html = str_replace('{address}', $this->address, $html);

        // $html = str_replace('{address}', $this->address, $html);

        $publicidadstart = strpos($html,'{begin:publicidad}');
        $publicidadend = strpos($html,'{end:publicidad}')+16;

        if($publicidad) {
            switch ($publicidad->idTipoLink) {
                case 3:
                    $html = str_replace('{custom:linkPublicidad}', $publicidad->linkPublicidad, $html);
                    break;
                case 2:
                    $html = str_replace('{custom:linkPublicidad}', $url.'buscar/'.$publicidad->linkPublicidad, $html);
                    break;
                default:
                    $html = str_replace('{custom:linkPublicidad}', '#', $html);
                    break;
            }
            $this->addImage($publicidad->fotoPublicidad, 'publicidad', IMG_PUB_PATH, IMG_PUB_URL);
            $html = str_replace('{begin:publicidad}', '', $html);
            $html = str_replace('{end:publicidad}', '', $html);
        } else {
            $publicidadhtml = substr($html, $publicidadstart, $publicidadend - $publicidadstart);
            $html = str_replace($publicidadhtml, '', $html);
        }

        // $cupon = Model::factory('CuponesOtros')->get(1)->find_one();
        // if($cupon->is_active()) {
        //     $html = str_replace('{custom:coupon}', 'Aqui va el cupon', $html);
        // } else {
            $html = str_replace('{custom:coupon}', '', $html);
        // }
        
        $tables = array('ticket'=>$this->ticket,'cliente'=>$this->cliente);

        foreach ($tables as $keytable => $table) {
            foreach ($table->as_array() as $key => $value) {
                $html = str_replace('{'.$keytable.':'.$key.'}', $value, $html);
            }
        }

        $this->message = $html;

        $this->to[] = $this->cliente->emailCliente;
        $mail = ($this->ticket->emailTo) ? $this->ticket->emailTo : QueryHelper::getConf('CONTACT','EMAIL');

        $this->cc[] = $mail; //QueryHelper::getConf('ORDER','EMAIL');
        $this->from = $mail;

        $this->replyTo = @$this->user->emailUsuario;
        if(@$asesor) {
            $this->cc[] = $asesor->emailUsuario;
            $this->replyTo = $asesor->emailUsuario;
        }
        
        $cc = explode(',',$this->ticket->emailsAdicionales);
        foreach ($cc as $email) {
            $this->cc[] = $email;
        }

        parent::__construct();
    }

    public function checkdata() {
        if ($this->ticket) { return true; }
        return false;
    }

    public function subject($idTicket, $altSubject = '') {
        if($altSubject) {
            $this->subject = str_replace('{idTicket}', $idTicket, $altSubject);
        } else {
            if($this->ticket->idEstado == 1) {
                $this->subject = "Nguillers - Se ha creado el PQR #".$idTicket;
            } elseif($this->ticket->idEstado == 4) {
                $this->subject = "Nguillers - Se ha cerrado el PQR #".$idTicket;
            } else {
                $this->subject = "Nguillers - Respuesta a PQR #".$idTicket;
            }
        }
    }
}