<?php
/**
* 
*/
class Mail_Contacto extends Mail
{
    private $nombre;
    private $email;
    private $telefono;
    private $comentario;

    function __construct($post,$type = 'NEW_CONTACT')
    {
        parent::__construct();
        $this->type = $type;
        $this->nombre = $post['name'];
        $this->email = $post['email'];
        $this->telefono = $post['telephone'];
        $this->comentario = $post['comment'];
        if(is_array(@$post['tipoTicket'])) {
            $idTipo = end($post['tipoTicket']);
            $ticket = Model::factory('TiposTicket')->where('idTipo',$idTipo)->find_one();
            if(!$ticket) {
                $this->to = QueryHelper::getConf('CONTACT','EMAIL');
            } else {
                $this->to = ($ticket->emailTo) ? $ticket->emailTo : QueryHelper::getConf('CONTACT','EMAIL');
            }
        } else {
            $this->to = QueryHelper::getConf('CONTACT','EMAIL');
        }

        // $this->cc = QueryHelper::getConf('ORDER','EMAIL');   // Ojo, esto activar en produccion
        // $this->from = QueryHelper::getConf('TICKET','EMAIL');
        $this->subject = "Consulta de contacto en Novedades Nguiller's";
        $this->replyTo = $this->email;

        $this->body();
        $this->title();
        $this->head();
        $this->foot();

    }

    public function checkdata() {
        return true;
    }

    private function title() {
        $this->title = '<span style="font-size:20px">CONTACTO PAGINA</span>';
    }

    private function body() {
        $html = 
            '<span style="font-size:12px">Comentario:</span> '.
            '<span style="font-size:11px">'.$this->comentario.'</span><br/>'
        ;
        $this->body = $html;
    }

    private function head() {
        $html = 
                '<td valign="top" colspan="3">'.
                    '<span style="font-size:12px">Nombre:</span> '.
                    '<span style="font-size:11px">'.$this->nombre.'</span><br/>'.
                    '<span style="font-size:12px">Email:</span> '.
                    '<span style="font-size:11px">'.$this->email.'</span><br/>'.
                    '<span style="font-size:12px">Telefono:</span> '.
                    '<span style="font-size:11px">'.$this->telefono.'</span><br/>'.
                '</td>'
        ;
        $this->head = $html;
    }

    private function foot() {
        $html = 
                '<td valign="top" colspan="3">'.
                '</td>'.
            '</tr>'.
            '<tr>'.
                '<td valign="top" colspan="3">'.
                    //'Hola 1'.
                '</td>'
        ;
        $this->foot = $html;
        //$this->head = QueryHelper::getConf('HEAD','PEDIDO');
    }

    public function get_laterales() {
        $this->laterales = '';
        return '';
    }
}