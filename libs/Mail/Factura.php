<?php
/**
* 
*/
class Mail_Factura extends Mail
{
    public $factura;
    private $idFactura;
    public $cliente;
    private $type;

    function __construct($idFactura, $type = 'NEW_ORDER')
    {

        if(is_array($type)) {
            $this->header = $type[0];
            $this->subject = $type[1];
            $this->footer = QueryHelper::getConf('FOOT_UPDATED_ORDER','EMAIL');
        } else {
            $this->header = QueryHelper::getConf('HEAD_'.$type,'EMAIL');
            $this->footer = QueryHelper::getConf('FOOT_'.$type,'EMAIL');
            $this->subject($idFactura);
        }

        $this->firma = QueryHelper::getConf('FIRMA_COTIZACIONES','PEDIDO');
        $this->address = QueryHelper::getConf('ADDRESS','EMAIL');

        $this->type = $type;
        $this->idFactura = $idFactura;
        $html = file_get_contents(MAIL_PATH.'pedido.html');

        $this->factura = Model::factory('Facturas')->where("idFactura",$idFactura)->find_one();

        if(file_exists(INVOICE_PATH.$this->factura->idFactura.".pdf")) {
            $this->addAttachment(INVOICE_PATH.$this->factura->idFactura.".pdf");
        }

        $this->factura->carrier = $this->factura->nombreCarrier();
        $this->factura->payment = $this->factura->nombrePayment();
        if(!$this->factura->numeroGuia) {
            $this->factura->numeroGuia = "No Disponible";
        }

        // idEmpresaTrasportadora
        $this->cliente = $this->factura->cliente();
        $this->productos = $this->factura->detalle();
        // $this->comercial = ($this->cliente->asesor()) ? $this->cliente->asesor() : Session::get('user_data');

        // $this->comercial = (Session::get('user_data')) ? Session::get('user_data') : $this->cliente->asesor();

        $logo = QueryHelper::logo(true);
        $this->addImage($logo, 'logo', IMG_LOG_PATH, IMG_LOG_URL);
        $this->addImage('icon_youtube.png', 'youtube', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_twitter.png', 'twitter', MAIL_PATH.'images/', MAIL_URL.'images/');
        $this->addImage('icon_facebook.png', 'facebook', MAIL_PATH.'images/', MAIL_URL.'images/');
        $publicidad = Model::factory('Publicidades')->where('idTipoPublicidad',3)->order_by_expr('rand()')->find_one();
        
        // $this->addImage('publicidad_cotizaciones_2.jpg', 'banner_cotizaciones_2',MAIL_PATH,MAIL_URL);
        // $this->addImage('banner_cotizaciones_3.jpg', 'banner_cotizaciones_3',MAIL_PATH,MAIL_URL);

// QueryHelper::getConf('HEAD_'.$this->type,'EMAIL');

        $url = 'http://'.$_SERVER['SERVER_NAME'].URL;
        $url = str_replace('admin/', '', $url);
        $urlFactura = $url.'account/order/'.$idFactura.'/'.$type;
        $html = str_replace('{url}', $url, $html);
        $html = str_replace('{urlFactura}', $urlFactura, $html);
        $html = str_replace('{copyright_notice}', QueryHelper::getConf('COPYRIGHT_NOTICE','EMAIL'), $html);
        $html = str_replace('{header}', $this->header, $html);
        $html = str_replace('{subject}', $this->subject, $html);
        $html = str_replace('{footer}', $this->footer, $html);
        $html = str_replace('{firma}', $this->firma, $html);
        $html = str_replace('{address}', $this->address, $html);
        // $html = str_replace('{firma}', $this->comercial->firmaUsuario, $html);

        if($type == 'FINAL_ORDER') {
            $carrier = $this->factura->carrier();
            $boton = '<a href="'.$url.'mail/tracking/'.$idFactura.'">'.$this->factura->numeroGuia.'</a>';
            $html = str_replace('{custom:tracking}', $boton, $html);
        } else {
            $html = str_replace('{custom:tracking}', '', $html);
        }

        $publicidadstart = strpos($html,'{begin:publicidad}');
        $publicidadend = strpos($html,'{end:publicidad}')+16;

        if($publicidad) {

            switch ($publicidad->idTipoLink) {
                case 3:
                    $html = str_replace('{custom:linkPublicidad}', $publicidad->linkPublicidad, $html);
                    break;
                case 2:
                    $html = str_replace('{custom:linkPublicidad}', $url.'buscar/'.$publicidad->linkPublicidad, $html);
                    break;
                default:
                    $html = str_replace('{custom:linkPublicidad}', '#', $html);
                    break;
            }

            $this->addImage($publicidad->fotoPublicidad, 'publicidad', IMG_PUB_PATH, IMG_PUB_URL);
            $html = str_replace('{begin:publicidad}', '', $html);
            $html = str_replace('{end:publicidad}', '', $html);
        } else {
            $publicidadhtml = substr($html, $publicidadstart, $publicidadend - $publicidadstart);
            $html = str_replace($publicidadhtml, '', $html);
        }

        if(@$this->factura->activaCupon) {
            $html = str_replace('{custom:coupon}', QueryHelper::get_coupon_mail(2), $html);
        } else {
            $html = str_replace('{custom:coupon}', '', $html);
        }

        if(file_exists(INVOICE_PATH.$idFactura.".pdf")) {
            $html = str_replace('{custom:pdf}', '<a href="'.$url.'file/invoice/'.$idFactura.'.pdf">Descargar Factura / Download Invoice</a>', $html);
        } else {
            $html = str_replace('{custom:pdf}', '', $html);
        }

        if($this->factura->multiplicadorMoneda == '1') {
            $moneda = '$';
            $decimal = 0;
            $factor = 1;
            $html = str_replace('{custom:factor}', '', $html);
        } else {
            $moneda = 'USD$';
            $decimal = 2;
            $factor = 1/$this->factura->multiplicadorMoneda;
            $html = str_replace('<td width="5%"><div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>Total</b><br>IVA Inc.</font></div></td>', '', $html);
            $html = str_replace('<td><div align="center"><font color="#333333" face="Verdana, Arial, Helvetica, sans-serif" size="1">{custom:total}</font></div></td>', '', $html);

            $html = str_replace('{custom:factor}', '- Precio Dolar: COP$'.number_format($this->factura->multiplicadorMoneda,2), $html);
        }

        $tables = array('factura'=>$this->factura, 'cliente'=>$this->cliente);

        foreach ($tables as $keytable => $table) {
            foreach ($table->as_array() as $key => $value) {
                $html = str_replace('{'.$keytable.':'.$key.'}', $value, $html);
            }
        }
        $html = str_replace('{custom:nombreDocumento}', $this->cliente->nombreDocumento(), $html);

            // $shippinghtml = substr($html, $shippingstart, $shippingend - $shippingstart);


        $shippingstart = strpos($html,'{begin:shipping}');
        $shippingend = strpos($html,'{end:shipping}')+14;
        if($this->factura->use_billing) {
            $shippinghtml = substr($html, $shippingstart, $shippingend - $shippingstart);
            $html = str_replace($shippinghtml, '', $html);
        } else {
            $html = str_replace('{begin:shipping}', '', $html);
            $html = str_replace('{end:shipping}', '', $html);
        }


        if($payment = $this->factura->extra_payment()) {
            switch ($this->factura->idTipoPago) {
                case '3':
                    $html = str_replace('{custom:transaccion}', '<br><b>Transacción:</b> '.$payment->transaccion_id, $html);
                    break;
                case '5':
                    $html = str_replace('{custom:transaccion}', '<br><b>Transacción:</b> '.$payment->txn_id, $html);
                    break;
            }
        } else {
            $html = str_replace('{custom:transaccion}', '', $html);
        }

        $prdstart = strpos($html,'{foreach:producto}')+18;
        $prdend = strpos($html,'{end foreach:producto}');

        $prodhtml = substr($html, $prdstart, $prdend - $prdstart);
        $prods = array();
        $total = 0;
        $cont = 0;
        foreach ($this->productos as $producto) {
            // var_dump($producto->as_array());
            $cont++;
            if(!$producto->idProducto) { continue; }
            // var_dump($producto->as_array());
            $prdhtml = $prodhtml;
            if(!$producto->activeProducto) {
                // $prdhtml = str_replace('<tr>', '<tr style="background-color:#f66;color:#666">', $prdhtml);
                $prdhtml = str_replace('Referencia', '<font color="#777"><b>DESCONTINUADO</b></font> Referencia', $prdhtml);
            }
            $prdhtml = str_replace('{custom:nombreProducto}', Html::limit_string($producto->nombreProducto,40), $prdhtml);
            foreach ($producto->as_array() as $key => $value) {
                // if($key == 'nombreProducto') {
                //     $prdhtml = str_replace('{producto:'.$key.'}', iconv("UTF-8", "WINDOWS-1252", $value), $prdhtml);
                // }
                $prdhtml = str_replace('{producto:'.$key.'}', $value, $prdhtml);

                if(strpos($prdhtml, '{money:'.$key.'}') !== false ) { $prdhtml = str_replace('{money:'.$key.'}', $moneda.number_format($value*$factor, $decimal), $prdhtml); }
                // $prdhtml = str_replace('{customnumber:valorProducto}', number_format($producto->valorUnitario), $prdhtml);
            }
            $prdhtml = str_replace('{custom:subtotal}', $moneda.number_format($producto->cantidad*$producto->valorUnitario*$factor, $decimal), $prdhtml);
            $prdhtml = str_replace('{custom:id}', $cont, $prdhtml);
            $this->addImage($producto->imagenProducto,'producto'.$producto->idProducto,IMG_PRD_PATH_CART,IMG_PRD_URL_CART);

            // if($nombreMarca = $producto->nombreMarca()) {
            //     if($nombreMarca != 'Etiqueta') {
            //         $marca = $nombreMarca.'<br>'.$producto->numeroTintas.' tintas';
            //     } else {
            //         $marca = $nombreMarca;
            //     }
            // } else {
            //     $marca = '';
            // }

            // $prdhtml = str_replace('{custom:marca}', utf8_decode($marca), $prdhtml);

            $prdhtml = str_replace('{custom:total}', $moneda.number_format($producto->valorTotal*$factor, $decimal), $prdhtml);
            $total += $producto->valorTotal;

            $prods[] = $prdhtml;
        }

        $html = str_replace('{total:subtotal}', $moneda.number_format($total*$factor,$decimal), $html);

        $fletestart = strpos($html,'{begin:flete}');
        $fleteend = strpos($html,'{end:flete}')+11;
        if(!$this->factura->flete) {
            $fletehtml = substr($html, $fletestart, $fleteend - $fletestart);
            $html = str_replace($fletehtml, '', $html);
        } else {
            $total += $this->factura->flete;
            $html = str_replace('{begin:flete}', '', $html);
            $html = str_replace('{end:flete}', '', $html);
            $html = str_replace('{total:flete}', $moneda.number_format($this->factura->flete*$factor,$decimal), $html);
        }

        $adicionalesstart = strpos($html,'{begin:adicionales}');
        $adicionalesend = strpos($html,'{end:adicionales}')+17;
        if(!$this->factura->adicionales) {
            $adicionaleshtml = substr($html, $adicionalesstart, $adicionalesend - $adicionalesstart);
            $html = str_replace($adicionaleshtml, '', $html);
        } else {
            $total += $this->factura->adicionales;
            $html = str_replace('{total:adicionales}', $moneda.number_format($this->factura->adicionales*$factor,$decimal), $html);
            $html = str_replace('{begin:adicionales}', '', $html);
            $html = str_replace('{end:adicionales}', '', $html);
        }        

        $html = str_replace('{total:total}', $moneda.number_format($total*$factor,$decimal), $html);

        $html = str_replace('{foreach:producto}'.$prodhtml.'{end foreach:producto}', implode('', $prods), $html);
        

        $this->message = $html;
        // echo ($this->message);


        $this->to[] = $this->cliente->emailCliente;
        // $this->to[] = array($this->cliente->emailCliente, $this->cliente->nombresCliente);
        $this->cc[] = QueryHelper::getConf('ORDER','EMAIL');
        $this->from = QueryHelper::getConf('ORDER','EMAIL');

        $this->replyTo = @$this->user->emailUsuario;
        if(@$asesor) {
            $this->cc[] = $asesor->emailUsuario;
            $this->replyTo = $asesor->emailUsuario;
        }
        
        $cc = explode(',',$this->factura->emailsAdicionales);
        foreach ($cc as $email) {
            $this->cc[] = $email;
        }

        parent::__construct();
    }

    public function checkdata() {
        if ($this->factura) { return true; }
        return false;
    }

    public function subject($idFactura, $altSubject = '') {
        if($altSubject) {
            $this->subject = str_replace('{idFactura}', $idFactura, $altSubject);
        } else {
            $this->subject = "Su pedido #".$idFactura." en Novedades Nguiller's";
        }
    }
}