<?php

/**
* DHL
*/
class DHL
{
    public $url = 'http://dct.dhl.com/data/quotation';

    public $params = array('declVal' => '15000',
            'declValCur' => 'COP',
            'dimUom' => 'cm',
            'dstCity' => 'MIAMI',
            'dstCtry' => 'US',
            'dstSub' => '',
            'dstZip' => '33142',
            'dtbl' => 'Y', // Dutiable Y/N
            'h0' => '10',
            'l0' => '25',
            'noPce' => '1',
            'orgCity' => 'BOGOTA',
            'orgCtry' => 'CO',
            'orgSub' => 'AMERICAS OCCIDENTAL',
            'orgZip' =>' ',
            'shpDate' => '2014-05-07',
            'w0' => '25',
            'wgt0' => '5',
            'wgtUom' => 'kg'
            );


    function __construct()
    {
        # code...
    }

    public static function getQuote($params = array()) {

        $dhl = new self();

        foreach ($params as $key => $value) {
            $dhl->params[$key] = $value;
        }
        if(!isset($params['shpDate'])) {
            $date = new DateTime();
            $dhl->params['shpDate'] = $date->modify('+1 day')->format('Y-m-d');
        }
        
        // var_dump($dhl->params);

        $result = Html::get_http_anyway($dhl->url, $dhl->params);
        try {
            $xml = new SimpleXMLElement($result);
            $prices = array();
            foreach ($xml->quotationList->quotation as $quotation) {
                $price = (float) str_replace('COP', '', str_replace(',', '', $quotation->estTotPrice));
                if($price)
                    $prices[] = $price;
                // var_dump($quotation->estTotPrice);
                // var_dump($quotation->optServiceList);
                // var_dump($quotation->quotationServiceList);
            }
            // var_dump($xml->quotationList->quotation);
            return (min($prices));
        } catch (Exception $e) {
            return QueryHelper::getConf('FLETE_INTERNACIONAL','PEDIDO');
        }
    }
}





///// DHL



// var_dump($xml->quotationList);


// function SimpleXML2Array($xml){
//     $array = (array)$xml;

//     //recursive Parser
//     foreach ($array as $key => $value){
//         if(strpos(get_class($value),"SimpleXML")!==false){
//             $array[$key] = SimpleXML2Array($value);
//         }
//     }
//     return $array;
// }



/*



$params = array(
    'cmdcResponse' => '{"output":{"serviceOptions":[{"key":"INTERNATIONAL_PRIORITY","displayText":"FedEx International Priority<SUP>&reg;</SUP>"},{"key":"INTERNATIONAL_ECONOMY","displayText":"FedEx International Economy<SUP>&reg;</SUP>"},{"key":"INTERNATIONAL_PRIORITY_FREIGHT","displayText":"FedEx International Priority<SUP>&reg;</SUP> Freight"}],"packageOptions":[{"packageType":{"key":"FEDEX_ENVELOPE","displayText":"FedEx Envelope"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Carta","dimensionText":"9-1/2” x 12-1/2”"},{"description":"Legal","dimensionText":"9-1/2” x 15-1/2”"}],"maxMetricWeightAllowed":{"units":"KG","value":0.5},"maxWeightAllowed":{"units":"LB","value":1}},{"packageType":{"key":"FEDEX_PAK","displayText":"FedEx Pak"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Pequeño","dimensionText":"10-1/4” x 12-3/4”"},{"description":"Grande","dimensionText":"12” x 15-1/2”"}],"maxMetricWeightAllowed":{"units":"KG","value":9},"maxWeightAllowed":{"units":"LB","value":20}},{"packageType":{"key":"FEDEX_BOX","displayText":"FedEx Box"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Pequeño","dimensionText":"10-7/8” x 1-1/2” x 12-3/8”"},{"description":"Medio","dimensionText":"11-1/2” x 2-3/8” x 13-1/4”"},{"description":"Grande","dimensionText":"12-3/8” x 3” x 17-1/2”"}],"maxMetricWeightAllowed":{"units":"KG","value":18},"maxWeightAllowed":{"units":"LB","value":40}},{"packageType":{"key":"FEDEX_TUBE","displayText":"FedEx Tube"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Tubo","dimensionText":"6” x 6” x 38”"}],"maxMetricWeightAllowed":{"units":"KG","value":9},"maxWeightAllowed":{"units":"LB","value":20}},{"packageType":{"key":"FEDEX_10KG_BOX","displayText":"FedEx 10kg Box"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Caja de FedEx 10kg","dimensionText":"15-13/16” x 12-15/16” x 10-3/16”"}],"maxMetricWeightAllowed":{"units":"KG","value":50},"maxWeightAllowed":{"units":"LB","value":110}},{"packageType":{"key":"FEDEX_25KG_BOX","displayText":"FedEx 25kg Box"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Caja de FedEx 25kg","dimensionText":"21-9/16” x 16-9/16” x 13-3/16”"}],"maxMetricWeightAllowed":{"units":"KG","value":50},"maxWeightAllowed":{"units":"LB","value":110}},{"packageType":{"key":"YOUR_PACKAGING","displayText":"Su embalaje"},"rateTypes":["WEIGHT_BASED"],"subpackageInfoList":[{"description":"Ingrese el peso y las dimensiones de su paquete para obtener una tarifa estimada más precisa.","dimensionText":""}],"maxMetricWeightAllowed":{"units":"KG","value":998},"maxWeightAllowed":{"units":"LB","value":2200}}],"oneRate":false,"pricingOptions":[{"key":"WEIGHT_BASED","displayText":"FedEx Standard Rate"},{"key":"FLAT_BASED","displayText":"FedEx One Rate"}]},"successful":true}',
    'currentPage' => 'rfsshipfromto',
    'destCity' => 'Lima',
    'destCountry' => 'PE',
    'destSelected' => 'N',
    'doEdt' => 'false',
    'isPackageIdentical' =>  'NO',
    'origCity' => 'Bogota',
    'origCountry' => 'CO',
    'originSelected' => 'N',
    'perPackageWeight' => '5',
    'pricingOption' => 'FEDEX_STANDARD_RATE',
    'pricingOptionDisplayed' => 'false',
    'receivedAtCode' => '1',
    'shipCalendarDate' => '05/06/2014',
    'shipDate' => '05/06/2014',
    'totalNumberOfPackages' => '1',
    'transitTime' => 'false',
    'weightUnit' => 'kgs',
    'locId' => '',
    'outLookResult' => '',
    'outlookAddressType' => ''
);

$params = array(
    'action' => 'rate',
    'data' => '{"RateAndServicesRequest":{"processingParameters":{"returnDetailedErrors":true,"anonymousTransaction":true,"returnLocalizedDateTime":true,"clientId":"HPRM"},"rateRequestControlParameters":{"rateSortOrder":"SERVICENAMETRADITIONAL","serviceTypeList":[{"serviceType":"EUROPE_FIRST_INTERNATIONAL_PRIORITY"},{"serviceType":"INTERNATIONAL_FIRST"},{"serviceType":"INTERNATIONAL_PRIORITY"},{"serviceType":"INTERNATIONAL_ECONOMY"},{"serviceType":"FIRST_OVERNIGHT"},{"serviceType":"PRIORITY_OVERNIGHT"},{"serviceType":"STANDARD_OVERNIGHT"},{"serviceType":"FEDEX_2_DAY"},{"serviceType":"FEDEX_2_DAY_AM"},{"serviceType":"FEDEX_EXPRESS_SAVER"},{"serviceType":"FEDEX_GROUND"},{"serviceType":"INTERNATIONAL_GROUND"},{"serviceType":"SAME_DAY"},{"serviceType":"SAME_DAY_CITY"}],"returnTransitTimes":true},"requestedShipment":{"shipper":{"address":{"city":"Bogotá","countryCode":"CO"}},"recipientList":[{"recipient":{"address":{"city":"Lima","postalCode":"15082","countryCode":"PE"}}}],"shipTimestamp":"May-05-2014","dropoffType":"DROP_BOX","shippingChargesPayment":{"payor":{"responsibleParty":{"address":{"countryCode":"CO"},"accountNumber":null}}},"customsClearanceDetail":{"commodityList":[{"commodity":{"name":"DOCUMENTS","quantity":1,"customsValue":{"amount":1,"currency":"COP"}}}]},"rateRequestTypeList":[{"rateRequestType":"LIST"}],"requestedPackageLineItemList":[{"requestedPackageLineItem":{"groupPackageCount":"1","physicalPackaging":"FEDEX_BOX","insuredValue":{"amount":"1.0","currency":"COP"},"weight":{"units":"KG","value":"5"}}}]},"carrierCodeList":[{"carrierCode":"FDXE"},{"carrierCode":"FDXG"}]}}',
    'format' => 'json',
    'locale' => 'es_CO',
    'version' => '1'
);

// $result = getFedexQuotation($params);
// echo '<pre>';
// print_r(json_decode($result,true));





function getFedexQuotation($params) {
        // create curl resource
        $url = 'https://www.fedex.com/ratefinder/standalone?method=getQuickQuote';
        $url = 'https://www.fedex.com/rateCal/rate';
        $ch = curl_init();
        if ($ch == FALSE) {
            return FALSE;
        }
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // Set maximum redirects
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        // Allow a max of 5 seconds.
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);


        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

        curl_setopt($ch, CURLOPT_URL, $url);

        // $output contains the output string
        $output = curl_exec($ch);

        // Check for errors and such.
        $info = curl_getinfo($ch);
        $errno = curl_errno($ch);
        if( $output === false || $errno != 0 ) {
            // Do error checking
        } else if($info['http_code'] != 200) {
            // Got a non-200 error code.
            // Do more error checking
        }

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }


*/



?>