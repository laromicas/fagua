<?php

class Paypal {
    function __construct() {
        require_once(FRNT_ROOT_PATH.'config/paypal.php');
    }

    public function make_payment($idFactura) {
        $factura = Model::factory('Facturas')->where('idFactura',$idFactura)->find_one();
        // var_dump($factura->as_array());die();
        if(!$factura) {
            return false;
        }
        $cliente = $factura->cliente();
        $cupon = $factura->cupon();
        $detalleFactura = $factura->detalle();

        if($cupon) {
            $multCupon = 1 - ($cupon->porcentajeCupon/100);
            $idCupon = $cupon->idCupon;
        } else {
            $multCupon = 1;
        }

        $datos = array(
                //datos paypal
                'cmd' => '_cart',
                'no_note' => 1,
                'lc' => 'ES',   //Para revisar, validos UK y US
                'currency_code' => 'USD',
                'bn' => 'PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest',
                'business' => PAYPAL_EMAIL,
                'return' => PAYPAL_RETURN_URL,
                'cancel_return' => PAYPAL_CANCEL_URL,
                'notify_url' => PAYPAL_NOTIFY_URL,
                'upload' => 1,
                'custom' => $idFactura,

                //Datos del Cliente
                'first_name' => $cliente->nombresCliente,
                'last_name' => $cliente->apellidosCliente,
                'payer_email' => $cliente->emailCliente
            );
        $cont = 1;
        if($factura->multiplicadorMoneda == '1' || $factura->multiplicadorMoneda == 0) {
            $cur = new Currency();
            $dollar = $cur->getMultiplier('USD','COP');
            $factor = $dollar;
        } else {
            $factor = 1/$factura->multiplicadorMoneda;
        }
        // echo $factor;
        // echo '-';
        // echo 1/$factor;
        // echo '-';
        // echo $factura->multiplicadorMoneda;
        // die();
        // echo $factor;die();
        if($factura->flete+$factura->adicionales) {
            $datos['handling_cart'] = ceil(($factura->flete+$factura->adicionales)*$factor*100)/100;
            // $datos['tax_cart'] = ceil($factura->flete*$factor*100)/100;
        }
        // foreach ($detalleFactura as $producto) {
        //     $datos['item_name_'.$cont] = utf8_decode($producto->nombreProducto);
        //     $datos['amount_'.$cont] = ceil($producto->valorUnitario*$factor*100)/100;
        //     $datos['quantity_'.$cont] = utf8_decode($producto->cantidad);
        //     $cont++;
        // }
        $total = 0;
        foreach ($detalleFactura as $producto) {
            $total += $producto->valorUnitario * $producto->cantidad;
            // $total += $producto->valorUnitario * $producto->cantidad * $multCupon;
        }
        // echo $producto->valorUnitario*$producto->cantidad;
        // die();
        $datos['item_name_1'] = "Payment for Invoice #".$idFactura;
        $datos['amount_1'] = round($total*$factor,2);
        // $datos['amount_1'] = $total;
        $datos['quantity_1'] = 1;
        // var_dump($datos);die();
        return $datos;
    }

    // functions.php
    public static function check_txnid($tnxid){
        global $link;
        return true;
        $valid_txnid = true;
        //get result set
        $Pago = Model::factory('Payments')->where('tnxid',$tnxid);
        // $sql = mysql_query("SELECT * FROM `payments` WHERE txnid = '$tnxid'", $link);
        if($row = mysql_fetch_array($sql)) {
            $valid_txnid = false;
        }
        return $valid_txnid;
    }

    public static function check_price($price, $id){
        $valid_price = false;
        /*
        you could use the below to check whether the correct price has been paid for the product
        if so uncomment the below code

        $sql = mysql_query("SELECT amount FROM `products` WHERE id = '$id'");
        if (mysql_numrows($sql) != 0) {
            while ($row = mysql_fetch_array($sql)) {
                $num = (float)$row['amount'];
                if($num == $price){
                    $valid_price = true;
                }
            }
        }
        return $valid_price;
        */
        return true;
    }

    public static function updatePayments($data){
        global $link;
        if(is_array($data)){
            $sql = mysql_query("INSERT INTO `payments` (txnid, payment_amount, payment_status, itemid, createdtime) VALUES (
                    '".$data['txn_id']."' ,
                    '".$data['payment_amount']."' ,
                    '".$data['payment_status']."' ,
                    '".$data['item_number']."' ,
                    '".date("Y-m-d H:i:s")."'
                    )", $link);
        return mysql_insert_id($link);
        }
    }

    public static function validate($req) {
        $ch = curl_init('https://'.PAYPAL_URL.'/cgi-bin/webscr');
        if ($ch == FALSE) {
        return FALSE;
        }

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        // curl.cainfo=<path-to>cacert.pem
        // if(true) {
        //     curl_setopt($ch, CURLOPT_HEADER, 1);
        //     curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        // }
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        $res = curl_exec($ch);
        // echo '<br>';
        $result = array();
        if (curl_errno($ch) != 0) // cURL error
        {
            $result =  array('error'=>1, 'message'=>'1. '.date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch));
            curl_close($ch);
            return $result;

        } else {
            curl_close($ch);
        }
        $test = ORM::for_table('paypaltest')->create();
        $test->paypalget = $req.'----'.$res;
        $test->save();


        if (strcmp ($res, "VERIFIED") == 0) {
            $result = array('error'=>0, 'message'=>'VERIFIED');
        } else if (strcmp ($res, "INVALID") == 0) {
            $result = array('error'=>2, 'message'=>'2. '.date('[Y-m-d H:i e] '). "Invalid IPN");
        } else {
            $result = array('error'=>3, 'message'=>'3. '.date('[Y-m-d H:i e] '). "Unespecified Error");
        }
        return $result;
    }


    public function valid_payment($idFactura) {
        $paypals = Model::factory('PaypalPayments')->where('idFactura',$idFactura)->find_many();
        if(!count($paypals)) {
            return false;
        }
        $error = 1;
        foreach ($paypals as $paypal) {
            $error = $paypal->error;
            if($paypal->error == 'VERIFIED') {
                break;
            }
        }
        return $error;
    }
}