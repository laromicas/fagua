<?php

/**
 * 
 */
class System {
    public static function execInBackground($cmd) {
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ". $cmd, "r")); 
        }
        else {
            exec($cmd . " > /dev/null &");  
        }
    } 

    public static function execPhpInBackground($cmd) {
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ".BINDIR." ". $cmd, "r")); 
            // pclose(popen("start ".BINDIR." ". $cmd, "r")); 
        }
        else {
            exec(BINDIR." ".$cmd . " > /dev/null &");
        }
    } 
}