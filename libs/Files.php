<?php

class Files {
    public static function getDirFiles($dir,$filters = array(), $get_contents = false) {
        if(!is_array($filters)) {
            $filters = array($filters);
        }
        $files = array();
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != "." && $file != "..") {
                        if(!count($filters) || in_array(pathinfo($dir . $file,PATHINFO_EXTENSION),$filters)) {
                            if($get_contents) {
                                $files[] = array("name" => $file , "type" => filetype($dir . $file), "ext" => pathinfo($dir . $file,PATHINFO_EXTENSION), "size"  => filesize($dir . $file), "content"  => file_get_contents($dir . $file));
                            } else {
                                $files[] = array("name" => $file , "type" => filetype($dir . $file), "ext" => pathinfo($dir . $file,PATHINFO_EXTENSION), "size"  => filesize($dir . $file));
                            }
                        }
                    }
                }
                closedir($dh);
            }
        }
        return $files;
    }

    public static function deleteDir($path, $rmdir = true) {
        // foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
        //     $path->isFile() ? unlink($path->getPathname()) : rmdir($path->getPathname());
        // }
        // if ($rmdir) { return rmdir($dirPath); }
        // return true;

        if (!is_dir($path)) {
            throw new InvalidArgumentException("$path is not a directory");
        }
        if (substr($path, strlen($path) - 1, 1) != '/') {
            $path .= '/';
        }
        $dotfiles = glob($path . '.*', GLOB_MARK);
        $files = glob($path . '*', GLOB_MARK);
        $files = array_merge($files, $dotfiles);
        foreach ($files as $file) {
            if (basename($file) == '.' || basename($file) == '..') {
                continue;
            } else if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        if ($rmdir) { return rmdir($path); }
        return true;
    }
}