<?php

class BodyClass {
    public static function getBodyClass($controller) {
        require 'config/bodyclass_config.php';
        if(isset($bodyClass[$controller])) {
        	return $bodyClass[$controller];
        } else {
        	return $bodyClass["default"];
        }
    }
}