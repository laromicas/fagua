<?php

class Translate {
    public $dict;
    function __construct($lang = '') {
        Session::init();
        if($lang) {
            $this->load($lang);
            return;
        }
        if(!Session::get('lang')) {
            $lang = (Cookie::get('lang')) ? Cookie::get('lang') : 'esp';
            $this->load($lang);
                        /*
                Session::set('lang',$this->dict);
                Cookie::set('lang',$lang);
                */
        } else {
            if(!is_array(Session::get('lang'))) {
                $lang = (Cookie::get('lang')) ? Cookie::get('lang') : 'esp';
                $this->load($lang);
            }
        }
        //var_dump($_COOKIE);
        // var_dump(Session::get('lang'));die();
    }

    public static function tr($path, $opts = array()) {
        //$lang = Session::get('lang');
        $response = '';
        switch (count($path)) {
            case 4:
                $response = @$_SESSION['lang'][$path[0]][$path[1]][$path[2]][$path[3]];
                break;
            case 3:
                $response = @$_SESSION['lang'][$path[0]][$path[1]][$path[2]];
                break;
            case 2:
                $response = @$_SESSION['lang'][$path[0]][$path[1]];
                break;
            case 1:
                $response = @$_SESSION['lang'][$path[0]];
                break;
        }
        if(is_string($response)) {
            if($response) {
                for ($i=0; $i < sizeof($opts); $i++) { 
                    $response = str_replace('{'.$opts[$i].'}', $opts[$i+1], $response);
                    $i++;
                }
            }
        } elseif (is_array($response)) {
            $response = $response;
        } else {
            $response = '**'.end($path).'**';
        }
        return $response;
    }

    private function load($lang) {
        $json = file_get_contents("public/lng/esp.txt");
        //$json = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t]//.*)|(^//.*)#", '', $json);
        $this->dict = json_decode($json,true);
        if(!file_exists("public/lng/".$lang.".txt")) {
            $lang = 'esp';
        } else {
            $json = file_get_contents("public/lng/".$lang.".txt");
            $newlang = json_decode($json,true);
            if(!$newlang) die('Error Loading');
            foreach ($newlang as $key => $value) {
                if(is_array($value)) {
                    foreach ($value as $id => $val) {
                        $this->dict[$key][$id] = $val;
                    }
                } else {
                    $this->dict[$key] = $value;
                }
            }
        }
        Session::set('lang',$this->dict);
        Cookie::set('lang',$lang);
        $idioma = Model::factory('Idiomas')->where('shortName',$lang)->find_one();
        if($idioma) {
            Session::set('id_lang',$idioma->idIdioma);
        }
    }
}