<?php

class PagosOnline {
    function __construct() {
        require_once(FRNT_ROOT_PATH.'config/pagosonline.php');
    }

    public static function make_payment($idFactura, $idioma = '') {
// die();
        $factura = Model::factory('Facturas')->where('idFactura',$idFactura)->find_one();
        // var_dump($factura->as_array());die();
        if(!$factura) {
            return false;
        }
        $cliente = $factura->cliente();
        $cupon = $factura->cupon();
        $detalleFactura = $factura->detalle();

        if($cupon) {
            $multCupon = 1 - ($cupon->porcentajeCupon/100);
            $idCupon = $cupon->idCupon;
        } else {
            $multCupon = 1;
        }

        if(!$idioma) {
            $idioma = (Cookie::get('lang')) ? substr(Cookie::get('lang'), 0, 2) : 'es';
            if(!in_array($idioma, array('fr','en','es','it'))) {
                $idioma = 'en';
            }
        }

        $datos = array(
                'usuarioId' => PAGOSONLINE_USUARIO_ID,
                'refVenta' => "".time(),
                'descripcion' => "Pago Factura #".$factura->idFactura,
                'valor' => 0,
                'iva' => 0,
                'baseDevolucionIva' => 0,
                // 'moneda' => 'COP',
                'llaveEncripcion' => PAGOSONLINE_KEY,
                'extra1' => $idFactura,
                'url_respuesta' => PAGOSONLINE_RETURN_URL,
                'url_confirmacion' => PAGOSONLINE_NOTIFY_URL,
                'lng' => $idioma,
                // Datos Ciente
                'nombreComprador' => $cliente->nombresCliente.' '.$cliente->apellidosCliente,
                // 'documentoIdentificacion' => $cliente->emailCliente,
                'telefonoMovil' => $cliente->telefonoCliente,
                'direccionCobro' => $cliente->direccionCliente,
                'ciudadCobro' => $cliente->ciudadCliente,
                'paisCobro' => $cliente->paisCliente,
                'telefono' => $cliente->telefonoCliente,
                'telefonoOficina' => $cliente->celularCliente
            );
        if(!PAGOSONLINE_PRODUCCION) {
            $datos['prueba'] = 1;
        } else {
            $datos['prueba'] = 0;
        }
        $cont = 1;
        if($factura->multiplicadorMoneda == '1' || $factura->multiplicadorMoneda == 0) {
            // $cur = new Currency();
            // $dollar = $cur->getMultiplier('USD','COP');
            $datos['moneda'] = 'COP';
            $factor = 1;
        } else {
            $datos['moneda'] = 'USD';
            $factor = 1/$factura->multiplicadorMoneda;
        }
        // echo $factor;die();
        if($factura->flete+$factura->adicionales) {
            // $datos['handling_cart'] = ceil(($factura->flete+$factura->adicionales)*$factor*100)/100;
            // $datos['tax_cart'] = ceil($factura->flete*$factor*100)/100;
        }
        $datos['baseDevolucionIva'] = 0;
        $datos['iva'] = 0;
        foreach ($detalleFactura as $producto) {
            // $datos['valor'] = utf8_decode($producto->nombreProducto);

            $datos['valor'] += $producto->valorUnitario * $producto->cantidad;
            // $datos['valor'] += $producto->valorUnitario * $producto->cantidad * $multCupon;
            if($cliente->paisCliente == 'CO') {
                $datos['baseDevolucionIva'] += round($producto->valorUnitario*$producto->cantidad/1.16,2);
                $datos['iva'] += $producto->valorUnitario*$producto->cantidad-round($producto->valorUnitario*$producto->cantidad/1.16,2);
            }

            // if($factura->multiplicadorMoneda == '1' || $factura->multiplicadorMoneda == 0) {
            //     $datos['valor'] += $producto->valorUnitario*$producto->cantidad;
            //     $datos['baseDevolucionIva'] += round($producto->valorUnitario*$producto->cantidad/1.16,2);
            //     $datos['iva'] += $producto->valorUnitario*$producto->cantidad-round($producto->valorUnitario*$producto->cantidad/1.16,2);
            // } else {
            //     $datos['valor'] += $producto->valorUnitario*$factor*$producto->cantidad;
            // }
        }

        $datos['valor'] += $factura->adicionales;
        $datos['valor'] += $factura->flete;
        $datos['valor'] = round($datos['valor']*$factor,2);
        $datos['baseDevolucionIva'] = round($datos['baseDevolucionIva']*$factor,2);
        $datos['iva'] = round($datos['iva']*$factor,2);

        // $datos['baseDevolucionIva'] += round($factura->adicionales/1.16,2);
        // $datos['iva'] += $factura->adicionales-round($factura->adicionales/1.16,2);
        // if($factura->multiplicadorMoneda == '1' || $factura->multiplicadorMoneda == 0) {
        //     $datos['valor'] += $factura->flete;
        //     // $datos['baseDevolucionIva'] += round($factura->flete/1.16,2);
        //     // $datos['iva'] += $factura->flete-round($factura->flete/1.16,2);
        // } else {
        //     $datos['valor'] = round($datos['valor'],2);
        //     $datos['valor'] += round($factura->flete*$factor,2);
        //     $datos['baseDevolucionIva'] = 0;
        //     $datos['iva'] = 0;
        // }

        $concat = $datos['llaveEncripcion'].'~'.$datos['usuarioId'].'~'.$datos['refVenta'].'~'.$datos['valor'].'~'.$datos['moneda'];
        // echo $concat;
        $datos['firma'] = md5($concat);
        // echo $datos['firma'];die();
        // var_dump($datos);die();
        foreach ($datos as $key => $value) {
            $datos[$key] = utf8_decode($value);
        }
        return $datos;
    }

    // functions.php
    public static function check_txnid($tnxid){
        global $link;
        return true;
        $valid_txnid = true;
        //get result set
        $Pago = Model::factory('Payments')->where('tnxid',$tnxid);
        // $sql = mysql_query("SELECT * FROM `payments` WHERE txnid = '$tnxid'", $link);
        if($row = mysql_fetch_array($sql)) {
            $valid_txnid = false;
        }
        return $valid_txnid;
    }

    public static function check_price($price, $id){
        $valid_price = false;
        /*
        you could use the below to check whether the correct price has been paid for the product
        if so uncomment the below code

        $sql = mysql_query("SELECT amount FROM `products` WHERE id = '$id'");
        if (mysql_numrows($sql) != 0) {
            while ($row = mysql_fetch_array($sql)) {
                $num = (float)$row['amount'];
                if($num == $price){
                    $valid_price = true;
                }
            }
        }
        return $valid_price;
        */
        return true;
    }

    public static function updatePayments($data){
        global $link;
        if(is_array($data)){
            $sql = mysql_query("INSERT INTO `payments` (txnid, payment_amount, payment_status, itemid, createdtime) VALUES (
                    '".$data['txn_id']."' ,
                    '".$data['payment_amount']."' ,
                    '".$data['payment_status']."' ,
                    '".$data['item_number']."' ,
                    '".date("Y-m-d H:i:s")."'
                    )", $link);
        return mysql_insert_id($link);
        }
    }

    public static function validate($req) {
        $ch = curl_init('https://'.PAGOSONLINE_URL);
        if ($ch == FALSE) {
        return FALSE;
        }

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        // curl.cainfo=<path-to>cacert.pem
        // if(true) {
        //     curl_setopt($ch, CURLOPT_HEADER, 1);
        //     curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        // }
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        $res = curl_exec($ch);
        // echo '<br>';
        $result = array();
        if (curl_errno($ch) != 0) // cURL error
        {
            $result =  array('error'=>1, 'message'=>'1. '.date('[Y-m-d H:i e] '). "Can't connect to PagosOnline to validate IPN message: " . curl_error($ch));
            curl_close($ch);
            return $result;

        } else {
            curl_close($ch);
        }
        if (strcmp ($res, "VERIFIED") == 0) {
            $result =  array('error'=>0, 'message'=>'VERIFIED');
        } else if (strcmp ($res, "INVALID") == 0) {
            $result =  array('error'=>2, 'message'=>'2. '.date('[Y-m-d H:i e] '). "Invalid IPN");
        }
        return $result;
    }

    public function valid_payment($idFactura) {
        $pagos_onlines = Model::factory('PagosOnlinePayments')->where('idFactura',$idFactura)->find_many();
        if(!count($pagos_onlines)) {
            return false;
        }
        $error = 1;
        foreach ($pagos_onlines as $pagos_online) {
            $error = $pagos_online->payment_status;
            if(in_array($pagos_online->payment_status, array(4))) {
            // if($pagos_online->error == 'Transaccion aprobada.') {
                return 'VERIFIED';
            }
            if(in_array($pagos_online->payment_status, array(3,7,10,11,12,14,15,17,18))) {
            // if($pagos_online->error == 'Transaccion aprobada.') {
                return 'PENDING';
            }
        }
        return false;
    }
}