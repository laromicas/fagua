<?php
$bodyClass = array(
      "index" => "cms-index-index cms-home",
      "login" => "customer-account-login",
      "login-register" => "customer-account-create",
      "catalogo" => "catalog-category-view categorypath-accessories-html category-accessories",
      "contacto" => "contacts-index-index",
      "cart" => "checkout-cart-index",
      "checkout" => "checkout-onepage-index",
      "default" => "cms-index-index cms-home",
      "wiki" => "cms-page-view cms-about-magento-demo-store"
      );