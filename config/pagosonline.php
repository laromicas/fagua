<?php
/**
 * THIS IS THE CONFIGURATION FILE
 * 
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 */

$url = 'http://'.$_SERVER['SERVER_NAME'].URL.'pagosonline/';

// define('PAGOSONLINE_EMAIL','nguillers@nguillers.com.co');
define('PAGOSONLINE_RETURN_URL',$url.'pago_ok');
define('PAGOSONLINE_CANCEL_URL',$url.'cancelado');
define('PAGOSONLINE_NOTIFY_URL',$url.'verify');


/**
 * Sandbox gateway2.pagosonline.net/apps/gateway/index.html
 * Produccion gateway.pagosonline.net/apps/gateway/index.html
 * 
*/
define('PAGOSONLINE_PRODUCCION',QueryHelper::getConf('PAGOSONLINE_PRODUCCION','CONSTANT'));
if(PAGOSONLINE_PRODUCCION) {
	define('PAGOSONLINE_URL','gateway.pagosonline.net/apps/gateway/index.html');
	define('PAGOSONLINE_USUARIO_ID','55400');
	define('PAGOSONLINE_KEY','126c7d0c2b8');
	// define('PAGOSONLINE_USUARIO_ID',2);
	// define('PAGOSONLINE_KEY','1111111111111111');
	define('PAGOSONLINE_EMAIL','gpltda@nguillers.com.co');
} else {
	define('PAGOSONLINE_URL','gateway2.pagosonline.net/apps/gateway/index.html');
	define('PAGOSONLINE_USUARIO_ID',2);
	define('PAGOSONLINE_KEY','1111111111111111');
	define('PAGOSONLINE_EMAIL','gpltda@nguillers.com.co');
}
// define('PAGOSONLINE_VERIFY','ipnpb.sandbox.paypal.com');