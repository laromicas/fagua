<?php
/**
 * THIS IS THE CONFIGURATION FILE
 * 
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 */

$url = 'http://'.$_SERVER['SERVER_NAME'].URL.'paypal/';

// define('PAYPAL_EMAIL','nguillers@nguillers.com.co');
define('PAYPAL_RETURN_URL',$url.'pago_ok');
define('PAYPAL_CANCEL_URL',$url.'cancelado');
define('PAYPAL_NOTIFY_URL',$url.'verify');

/**
 * Sandbox www.sandbox.paypal.com
 * Produccion www.paypal.com
 * 
*/
define('PAYPAL_PRODUCCION',QueryHelper::getConf('PAYPAL_PRODUCCION','CONSTANT'));
// define('PAYPAL_PRODUCCION',true);
if(PAYPAL_PRODUCCION) {
	define('PAYPAL_EMAIL','gpltda@nguillers.com.co');
	define('PAYPAL_URL','www.paypal.com');
	define('PAYPAL_VERIFY','ipnpb.paypal.com');
} else {
	define('PAYPAL_EMAIL','laromicas-facilitator@hotmail.com');
	define('PAYPAL_URL','www.sandbox.paypal.com');
	define('PAYPAL_VERIFY','ipnpb.sandbox.paypal.com');
}