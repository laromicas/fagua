<?php
/**
 * THIS IS THE CONFIGURATION FILE
 * 
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 */
 //******************* PARAMETROS ****************************
//$emailprueba[0]= "gpltda@nguillers.com.co";

define('EXT_URL', FRNT_URL);
define('EXT_PATH', FRNT_ROOT_PATH);

//Configuraciones de rutas
define('IMG_PUB_URL', EXT_URL.'file/publicidades/');
define('IMG_PRD_URL', EXT_URL.'file/productos/');

define('IMG_PRD_URL_CART',EXT_URL.'file/productos/cart/');
define('IMG_PRD_URL_SMALL',EXT_URL.'file/productos/small/');
define('IMG_PRD_URL_BIG',EXT_URL.'file/productos/big/');
define('IMG_LOG_URL',EXT_URL.'file/logos/');
define('IMG_BOL_URL',EXT_URL.'file/boletines/');
define('IMG_BAN_URL',EXT_URL.'file/banners/');
define('IMG_SEC_URL',EXT_URL.'file/secciones/');
define('IMG_SEL_URL',EXT_URL.'file/selecciones/');
define('IMG_WIK_URL',EXT_URL.'file/wikis/');
define('IMG_WIR_URL',EXT_URL.'file/medios_wiki/');

define('TMP_IMG_URL',EXT_URL.'file/tmp_img/');

define('INVOICE_URL',EXT_URL.'file/invoice/');

define('MAIL_URL',EXT_URL.'libs/Mail/html/');
define('MAIL_PATH',EXT_PATH.'libs/Mail/html/');


define('EXCEL_FILES_PATH',EXT_PATH.'file/ftp/');
define('INVOICE_PATH',EXT_PATH.'file/invoice/');
define('IMG_PUB_PATH',EXT_PATH.'file/publicidades/');
define('IMG_PRD_PATH',EXT_PATH.'file/productos/');
define('IMG_PRD_PATH_CART',EXT_PATH.'file/productos/cart/');
define('IMG_PRD_PATH_SMALL',EXT_PATH.'file/productos/small/');
define('IMG_PRD_PATH_BIG',EXT_PATH.'file/productos/big/');
define('IMG_LOG_PATH',EXT_PATH.'file/logos/');
define('IMG_BOL_PATH',EXT_PATH.'file/boletines/');
define('IMG_BAN_PATH',EXT_PATH.'file/banners/');
define('IMG_SEC_PATH',EXT_PATH.'file/secciones/');
define('IMG_SEL_PATH',EXT_PATH.'file/selecciones/');
define('IMG_WIK_PATH',EXT_PATH.'file/wikis/');
define('IMG_WIR_PATH',EXT_PATH.'file/medios_wiki/');

define('TMP_PATH',EXT_PATH.'file/temporal/');
define('TMP_IMG_PATH',EXT_PATH.'file/tmp_img/');

define('TMP_URL',EXT_URL.'file/temporal/');

//Tamaños de imagenes de productos

define('IMG_PRODUCTO_BIG_WIDTH', 300);
define('IMG_PRODUCTO_BIG_HEIGHT', 300);

define('IMG_PRODUCTO_SMALL_WIDTH', 100);
define('IMG_PRODUCTO_SMALL_HEIGHT', 100);

define('IMG_PRODUCTO_CART_WIDTH', 50);
define('IMG_PRODUCTO_CART_HEIGHT', 50);

//Tamaños de imagenes de pubicidades
define('IMG_PORTADA_WIDTH', 550);
define('IMG_PORTADA_HEIGHT', 386);

define('IMG_LATERAL_WIDTH', 190);

define('IMG_SUPERIOR_WIDTH', 140);
define('IMG_SUPERIOR_HEIGHT', 45);

define('IMG_EMAIL_WIDTH', 600);
define('IMG_EMAIL_HEIGHT', 150);

//Imagenes Secciones
define('IMG_LOGO_WIDTH', 563);
define('IMG_LOGO_HEIGHT', 73);

define('IMG_MAIL_LOGO_WIDTH', 380);
define('IMG_MAIL_LOGO_HEIGHT', 68);

define('IMG_SECCION_WIDTH', 300);
define('IMG_SECCION_HEIGHT', 300);

define('IMG_SECCION_WIDTH_RESIZE', 100);
define('IMG_SECCION_HEIGHT_RESIZE', 100);

define('IMG_ABUELO_BANNER_WIDTH', 713);
define('IMG_ABUELO_BANNER_HEIGHT', 161);

define('IMG_ABUELO_WIDTH', 236);
define('IMG_ABUELO_HEIGHT', 120);


//Otras Configuraciones
define('BANNER_SECCION_WIDTH', 503);
define('BANNER_SECCION_HEIGHT', 150);

define('IMG_PRODUCTO_SMALL_PROM_WIDTH', 60);
define('IMG_PRODUCTO_SMALL_PROM_HEIGHT', 60);

define('IMG_NOTICIA_WIDTH', 250);
define('IMG_NOTICIA_HEIGHT', 200);

define('IMG_BANNER_WIDTH', 750);
define('IMG_BANNER_HEIGHT', 120);

define('IMG_BOLETIN_WIDTH', 300);
define('IMG_BOLETIN_HEIGHT', 200);

define('IMG_BOLETIN2_WIDTH', 400);
define('IMG_BOLETIN2_HEIGHT', 266);
define('IMG_WIKI_WIDTH', 450);

define('PAGINACION1', 20);
define('PAGINACION2', 50);
define('PAGINACION3', 100);

define('EMAILEMPRESA', "gpltda@nguillers.com.co");
define('EMAILEMPRESAVENTAS', "ventas@nguillers.com.co");
define('DOMINIO', "http://www.nguillers.com.co");

define('STOCKDISPONIBLE', 1);
define('COLUMNASSECCION', 5);

define('FLETE', 8000);

define('TAMANO_TITULO_PRODUCTO', 100);
define('TAMANO_TITULO_NOTICIA', 45);
define('TAMANO_TEXTO_NOTICIA', 150);
define('TAMANO_TITULO_VIDEO', 35);

define('IMG_PUBLICIDAD_WIDTH', 198);
define('IMG_TENDENCIA_WIDTH', 300);
/*
define('IMG_PORTADA_WIDTH', 504);
define('IMG_PORTADA_HEIGHT', 355);
*/

define('MAX_PUBLICIDAD', 1200);
define('MAX_VIDEOS', 2);
define('MAX_BOLETINES', 5);
define('TXT_ENVIO_SOLICITUD', "Hola, <br/>
Muchas gracias por enviar tu solicitud de pedido !!!<br/>
Tu pedido ingresa al proceso de verificación.<br/>
LA COTIZACION FINAL SERA ENVIADA DENTRO DE LAS PROXIMAS 12 HORAS LABORALES, especificando valor total en Pesos Colombianos (COP) o Dólares Americanos (USD), según corresponda, incluyendo el proceso de pago y formas de envío. (Lun – Vie 9:00am – 6:00pm  / Sab  9:00am – 3pm)<br/><br/>
Esta es una copia de lo que ordenaste:");

//******************* PARAMETROS ****************************
