<?php
Class ImportFiles {
	public static $productos = array(
			"Model" => "Productos",
			"Id" => "IdProducto",
			"Columns" => array(
	            "nombreProducto" => array("alt"=>"Nombre","required"=>true),
	            "descripcionProducto" => array("required"=>true),
	            "medidaProducto" => array("required"=>true),
	            "materialProducto" => array("required"=>true),
	            "nombreFormaProducto" => array("alt"=>"nombreForma","required"=>true),
	            "colorProducto" => "colorProducto",
	            "nombrePeso" => array("get_from" => array("Pesos","nombrePeso","idPeso")),
	            "novedadProducto" => "novedadProducto",
	            "descuentoProducto" => "descuentoProducto",
	            "idSeccion" => "idSeccion",
	            "nombrePaquete" => array("get_from" => array("Paquetes","nombrePaquete","idPaquete")),
	            "nombrePresentacion" => array("get_from" => array("Presentaciones","nombrePresentacion","idPresentacion")),
	            "refPaqueteProducto" => array(
	            		"image" => array("destinyName" => "idProducto",
        				"Width" => IMG_PRODUCTO_BIG_WIDTH,
        				"Height" => IMG_PRODUCTO_BIG_HEIGHT,
	            		"originFolderPath" => TMP_IMG_PATH,
	            		"originFolderUrl" => TMP_IMG_URL,
	            		"destinyFolderPath" => IMG_PRD_PATH_BIG,
	            		"destinyFolderUrl" => IMG_PRD_URL_BIG),
	            		"resize" => array(
	            				"Width" => IMG_PRODUCTO_SMALL_WIDTH,
	            				"Height" => IMG_PRODUCTO_SMALL_HEIGHT,
			            		"destinyFolderPath" => IMG_PRD_PATH_SMALL,
			            		"destinyFolderUrl" => IMG_PRD_URL_SMALL),
	            			)
	            		),
	        "additional_tables" => array(
	        	"PaquetesProducto" => array(
	        		"references_with" => "idProducto",
	        		"Columns" => array(
						"refPaqueteProducto" => array("required"=>true),
						"intervalo1" => array("required"=>true),
						"precio1" => array("required"=>true),
						"intervalo2" => array("required"=>true),
						"precio2" => array("required"=>true),
						"intervalo3" => array("required"=>true),
						"precio3" => array("required"=>true),
						"stockPaqueteProducto" => array("alt"=>"stock","required"=>true)
	        		)
	        	)
	        )
		);
}

/*


	            "nombreProducto" => "nombreProducto",
	            "descripcionProducto" => "descripcionProducto",
	            "medidaProducto" => "medidaProducto",
	            "materialProducto" => "materialProducto",
	            "nombreForma" => "nombreFormaProducto",
	            "colorProducto" => "colorProducto",
	            "nombrePeso" => "nombrePeso",
	            "novedadProducto" => "novedadProducto",
	            "descuentoProducto" => "descuentoProducto",
	            "idSeccion" => "idSeccion",
	            "nombrePaquete" => "nombrePaquete",
	            "nombrePresentacion" => "nombrePresentacion",
	            "refPaqueteProducto" => "refPaqueteProducto",
	            "intervalo1" => "intervalo1",
	            "precio1" => "precio1",
	            "intervalo2" => "intervalo2",
	            "precio2" => "precio2",
	            "intervalo3" => "intervalo3",
	            "precio3" => "precio3",
	            "stock" => "stock"


	            */