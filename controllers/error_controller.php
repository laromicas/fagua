<?php

class Error_Controller extends Controller {

	function __construct() {
		parent::__construct();
	}
	
	function index() {
		header("HTTP/1.0 404 Not Found");
		header("Status: 404 Not Found");
		/*$this->view->msg = 'This page doesnt exist';
		$this->view->render('error/index');*/
        // $this->view->Result = Translate::tr(array("message","page_under_construction"));

        $this->view->MenuSuperior = QueryHelper::make_menu(1);
        $this->view->MenuInferior = QueryHelper::make_menu(2);

        
        $this->view->Result = "Error 404. Oops. The page you are typing cannot be found. Please return to main page.";
        $this->view->render('shared/result');
	}

	function export_imgs () {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 600);
		ignore_user_abort (true);
		set_time_limit(0);
		$productos = Model::factory('Productos')->filter('joinPaquete')->find_many();
		foreach ($productos as $producto) {
			$file = pathinfo($producto->imagenProducto);
			$filename = $file['basename'];
			if($filename) {
				$ext = $file['extension'];
				if(file_exists(IMG_PRD_PATH_BIG.$filename) && !file_exists(TMP_IMG_PATH.$producto->refPaqueteProducto.'.'.$ext)) {
					echo IMG_PRD_PATH_BIG.$filename .' - '.TMP_IMG_PATH.$producto->refPaqueteProducto.'.'.$ext.'<br>';
					copy(IMG_PRD_PATH_BIG.$filename, TMP_IMG_PATH.$producto->refPaqueteProducto.'.'.$ext);
				}
			}
			flush();
		}
		echo 'bien';
	}

	function copy_images() {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 600);
		ignore_user_abort (true);
		set_time_limit(0);
		$source = '/var/chroot/home/content/10/11198210/html/nguillerstemp/file/tmp_img/';
		$dest = '/var/chroot/home/content/10/11198210/html/file/tmp_img/';
		// $source = TMP_IMG_PATH;
		$files = glob($source.'*.*');
		// var_dump($files);die();
		foreach ($files as $filen) {
			$file = pathinfo($filen);
			$filename = $file['basename'];
			$ext = $file['extension'];
			if(!file_exists($dest.$filename)) {
				echo $source.$filename.' - '.$dest.$filename.'<br>';
				copy($source.$filename,$dest.$filename);
			}
			flush();
		}
		echo 'bien';
	}

	function rescue_imgs() {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 600);
		ignore_user_abort (true);
		set_time_limit(0);
		$ids = array(10976,34892,34919,34928);
		// $productos = Model::factory('Productos')->filter('joinPaquete')->order_by_asc('idProducto')->find_many();
		$ids = array(699,2852,5174,5190,5190,5193,5193,5194,5194,5195,5195,5196,5196,7439,7816,7816,7978,7978,7979,7979,7981,7981,7982,7982,7984,7984,7985,7985,7986,7986,8742,8742,9102,9343,9873,10976,12143,12808,13717,13838,14270,34886,34892,34919,34928,34943,34951,35001,35002,35004,35005,35028,35028,35042,35118,35122,35127,35174,35175,35194,35196,35201,35205,35209,35210,35211,35227,35228,35229,35232,35257,35268,35273,35275,35276,35283,35284,35304,35305,35306,35307,35308,35309,35310,35311,35312,35313,35314,35315,35316,35317,35318,35319,35320,35321,35322,35323,35324,35325,35326,35327,35328,35329,35330,35331,35332,35333,35334,35335,35336,35337,35338,35339,35340,35341,35342,35343,35344,35345,35346,35347,35348,35349,35350,35351,35352,35353,35354,35355,35356,35357,35358,35359,35360,35361,35362,35363,35364,35365,35366,35367,35368,35369,35370,35371,35372,35373,35374,35374,35375,35376,35377,35378,35379,35380,35381,35382,35383,35384,35385,35386,35387,35388,35389,35390,35391,35392,35393,35394,35395,35396,35397,35398,35399,35400,35401,35402,35403,35404,35405,35406,35407,35408,35409,35410,35411,35412,35413,35414,35417,35469,35470,35471,35474,35475,35476,35477,35478,35479,35480,35481,35482,35483,35484,35485,35486,35487,35488,35489,35490,35491,35492,35493,35494,35495,35496,35497,35498,35499,35500,35501,35502,35503,35504,35505,35506,35507,35508,35509,35510,35511,35512,35513,35514,35515,35516,35517,35518,35519,35520,35521,35522,35523,35524,35525,35526,35527,35528,35529,35530,35531,35532,35533,35534,35535,35536,35537,35538,35539,35540,35541,35542,35543,35544,35545,35546,35547,35548,35549,35550,35551,35552,35553,35554,35555,35556,35557,35558,35559,35560,35561,35562,35563,35564,35565,35566,35567,35568,35569,35569,35570,35570,35571,35571,35573,35573,35574,35574,35575,35575,35576,35576,35577,35577,35578,35579,35580,35581,35582,35583,35584,35585,35586,35587,35588,35589,35590,35591,35592,35593,35594,35595,35596,35597,35598,35599,35600,35601,35602,35603,35604,35605,35606,35607,35608,35609,35610,35611,35612,35613,35614,35615,35616,35617,35618,35619,35620,35621,35622,35623,35624,35625,35626,35627,35628,35629,35630,35631,35632,35633,35634,35635,35636,35637,35638,35639,35640,35641,35642,35643,35644,35645,35646,35647,35648,35649,35650,35651,35652,35653,35654,35655,35656,35657,35658,35659,35660,35661,35662,35663,35664,35665,35666,35667,35668,35669,35670,35671,35672,35674,35686,35686,35687,35687,35688,35688,35689,35689,35690,35690,35691,35691,35692,35692,35693,35694,35695,35696,35697,35698,35699,35700,35723,35724,35725,35726,35727,35728,35729,35730,35731,35732,35733,35734,35735,35736,35737,35738,35739,35740,35741,35742,35743,35744,35745,35746,35747,35748,35749,35750);
		$productos = Model::factory('Productos')->filter('joinPaquete')->where_in('productos.idProducto',$ids)->find_many();
		// echo count($productos); die();
		foreach ($productos as $producto) {
			$imagen = TMP_IMG_PATH.$producto->refPaqueteProducto.'.jpg';
			if(file_exists($imagen)) {
				// if(!$producto->imagenProducto) {
				// 	$producto->imagenProducto = $producto->idProducto.'.jpg';
				// }
				if(!file_exists(IMG_PRD_PATH_BIG.$producto->imagenProducto)) {
					copy($imagen,IMG_PRD_PATH_BIG.$producto->imagenProducto);
					echo IMG_PRD_PATH_BIG.$producto->imagenProducto;
				}
				if(!file_exists(IMG_PRD_PATH_SMALL.$producto->imagenProducto) && !file_exists(IMG_PRD_PATH_CART.$producto->imagenProducto)) {
			        $image = new SimpleImage();
			        $image->load($imagen);
			        if($image->getWidth() != IMG_PRODUCTO_BIG_WIDTH || $image->getHeight() != IMG_PRODUCTO_BIG_HEIGHT) {
			            continue;
			        }
				}
				if(!file_exists(IMG_PRD_PATH_SMALL.$producto->imagenProducto)) {
					$pathImage = IMG_PRD_PATH_SMALL.$producto->imagenProducto;
					$image->resize(IMG_PRODUCTO_SMALL_WIDTH,IMG_PRODUCTO_SMALL_HEIGHT);
					$image->save($pathImage);
					echo ' - '.$pathImage;
				}
				if(!file_exists(IMG_PRD_PATH_CART.$producto->imagenProducto)) {
					$pathImage = IMG_PRD_PATH_CART.$producto->imagenProducto;
					$image->resize(IMG_PRODUCTO_CART_WIDTH,IMG_PRODUCTO_CART_HEIGHT);
					$image->save($pathImage);
					echo ' - '.$pathImage;
				}
				echo '<br>';
			}
			flush();
			file_put_contents(EXT_PATH.'file/vamos.txt', $producto->idProducto);
		}
		echo 'bien';
	}

	function list_imgs() {
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 600);
		ignore_user_abort (true);
		set_time_limit(0);
		$productos = Model::factory('Productos')->filter('joinPaquete')->where('activeProducto',1)->order_by_asc('idProducto')->find_many();
		foreach ($productos as $producto) {
			$imagen = IMG_PRD_PATH_BIG.$producto->imagenProducto;
			if(!file_exists($imagen)) {
				echo $producto->idProducto.';'.$producto->refPaqueteProducto.'<br>';
			}
		}
	}

	function find_imgs() {
		$ids = array(699,2852,5174,5190,5190,5193,5193,5194,5194,5195,5195,5196,5196,7439,7816,7816,7978,7978,7979,7979,7981,7981,7982,7982,7984,7984,7985,7985,7986,7986,8742,8742,9102,9343,9873,10976,12143,12808,13717,13838,14270,34886,34892,34919,34928,34943,34951,35001,35002,35004,35005,35028,35028,35042,35118,35122,35127,35174,35175,35194,35196,35201,35205,35209,35210,35211,35227,35228,35229,35232,35257,35268,35273,35275,35276,35283,35284,35304,35305,35306,35307,35308,35309,35310,35311,35312,35313,35314,35315,35316,35317,35318,35319,35320,35321,35322,35323,35324,35325,35326,35327,35328,35329,35330,35331,35332,35333,35334,35335,35336,35337,35338,35339,35340,35341,35342,35343,35344,35345,35346,35347,35348,35349,35350,35351,35352,35353,35354,35355,35356,35357,35358,35359,35360,35361,35362,35363,35364,35365,35366,35367,35368,35369,35370,35371,35372,35373,35374,35374,35375,35376,35377,35378,35379,35380,35381,35382,35383,35384,35385,35386,35387,35388,35389,35390,35391,35392,35393,35394,35395,35396,35397,35398,35399,35400,35401,35402,35403,35404,35405,35406,35407,35408,35409,35410,35411,35412,35413,35414,35417,35469,35470,35471,35474,35475,35476,35477,35478,35479,35480,35481,35482,35483,35484,35485,35486,35487,35488,35489,35490,35491,35492,35493,35494,35495,35496,35497,35498,35499,35500,35501,35502,35503,35504,35505,35506,35507,35508,35509,35510,35511,35512,35513,35514,35515,35516,35517,35518,35519,35520,35521,35522,35523,35524,35525,35526,35527,35528,35529,35530,35531,35532,35533,35534,35535,35536,35537,35538,35539,35540,35541,35542,35543,35544,35545,35546,35547,35548,35549,35550,35551,35552,35553,35554,35555,35556,35557,35558,35559,35560,35561,35562,35563,35564,35565,35566,35567,35568,35569,35569,35570,35570,35571,35571,35573,35573,35574,35574,35575,35575,35576,35576,35577,35577,35578,35579,35580,35581,35582,35583,35584,35585,35586,35587,35588,35589,35590,35591,35592,35593,35594,35595,35596,35597,35598,35599,35600,35601,35602,35603,35604,35605,35606,35607,35608,35609,35610,35611,35612,35613,35614,35615,35616,35617,35618,35619,35620,35621,35622,35623,35624,35625,35626,35627,35628,35629,35630,35631,35632,35633,35634,35635,35636,35637,35638,35639,35640,35641,35642,35643,35644,35645,35646,35647,35648,35649,35650,35651,35652,35653,35654,35655,35656,35657,35658,35659,35660,35661,35662,35663,35664,35665,35666,35667,35668,35669,35670,35671,35672,35674,35686,35686,35687,35687,35688,35688,35689,35689,35690,35690,35691,35691,35692,35692,35693,35694,35695,35696,35697,35698,35699,35700,35723,35724,35725,35726,35727,35728,35729,35730,35731,35732,35733,35734,35735,35736,35737,35738,35739,35740,35741,35742,35743,35744,35745,35746,35747,35748,35749,35750);
		$productos = Model::factory('Productos')->filter('joinPaquete')->where_in('productos.idProducto',$ids)->find_many();
		foreach ($productos as $producto) {
			$imagen = IMG_PRD_PATH_BIG.$producto->imagenProducto;
			if(file_exists($imagen)) {
				echo $producto->idProducto.';'.$producto->refPaqueteProducto.'<br>';
			}
		}
	}

	function move_imported() {
		$source = '/var/chroot/home/content/10/11198210/html/file/tmp_img/';
		$dest = '/var/chroot/home/content/10/11198210/html/file/tmp_img/imported/';
		$files = glob($source.'*.*');
		foreach ($files as $filen) {
			$file = pathinfo($filen);
			$filename = $file['basename'];
			$ext = $file['extension'];
			echo $source.$filename.' - '.$dest.$filename.'<br>';
			rename($source.$filename,$dest.$filename);
			flush();
		}
	}
}