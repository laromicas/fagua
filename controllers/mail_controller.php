<?php

class Mail_Controller extends Controller {
    
    function __construct() {
        parent::__construct();
    }

    function ver() {
        $this->view->Plantillas = Model::factory('Plantillas')->find_many();
        $this->view->render('ver');
    }

    function previsualizar() {
        // var_dump($_POST);
        $this->view->idPlantilla = $_POST['plantilla'];
        $this->view->Plantilla = Model::factory('Plantillas')->where('id',$_POST['plantilla'])->find_one();


        $Enviados = Model::factory('Enviados')->where('id_plantilla',$_POST['plantilla'])->find_many();
        $Sent = array();
        foreach ($Enviados as $enviado) {
            $Sent[] = $enviado->id_casting;
        }
        $Casting = array_diff($_POST['casting'], $Sent);

        $Sent = array_intersect($_POST['casting'], $Sent);

        // echo 'aca';die();
        if(count($Casting)) {
            $this->view->Casting = Model::factory('Casting')->where_in('id',$Casting)->find_many();
        } else {
            $this->view->Casting = array();
        }

        if(count($Sent)) {
            $this->view->Sent = Model::factory('Casting')->where_in('id',$Sent)->find_many();
        } else {
            $this->view->Sent = array();
        }



        $this->view->castingIds = $_POST['casting'];
        $this->view->render('previsualizar');
    }

    function enviarmails() {
        // var_dump($_POST);die();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3600);
        $this->view->idPlantilla = $_POST['plantilla'];
        $Plantilla = Model::factory('Plantillas')->where('id',$_POST['plantilla'])->find_one();
        if(!(@$_POST["reenviar"])) {
            $Enviados = Model::factory('Enviados')->where('id_plantilla',$_POST['plantilla'])->find_many();
            $Sent = array();
            foreach ($Enviados as $enviado) {
                $Sent[] = $enviado->id_casting;
            }
            $Casting = array_diff($_POST['casting'], $Sent);
        } else {
            $Casting = $_POST['casting'];
        }

        if(count($Casting)) {
            $this->view->Casting = Model::factory('Casting')->where_in('id',$Casting)->find_many();
        } else {
            $this->view->Casting = array();
        }

        foreach ($this->view->Casting as $Casting) {
            $mail = new Mail_Casting($Plantilla,$Casting);
            $mail->send();
            echo $Casting->email;
            var_dump($mail->errors);
            ob_flush();
            flush();
            $sent = Model::factory('Enviados')->where('id_plantilla',$_POST['plantilla'])->where('id_casting',$Casting->id)->find_one();
            if(!$sent){
                $sent = Model::factory('Enviados')->create();
                echo 3;
            }
            $sent->id_casting = $Casting->id;
            $sent->id_plantilla = $_POST['plantilla'];
            $sent->save();
        }
        // $this->view->render('previsualizar');
    }

    function enviar() {
        $this->view->Peliculas = Model::factory('Peliculas')->find_many();
        $this->view->Plantillas = Model::factory('Plantillas')->find_many();
        $this->view->Casting = Model::factory('Casting')->find_many();
        $this->view->render('enviar');
    }

    function getnext($id) {
        if($id=='last') {
            $casting = Model::factory('Casting')->order_by_desc('id')->find_one();
            $id = $casting->id;
        }
        $casting1 = Model::factory('Casting')->where_gt('id',$id)->find_one();
        $casting2 = Model::factory('Casting')->where_lt('id',$id)->order_by_desc('id')->find_one();
        $next = ($casting1) ? $casting1->id : $id;
        $prev = ($casting2) ? $casting2->id : 1;
        echo json_encode(array('prev' => $prev, 'next' => $next));
    }

    function mail_prueba($idPlantilla,$idCasting) {
        if($idCasting=='last') {
            $casting = Model::factory('Casting')->order_by_desc('id')->find_one();
            $idCasting = $casting->id;
        }
        $Plantilla = Model::factory('Plantillas')->where('id',$idPlantilla)->find_one();
        $Casting = Model::factory('Casting')->where('id',$idCasting)->find_one();
        $mail = new Mail_Casting($Plantilla,$Casting);
        echo '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>';
        echo $mail->get_html();
        //$mail->send('davidmorales2005@gmail.com','Pedido numero #',$mensaje);
        // $mail->send('laromicas@hotmail.com','Pedido numero #',$mensaje);
        // $mail->send();
        // var_dump($mail->errors);
        return;
    }

    function save_boletines($value='') {
        $name = $_POST["name"];
        $email = $_POST["email"];
        if(filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email)) {
            $user = Model::factory('UsuariosBoletines')->where('email',$email)->find_one();
            if(!$user) {
                $user = Model::factory('UsuariosBoletines')->create();
                $user->email = $email;
                $user->name = $name;
                $user->save();
                echo "OK, grabo";
            }
        } else {
            echo "E-mail is not valid";
        }
    }

    function tracking($idFactura) {
        $factura = Model::factory('Facturas')->where('idFactura',$idFactura)->find_one();
        if(!$factura) {
            return;
        }
        $carrier = $factura->carrier();
        $this->view->bodyClass =  BodyClass::getBodyClass("login");
        $this->view->MenuSuperior = QueryHelper::make_menu(1);
        $this->view->MenuInferior = QueryHelper::make_menu(2);
        $this->view->message = '<form action="'.$carrier->urlTracking.'" method="'.$carrier->methodTracking.'" class="formTracking" style="display:inline-block" id="formTracking">'.
                '<button class="callTracking" style="float:right;border:none">'.$factura->numeroGuia.'</button>'.
                '<input type="hidden" name="'.$carrier->columnTracking.'" value="'.$factura->numeroGuia.'" >'.
                '</form>'.
            'You are being redirected'.
            '<script type="text/javascript">'.
            'jQuery("#formTracking").submit();'.
            '</script>';
        $this->view->render('shared/message');
    }
}
