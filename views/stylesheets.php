<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
<?php
Html::style("bootstrap",true);
Html::style("bootstrap-responsive",true);
Html::style("font-awesome.min",true);
Html::style("style",true);
Html::style("jquery.fancybox",true);
Html::style("cloud-zoom",true);
Html::style("portfolio",true);
Html::style("layerslider/layerslider",true);
Html::style("layerslider/layersliderstyle",true);
Html::style("jquery.dataTables",true);
