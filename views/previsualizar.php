<form id="form" action="<?= URL; ?>mail/enviarmails" method="post">
  <div class="sidebar1">
    <ul class="nav">
        <li></li>
    </ul>
    <aside>
      <h3>Enviar Plantilla</h3>
      <?= $this->Plantilla->tituloPelicula; ?> - <?= $this->Plantilla->tituloMail; ?>
      <input type="hidden" name="plantilla" value="<?= $_POST['plantilla']; ?>">
      <h3>A los siguientes:</h3>
      <ul>
      	<?php foreach ($this->Casting as $casting): ?>
      		<li><?= $casting->nombres; ?> <?= $casting->apellidos; ?></li>
          <input type="hidden" name="casting[]" value="<?= $casting->id; ?>">
      	<?php endforeach ?>
      </ul>
      <h3>A estas personas ya se les envio:</h3>
      <ul>
        <?php foreach ($this->Sent as $casting): ?>
          <li style="color:red"><?= $casting->nombres; ?> <?= $casting->apellidos; ?></li>
          <input type="hidden" name="casting[]" value="<?= $casting->id; ?>">
        <?php endforeach ?>
      </ul>
      
      <p><label for="reenviar"><input type="checkbox" name="reenviar" id="reenviar"> Reenviar</label></p>
      <p><button>ENVIAR</button></p>
    </aside>
  <!-- end .sidebar1 --></div>
</form>

  
  <article class="content">
    <h1>Controles</h1>
    <div id="botones">
    <button id="first"><span><<</span></button>
    <button id="prev"><span><</span></button>
    <button id="next"><span>></span></button>
    <button id="last"><span>>></span></button>
    </div>
    <section style="text-align:center">
     <iframe width="100%" height="800px" id="test" src="<?= URL; ?>mail/mail_prueba/<?= $this->idPlantilla; ?>/<?= $this->castingIds[0]; ?>"></iframe>
    </section>
    <section>    </section>
  <!-- end .content --></article>

<script type="text/javascript">
indice = 0;
casting = <?= json_encode($this->castingIds); ?>;
total = casting.length;
next = 2;
prev = 1;
	$('#first').click(function (e) {
		e.preventDefault();
		indice = 0;
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/<?= $this->idPlantilla; ?>/'+casting[indice]);
	})
	$('#prev').click(function (e) {
		e.preventDefault();
		indice = indice - 1;
		if(indice < 0) { indice = 0; }
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/<?= $this->idPlantilla; ?>/'+casting[indice]);
	})
	$('#next').click(function (e) {
		e.preventDefault();
		indice = indice + 1;
		if(indice > (casting.length - 1)) { indice = casting.length - 1; }
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/<?= $this->idPlantilla; ?>/'+casting[indice]);
	})
	$('#last').click(function (e) {
		e.preventDefault();
		indice = casting.length - 1;
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/<?= $this->idPlantilla; ?>/'+casting[indice]);
	})

</script>