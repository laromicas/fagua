  <div class="sidebar1">
    <ul class="nav">
        <li></li>
    </ul>
    <aside>
      <p>PLANTILLAS</p>
      <p><select id="select">
      <option value=""></option>
      	<?php foreach ($this->Plantillas as $plantilla): ?>
      		<option value="<?= $plantilla->id; ?>"><?= $plantilla->id; ?> - <?= $plantilla->tituloPelicula; ?></option>
      	<?php endforeach ?>
      </select></p>
    </aside>
  <!-- end .sidebar1 --></div>

  
  <article class="content">
    <h1>Controles</h1>
    <div id="botones" style="display:none">
    <button id="first"><span><<</span></button>
    <button id="prev"><span><</span></button>
    <button id="next"><span>></span></button>
    <button id="last"><span>>></span></button>
    </div>
    <section style="text-align:center">
     <iframe src="" width="90%" height="800px" id="test"></iframe>
    </section>
    <section>    </section>
  <!-- end .content --></article>

<script type="text/javascript">
indice = 1;
next = 2;
prev = 1;
	$('#select').change(function (e) {
		if($('#select').val() == '') {
			$('#test').attr('src','');
			$('#test').hide();
			$('#botones').hide();
		} else {
			$('#test').attr('src','<?= URL; ?>mail/mail_prueba/'+$('#select').val()+'/'+indice);
			$('#test').show();
			$('#botones').show();
		}
		indice = 1;
	})
	$('#first').click(function (e) {
		e.preventDefault();
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/'+$('#select').val()+'/1');
		indice = 1;
		jQuery.getJSON('<?= URL; ?>mail/getnext/1',function (data) {
			prev = data.prev;
			next = data.next;
		});
	})
	$('#prev').click(function (e) {
		e.preventDefault();
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/'+$('#select').val()+'/'+prev);
		indice = prev;
		jQuery.getJSON('<?= URL; ?>mail/getnext/'+indice,function (data) {
			prev = data.prev;
			next = data.next;
		});
	})
	$('#next').click(function (e) {
		e.preventDefault();
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/'+$('#select').val()+'/'+next);
		indice = next;
		jQuery.getJSON('<?= URL; ?>mail/getnext/'+indice,function (data) {
			prev = data.prev;
			next = data.next;
		});
	})
	$('#last').click(function (e) {
		e.preventDefault();
		$('#test').attr('src','<?= URL; ?>mail/mail_prueba/'+$('#select').val()+'/last');
		jQuery.getJSON('<?= URL; ?>mail/getnext/last',function (data) {
			prev = data.prev;
			next = data.next;
			indice = data.next;
		});
	})

</script>