<form id="form" action="<?= URL; ?>mail/previsualizar" method="post">
  <div class="sidebar1">
    <ul class="nav">
        <li></li>
    </ul>
    <aside>
      <p>Seleccione Plantilla</p>
      <p><select id="select" name="plantilla">
      <option value=""></option>
      	<?php foreach ($this->Plantillas as $plantilla): ?>
      		<option value="<?= $plantilla->id; ?>"><?= $plantilla->id; ?> - <?= $plantilla->tituloPelicula; ?></option>
      	<?php endforeach ?>
      </select></p>
      <br>
      <p>Filtrar por Pelicula</p>
      <p>
      	<?php foreach ($this->Peliculas as $pelicula): ?>
      		
			<label for="pelicula-<?= $pelicula->id; ?>">
			<input type="checkbox" value="<?= $pelicula->id; ?>" id="pelicula-<?= $pelicula->id; ?>" class="pelicula">
			<?= $pelicula->id; ?> - <?= $pelicula->nombre; ?></label>

      	<?php endforeach ?>
      </p>
      <p>
      	<button id="enviar"><span>Previsualizar</span></button>
      </p>
    </aside>
  <!-- end .sidebar1 --></div>
  
  <article class="content">
    <h1>Casting</h1>
    <div id="botones" style="display:none">
    <button id="first"><span><<</span></button>
    <button id="prev"><span><</span></button>
    <button id="next"><span>></span></button>
    <button id="last"><span>>></span></button>
    </div>
    <section style="text-align:center">
     <table id="casting">
     	<thead>
     		<tr>
     			<td><input type="checkbox" id="selectAll"></td>
     			<td>Nombres</td>
     			<td>Apellidos</td>
     			<td>Sexo</td>
     			<td>Edad</td>
     			<td>Telefono</td>
     			<td width="20%">Pelicula</td>
     		</tr>
     	</thead>
     	<tbody>
     	<?php foreach ($this->Casting as $actor): ?>
     		<tr id="actor-<?= $actor->id ?>">
     			<td><input type="checkbox" class="selectActor" data-actor="<?= $actor->id ?>" name="casting[]" value="<?= $actor->id ?>"></td>
     			<td><?= $actor->nombres ?></td>
     			<td><?= $actor->apellidos ?></td>
     			<td><?= $actor->sexo ?></td>
     			<td><?= $actor->edad ?></td>
     			<td><?= $actor->telefono ?></td>
     			<td><?= $actor->nombres_peliculas(); ?></td>
     		</tr>
     	<?php endforeach ?>
     	</tbody>
     </table>
    </section>
    <section>    </section>
  <!-- end .content --></article>
</form>
<script type="text/javascript">
$('table#casting').dataTable( {
		"iDisplayLength": 100,
		"bPaginate": false,
		"aoColumns":[
			{ "bSortable": false },
			{ "bSortable": true },
			{ "bSortable": true },
			{ "bSortable": true },
			{ "bSortable": true },
			{ "bSortable": true },
			{ "bSortable": true }
		],
        "sDom": '<fl><p>t<p>'
});
$('input.pelicula').click(function (e) {
	var oTable = $("table#casting").dataTable();
	var anNodes = $("table#casting tbody tr");
	for (var i = 0; i < anNodes.length; ++i)
	{
	    var rowData = oTable.fnGetData( anNodes[i] );
	    $(anNodes[i]).find('input.selectActor')
	 
	    //some stuff with the obtained data
	    //...
	}
})

sendTo = {};
$('#selectAll').click(function (e) {
	var checked = $(this).is(':checked');
	var oTable = $("table#casting").dataTable();
	var anNodes = $("table#casting tbody tr");
	for (var i = 0; i < anNodes.length; ++i)
	{
	    var rowData = oTable.fnGetData( anNodes[i] );
	    nodo = $(anNodes[i]).find('input.selectActor')
	    if(checked) {
	    	if(!(nodo.is(':checked'))) {
	    		nodo.trigger('click');
	    	}
	    	// nodo[0].checked = true;
	    } else {
	    	if((nodo.is(':checked'))) {
	    		nodo.trigger('click');
	    	}
	    	// nodo[0].checked = false;
	    }
	}
})

$('.selectActor').change(function (e) {
	if($(this).is(':checked')) {
		sendTo[$(this).data('actor')] = 1;
	} else {
		delete(sendTo[$(this).data('actor')]);
	}
})

$('button#enviar').click(function (e) {
	e.preventDefault();
	formData = $('form').serializeArray();
	if($('select#select').val() == "") {
		alert('Debe seleccionar una plantilla');
		return;
	}
	if($('.selectActor:checked').length == 0) {
		alert('Debe seleccionar al menos un actor');
		return;
	}
	$('form').submit();
})

</script>